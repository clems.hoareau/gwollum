/**
 * @file 
 * @brief Chains of TTrees.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Chain__
#define __Chain__

#include "CUtils.h"
#include <TFile.h>
#include <TChain.h>

using namespace std;

/**
 * @brief Chain ROOT trees in a list of files defined by a file pattern.
 * @details This class inherits from the <a href="http://root.cern.ch/root/htmldoc/TChain.html">TChain</a> class from ROOT. Only the constructor was modified and a few functions was added.
 * @author Florent Robinet
 */
class Chain : public TChain {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Chain class.
   * @param[in] aChainName TTree name to be found in the ROOT files.
   * @param[in] aPattern File pattern. Any unix-type patterns is supported (list of files separated by spaces, *, ?)
   * @param[in] aVerbose Flag to activate the verbosity.
   */
  Chain(const string aChainName, const string aPattern, const bool aVerbose=false);

  /**
   * @brief Destructor of the Chain class.
   */
  virtual ~Chain(void);
 
  /**
     @}
  */

  /**
   * @brief Adds files to the chain.
   * @details The TTrees named 'aChainName' found in 'aPattern' are added at the end of the chain.
   * @param[in] aPattern File pattern. Any unix-type patterns is supported (list of files separated by spaces, *, ?)
   */
  void AddFile(const string aPattern);

  /**
   * @brief Returns the number of ROOT files where the designated tree was found.
   */
  inline unsigned int GetN(void){ return rootfilename.size(); };

  /**
   * @brief Returns the file path of a given ROOT file.
   * @param[in] aFileIndex File index.
   * @pre the file index must be valid
   * @sa GetN()
   */
  inline string GetFilePath(const unsigned int aFileIndex){
    return rootfilename[aFileIndex];
  };

  /**
   * @brief Returns the input file pattern provided in the constructor.
   */
  inline string GetInputFilePattern(void){ return spattern; };

 private:

  vector <string> rootfilename; ///< Active root file names.
  string spattern;              ///< Input file pattern.
  string fchainname;            ///< Tree name.
  bool fverbose;                ///< Verbosity flag.

  /**
   * @brief Constructor for POSIX files.
   * @param[in] aRootFilePattern ROOT files pattern.
   */
  void ChainPOSIX(const string aRootFilePattern);

  /**
   * @brief Constructor for Xrootd files.
   * @param[in] aRootFiles List of root files
   */
  void ChainXROOTD(const vector <string> aRootFiles);

  ClassDef(Chain,0)
};

#endif


