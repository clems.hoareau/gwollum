/**
 * @file 
 * @brief Fast Fourier transform wrapper for FFTW.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __fft__
#define __fft__

#include "CUtils.h"
#include <fftw3.h>

using namespace std;

/**
 * @brief Wrap and optimize FFTW.
 * @sa <a href="http://www.fftw.org/">FFTW</a>.
 * @author Florent Robinet
 */
class fft {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the fft class.
   * @details A FFT plan is contructed for a given time-domain vector size.
   * The input size must be an even number or else it will be forced as such.
   *
   * A FFT type must be provided.
   * The default "c2c" type provides a complex-to-complex FFT.
   * In this case, the time-domain and the frequency domain vectors are both complex and have the same size.
   * If a "r2c" type is selected, the time-domain vector is purely real.
   * The frequency-domain vector is complex and its size is "aSize_t/2+1".
   *
   * @warning When using the "r2c" type, the backward transform will alter the frequency-domain vector. Do not use it after the transform. 
   *
   * Several FFTW plans are supported: "FFTW_ESTIMATE", "FFTW_MEASURE" and "FFTW_PATIENT".
   * @param[in] aSize_t Time-domain vector size: must be an even number.
   * @param[in] aPlan FFT plan.
   * @param[in] aType FFT type "r2c" or "c2c".
   */
  fft(const unsigned int aSize_t, const string aPlan = "FFTW_ESTIMATE", const string aType="c2c");

  /**
   * @brief Destructor of the fft class.
   */
  virtual ~fft(void);// destructor
  /**
     @}
  */
  
  /**
   * @brief Performs the forward FFT.
   */
  inline void Forward(void){ fftw_execute(FFT_Forward); };

  /**
   * @brief Performs the backward FFT.
   */
  inline void Backward(void){ fftw_execute(FFT_Backward); };

  /**
   * @brief Performs the forward FFT of a time-domain complex data vector.
   * @details The input data vector is defined by 2 arrays; the real and imaginary parts.
   * The input data vector is copied before being transformed.
   * @note This function can only be called in a "c2c" mode.
   * @returns false if the "c2c" mode was not selected in the constructor; true if success.
   * @param[in] aRe_t Time-domain data vector (real part).
   * @param[in] aIm_t Time-domain data vector (imaginary part).
   * @pre The input pointers must point to arrays the size of which must match the definition given in the constructor.
   */
  bool Forward(double *aRe_t, double *aIm_t);

  /**
   * @brief Performs the forward FFT of a time-domain purely real data vector.
   * @details The input data vector is copied before being transformed.
   * @note This function can be called both in a "c2c" and a "r2c" mode. In a "c2c" mode, the imaginary part is set to 0.
   * @param[in] aRe_t Time-domain real data vector.
   * @pre The input pointer must point to an array the size of which must match the definition given in the constructor.
   */
  bool Forward(double *aRe_t);

  /**
   * @brief Performs the backward FFT of a frequency-domain complex data vector.
   * @details The input data vector is defined by 2 arrays for the real and imaginary parts.
   * The input data vector is copied before being transformed.
   * @param[in] aRe_f Frequency-domain data vector (real part).
   * @param[in] aIm_f Frequency-domain data vector (imaginary part).
   * @pre The input pointers must point to arrays the size of which must match the definition given in the constructor.
   */
  bool Backward(double *aRe_f, double *aIm_f);

  /**
   * @brief Performs the backward FFT of a frequency-domain purely real data vector.
   * @details The input data vector is copied before being transformed.
   * @param[in] aRe_f Frequency-domain real data vector.
   */
  bool Backward(double *aRe_f);

  /**
   * @brief Returns the squared norm of a frequency-domain data vector element.
   * @param[in] aIndex Vector index.
   */
  inline double GetNorm2_f(const unsigned int aIndex){
    return x_f[aIndex][0]*x_f[aIndex][0] + x_f[aIndex][1]*x_f[aIndex][1];
  };

  /**
   * @brief Returns the squared norm of a time-domain data vector element.
   * @param[in] aIndex Vector index.
   */
  inline double GetNorm2_t(const unsigned int aIndex){
    if(fType) return xc_t[aIndex][0]*xc_t[aIndex][0] + xc_t[aIndex][1]*xc_t[aIndex][1];
    return xr_t[aIndex]*xr_t[aIndex];
  };

  /**
   * @brief Returns the norm of a frequency-domain data vector element.
   * @param[in] aIndex Vector index.
   */
  inline double GetNorm_f(const unsigned int aIndex){
    return sqrt(GetNorm2_f(aIndex));
  };

  /**
   * @brief Returns the norm of a time-domain data vector element.
   * @note For "r2c", prefer the use of GetRe_t() (faster).
   * @param[in] aIndex Vector index.
   */
  inline double GetNorm_t(const unsigned int aIndex){
    return sqrt(GetNorm2_t(aIndex));
  };

  /**
   * @brief Returns the real part of a frequency-domain data vector element.
   * @param[in] aIndex Vector index.
   */
  inline double GetRe_f(const unsigned int aIndex){
    return x_f[aIndex][0];
  };

  /**
   * @brief Returns the real part of a time-domain data vector element.
   * @param[in] aIndex Vector index.
   */
  inline double GetRe_t(const unsigned int aIndex){
    if(fType) return xc_t[aIndex][0];
    return xr_t[aIndex];
  };

  /**
   * @brief Returns the imaginary part of a frequency-domain data vector element.
   * @param[in] aIndex Vector index.
   */
  inline double GetIm_f(const unsigned int aIndex){
    return x_f[aIndex][1];
  };

  /**
   * @brief Returns the imaginary part of a time-domain data vector element.
   * @param[in] aIndex vector index
   */
  inline double GetIm_t(const unsigned int aIndex){
    if(fType) return xc_t[aIndex][1];
    return 0.0;
  };

  /**
   * @brief Returns the phase of a frequency-domain data vector element.
   * @details The returned phase is between \f$-\pi\f$ and \f$-\pi\f$.
   * @param[in] aIndex Vector index.
   */
  inline double GetPhase_f(const unsigned int aIndex){
    return atan2(x_f[aIndex][1], x_f[aIndex][0]);
  };

  /**
   * @brief Returns the phase of a time-domain data vector element.
   * @details The returned phase is between \f$-\pi\f$ and \f$-\pi\f$.
   * @param[in] aIndex vector index
   */
  inline double GetPhase_t(const unsigned int aIndex){
    if(fType) return atan2(xc_t[aIndex][1], xc_t[aIndex][0]);
    else return 0.0;
  };

  /**
   * @brief Returns the squared norm of the frequency-domain data vector.
   * @details The user is in charge of deleting the returned array.
   * @param[in] aNorm The returned vector is normalized by this parameter.
   */
  double* GetNorm2_f(const double aNorm=1.0);

  /**
   * @brief Returns the squared norm of the time-domain data vector.
   * @details The user is in charge of deleting the returned array.
   * @param[in] aNorm The returned vector is normalized by this parameter.
   */
  double* GetNorm2_t(const double aNorm=1.0);

  /**
   * @brief Returns the norm of the frequency-domain data vector.
   * @details The user is in charge of deleting the returned array.
   * @param[in] aNorm The returned vector is normalized by this parameter.
   */
  double* GetNorm_f(const double aNorm=1.0);

  /**
   * @brief Returns the norm of the time-domain data vector.
   * @details The user is in charge of deleting the returned array.
   * @param[in] aNorm The returned vector is normalized by this parameter.
   */
  double* GetNorm_t(const double aNorm=1.0);

  /**
   * @brief Returns the real part of the frequency-domain data vector.
   * @details The user is in charge of deleting the returned array.
   * @param[in] aNorm The returned vector is normalized by this parameter.
   */
  double* GetRe_f(const double aNorm=1.0);

  /**
   * @brief Returns the real part of the time-domain data vector.
   * @details The user is in charge of deleting the returned array.
   * @param[in] aNorm The returned vector is normalized by this parameter.
   */
  double* GetRe_t(const double aNorm=1.0);

  /**
   * @brief Returns the imaginary part of the frequency-domain data vector.
   * @details The user is in charge of deleting the returned array.
   * @param[in] aNorm The returned vector is normalized by this parameter.
   */
  double* GetIm_f(const double aNorm=1.0);

  /**
   * @brief Returns the imaginary part of the time-domain data vector.
   * @details The user is in charge of deleting the returned array.
   * @param[in] aNorm The returned vector is normalized by this parameter.
   */
  double* GetIm_t(const double aNorm=1.0);

  /**
   * @brief Returns the phase of the frequency-domain data vector.
   * @details The user is in charge of deleting the returned array.
   */
  double* GetPhase_f(void);

  /**
   * @brief Returns the phase of the time-domain data vector.
   * @details The user is in charge of deleting the returned array.
   */
  double* GetPhase_t(void);

  /**
   * @brief Returns the size of the time-domain vector.
   */
  inline unsigned int GetSize_t(void){ return fSize_t; };

  /**
   * @brief Returns the size of the frequency-domain vector.
   */
  inline unsigned int GetSize_f(void){ return fSize_f; };

  /**
   * @brief Sets a time-domain vector element (real part) to a given value.
   * @param[in] aIndex Vector index.
   * @param[in] aValue New value.
   */
  inline void SetRe_t(const unsigned int aIndex, const double aValue){
    if(fType) xc_t[aIndex][0]=aValue;
    else      xr_t[aIndex]=aValue;
  };

  /**
   * @brief Sets a time-domain vector element (imaginary part) to a given value.
   * @param[in] aIndex Vector index.
   * @param[in] aValue New value.
   */
  inline void SetIm_t(const unsigned int aIndex, const double aValue){
    xc_t[aIndex][1]=aValue;
  };

  /**
   * @brief Sets a frequency-domain vector element (real part) to a given value.
   * @param[in] aIndex Vector index.
   * @param[in] aValue New value.
   */
  inline void SetRe_f(const unsigned int aIndex, const double aValue){
    x_f[aIndex][0]=aValue;
  };

  /**
   * @brief Sets a frequency-domain vector element (imaginary part) to a given value.
   * @param[in] aIndex Vector index.
   * @param[in] aValue New value.
   */
  inline void SetIm_f(const unsigned int aIndex, const double aValue){
    x_f[aIndex][1]=aValue;
  };

  
 private:
  unsigned int fSize_t;    ///< Time-domain vector size.
  unsigned int fSize_f;    ///< Frequency-domain vector size.
  string fPlan;            ///< FFTW plan.
  bool fType;              ///< FFT type true=c2c, false=r2c.

  fftw_complex* xc_t;      ///< Time-domain data - complex.
  double* xr_t;            ///< Time-domain data - real.
  fftw_complex* x_f;       ///< Frequency-domain data.
  fftw_plan FFT_Forward;   ///< Forward plan.
  fftw_plan FFT_Backward;  ///< Backward plan.
};


#endif
