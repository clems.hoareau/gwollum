/**
 * @file 
 * @brief See Chain.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Chain.h"

ClassImp(Chain)

////////////////////////////////////////////////////////////////////////////////////
Chain::Chain(const string aChainName, const string aPattern, const bool aVerbose):
TChain(aChainName.c_str()) {
////////////////////////////////////////////////////////////////////////////////////

  fchainname = aChainName;
  fverbose = aVerbose;
  rootfilename.clear();
  spattern = aPattern;

  vector <string> sub_pattern = SplitString(aPattern,' ');

  // special case of Xrootd / posix
  if(sub_pattern.size()){
    if((sub_pattern[0].substr(0,5)).compare("root:")) ChainPOSIX(aPattern);
    else ChainXROOTD(sub_pattern);
  }

  sub_pattern.clear();
}

////////////////////////////////////////////////////////////////////////////////////
Chain::~Chain(void){}
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
void Chain::ChainPOSIX(const string aRootFilePattern){
////////////////////////////////////////////////////////////////////////////////////

  string ls;
  ls = "ls -U "+aRootFilePattern;
  
  if(fverbose) cout<<"Opening root files in "<<aRootFilePattern<<"..."<<endl;
    
  FILE *in;
  in = popen(ls.c_str(), "r");

  char filename[100000];
  while(fscanf(in,"%s",filename) != EOF){
    if(fverbose) cerr<<filename<<" ... ";
    TFile *tmp = new TFile(filename);
    if( !tmp || tmp->IsZombie() ){
      if(fverbose) cerr<<"not a root file"<<endl;
      continue;
    }
    if( tmp->ReadKeys() ){
      tmp->Close();
      if(int retcode = TChain::AddFile(filename,-1)){
        if(fverbose) cerr<<"OK! "<<retcode<<endl;
	rootfilename.push_back((string)filename);
      }
      else if(fverbose) cerr<<fchainname<<" not found"<<endl;
    }
    else 
      if(fverbose) cerr<<"No Keys !"<<endl;
  }
  pclose(in);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Chain::ChainXROOTD(const vector <string> aRootFiles){
////////////////////////////////////////////////////////////////////////////////////
 
  for(unsigned int f=0; f<aRootFiles.size(); f++){
    if(fverbose) cerr<<aRootFiles[f]<<" ... ";
    TFile *tmp = TFile::Open(aRootFiles[f].c_str());
    if( !tmp || tmp->IsZombie() ){
      if(fverbose) cerr<<"not a root file"<<endl;
      continue;
    }
    if( tmp->ReadKeys() ){
      tmp->Close();
      if(int retcode = TChain::AddFile(aRootFiles[f].c_str(),-1)){
        if(fverbose) cerr<<"OK! "<<retcode<<endl;
	rootfilename.push_back(aRootFiles[f]);
      }
      else if(fverbose) cerr<<fchainname<<" not found"<<endl;
    }
    else 
      if(fverbose) cerr<<"No Keys !"<<endl;
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Chain::AddFile(const string aPattern){
////////////////////////////////////////////////////////////////////////////////////

  vector <string> sub_pattern = SplitString(aPattern,' ');

  // special case of Xrootd / posix
  if(sub_pattern.size()){
    if((sub_pattern[0].substr(0,5)).compare("root:")) ChainPOSIX(aPattern);
    else ChainXROOTD(sub_pattern);
  }

  return;
}
