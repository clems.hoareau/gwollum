# -- versioning -----------
configure_file(${CMAKE_CURRENT_LIST_DIR}/Config.h.in
  ${CMAKE_CURRENT_LIST_DIR}/Config.h
  @ONLY)

# -- CUtils library -----------

set(
  GWOLLUM_CUTILS_HEADERS
  Config.h
  CUtils.h
  FFT.h
  IO.h
  ReadAscii.h
  )
add_library(
  CUtils SHARED
  CUtils.cc
  FFT.cc
  IO.cc
  ReadAscii.cc
  )
target_include_directories(
  CUtils
  PUBLIC
  ${FFTW_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}
  )
target_link_libraries(
  CUtils
  ${FFTW_LIBRARIES}
  )
set_target_properties(
  CUtils PROPERTIES
  PUBLIC_HEADER "${GWOLLUM_CUTILS_HEADERS}"
  )

# -- RootUtils library -------
#
# This set up is a bit weird, mainly to prevent having hard-coded source
# paths in the compiled SOs. I think this is fixed in root-6.18.0
# https://root.cern/doc/v618/release-notes.html#header-location-and-root_generate_dictionary-root_standard_library_package

set(
  GWOLLUM_ROOTUTILS_HEADERS
  GwollumPlot.h
  Chain.h
  )

# build ROOT dictionary
root_generate_dictionary(
  RootUtilsDict
  LINKDEF LinkDef.h
  MODULE RootUtils
  OPTIONS ${GWOLLUM_ROOTUTILS_HEADERS}
  )

# compile library
add_library(
  RootUtils
  SHARED
  Chain.cc
  GwollumPlot.cc
  RootUtilsDict.cxx
  )
target_include_directories(
  RootUtils
  PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  )
target_link_libraries(
  RootUtils
  CUtils
  ${ROOT_Core_LIBRARY}
  ${ROOT_Gpad_LIBRARY}
  ${ROOT_Graf_LIBRARY}
  ${ROOT_Hist_LIBRARY}
  ${ROOT_MathCore_LIBRARY}
  ${ROOT_RIO_LIBRARY}
  ${ROOT_Tree_LIBRARY}
  )
set_target_properties(
  RootUtils PROPERTIES
  PUBLIC_HEADER "${GWOLLUM_ROOTUTILS_HEADERS}"
  )

# -- executables ------------

add_executable(
  gwl-txt-to-tree
  gwl-txt-to-tree.cc
  )
target_link_libraries(
  gwl-txt-to-tree
  ${ROOT_Core_LIBRARY}
  ${ROOT_RIO_LIBRARY}
  ${ROOT_Tree_LIBRARY}
  CUtils
  )

add_executable(
  gwl-txt-to-frame
  gwl-txt-to-frame.cc
  )
target_link_libraries(
  gwl-txt-to-frame
  CUtils
  ${FRAMEL_LIBRARIES}
  )
target_include_directories(
  gwl-txt-to-frame
  PRIVATE
  ${FRAMEL_INCLUDE_DIRS}
  )

add_executable(
  gwl-test-cutils
  gwl-test-cutils.cc
  )
target_link_libraries(
  gwl-test-cutils
  CUtils
  )
add_test(
  NAME gwl-test-cutils
  COMMAND gwl-test-cutils
  )

# -- installation -----------

# install libraries
install(
  TARGETS
  CUtils
  RootUtils
  RUNTIME DESTINATION ${RUNTIME_DESTINATION}
  LIBRARY DESTINATION ${LIBRARY_DESTINATION}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  )

# install ROOT PCM
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/libRootUtils_rdict.pcm
  ${CMAKE_CURRENT_BINARY_DIR}/libRootUtils.rootmap
  DESTINATION ${CMAKE_INSTALL_LIBDIR}
  )

# install executable(s)
install(
  TARGETS
  gwl-txt-to-tree
  gwl-txt-to-frame
  DESTINATION ${CMAKE_INSTALL_BINDIR}
  )

# install test executable(s)
install(
  TARGETS
  gwl-test-cutils
  DESTINATION ${CMAKE_INSTALL_SBINDIR}/
  )

