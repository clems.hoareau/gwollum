/**
 * @file 
 * @brief Option file parser.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __IO__
#define __IO__

#include "CUtils.h"

using namespace std;

/**
 * @brief Parse option files.
 * @details This class is designed to read an option file and to extract parameters.
 * Options are uniquely defined by a tag and a keyword.
 *
 * An option is defined by a single line:
 * \verbatim TAG  KEY  [value] \endverbatim
 * The [value] can be of any C++ type (string, integer...)
 *
 * It is also possible to have more than one value:
 * \verbatim TAG  KEY  [value1]   [value2]   [value3]   [value4] \endverbatim
 * In that case, values are stored in a vector container.
 *
 * Option lines can be commented out when lines start with the following characters: '//', '\%' or '#'
 * @author Florent Robinet
 */
class IO {
  
public:
  
  /**
   * @name Constructors and destructors
   @{
  */

  /**
   * @brief Constructor of the IO class.
   * @details It loads and reads the option files listed in filepattern.
   * @param[in] filepattern File pattern pointing to the option files to be parsed.
   */
  IO(const char* filepattern);

  /**
   * Destructor of the IO class.
   */
  virtual ~IO(void);
 
  /**
     @}
  */

 
  /**
   * @brief Dumps all options.
   * @details By default it is dumped in the standard output.
   * @param[in] out Ouput stream.
   */
  void Dump(ostream& out = cout) const;
  
  /**
   * @brief Flags the parsing sequence.
   * @details This function returns false if the option files could not be parsed correctly.
   */
  inline bool IsZombie(void) const {return !fAllLines.size();}

  /**
   * @brief Flux operator wrapper.
   * @param[in] out Ouput stream.
   * @param[in] io IO object.
   */
  friend ostream& operator<<(ostream& out, IO& io);
  
  /**
   * @brief Gets an option vector.
   * @details This template function fills a vector with values defined by a tag and a keyword.
   * false is returned if the option is not found.
   * @param[in] tag tag name.
   * @param[in] key keyword.
   * @param[out] values returned vector.
  */
  template <class T>
    bool GetOpt(const char* tag, const char* key, vector< T >& values) const; 
  
  /**
   * @brief Gets an option.
   * @details This template function gets an option value defined by a tag and a keyword.
   * false is returned if the option is not found.
   * @param[in] tag tag name.
   * @param[in] key keyword.
   * @param[out] value returned value.
  */
  template <class T>
    bool GetOpt(const char* tag, const char* key, T& value) const; 
  
  /**
   * @brief Gets an option of a string type.
   * @details This function gets an option string value defined by a tag and a keyword.
   * false is returned if the option is not found.
   * @param[in] tag tag name.
   * @param[in] key keyword.
   * @param[out] value returned string value.
  */
  bool GetOpt(const char* tag, const char* key, string& value) const;
  
  /**
   * @brief Gets a vector of all options.    
   * @details This template function fills a vector with values defined by a tag and a keyword.
   * This function gets all options matching a tag and a keyword.
   * The combination of tag/keyword can be used over several lines.
   * false is returned if the option is not found.
   * @param[in] tag tag name.
   * @param[in] key keyword.
   * @param[out] values option value vector.
   */
  template <class T>  
    bool GetAllOpt(const char* tag, const char* key, vector< T >& values);
  
  /**
   * @brief Gets options iteratively.
   * @details This function gets options matching a tag and a keyword.
   * This function is useful when a combination of tag/keyword is repeated over several lines.
   * At each call of this function, the next option value is returned.
   * false is returned if the option is not found.
   * @param[in] tag tag name.
   * @param[in] key keyword.
   * @param[out] value option value.
   */
  template <class T>
    bool GetAllOpt(const char* tag, const char* key, T& value); 

  /**
   * @brief Returns the option line defined by a tag and a keyword.
   * @details If several lines match the tag and keyword, only the last line is retained.
   * @param[in] tag tag name.
   * @param[in] key keyword.
  */
  string GetLineData(const char* tag, const char* key) const;
  
  /**
   * @brief Returns the next option line defined by a tag and a keyword.
   * @details This function can be called many times (in a row!) to read several lines defined by the same tag/keyword.
   * @param[in] tag tag name.
   * @param[in] key keyword.
   */
  string GetNextLineData(const char* tag, const char* key);

private:
  
  vector< pair<string,string> > fAllLines; ///< List of lines.
  bool ParseFile(const char* filename);    ///< File parser.
  unsigned int fCurline;                   ///< Current line number.
  string fCurkey;                          ///< Current key.
  string fCurtag;                          ///< Current tag.
  static const unsigned int sLinesize = 2048;///< Maximum number of characters in a line.


};

//////////////////////////////////////////////////////////////////////////////
template <class T>
bool IO::GetOpt(const char* tag, const char* key, vector< T >& values) const {
//////////////////////////////////////////////////////////////////////////////
  string data = GetLineData(tag,key);
 
  istringstream in(data.c_str());  
  while(1) {
    T tmp;
    in>>tmp;
    if(!in) break;
    values.push_back(tmp);
  } 
  
  return (bool)values.size();
}

//////////////////////////////////////////////////////////////////////////////
template <class T>
bool IO::GetOpt(const char* tag, const char* key, T& value) const{
//////////////////////////////////////////////////////////////////////////////
  string data = GetLineData(tag,key);
  
  istringstream in(data.c_str());  
  in>>value;
  return (!!in)&&data.size();
}

//////////////////////////////////////////////////////////////////////////////
template <class T>
bool IO::GetAllOpt(const char* tag, const char* key, vector< T >& values){
//////////////////////////////////////////////////////////////////////////////
 
  string data;
  T tmp;
  while(1) {//  loop over lines
    data=GetNextLineData(tag, key);
    if(!data.compare("")) break;
    istringstream inval(data.c_str());  
    while(1) {// loop over values
      inval>>tmp;
      if(!inval) break;
      values.push_back(tmp);
    }
    inval.clear();
  } 

  return (bool)values.size();
}

//////////////////////////////////////////////////////////////////////////////
template <class T>
bool IO::GetAllOpt(const char* tag, const char* key, T& value){
//////////////////////////////////////////////////////////////////////////////
  string data = GetNextLineData(tag, key);
  if(data.empty()) return false;
  
  istringstream in(data.c_str());  
  in>>value;
  if(in) {
    return true;
  }
  else {
    return false;  
  }
}


#endif
