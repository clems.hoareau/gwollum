/*
*  Copyright (C) 2012 Karl Wette
*  Copyright (C) 2007 Bernd Machenschalk, Jolien Creighton, Kipp Cannon
*/
#include "Date.h"

static int delta_tai_utc(unsigned int gpssec){
  
  for(unsigned int leap = 1; leap < numleaps; ++leap) {
    if ( gpssec == leaps[leap].gpssec )
      return leaps[leap].taiutc - leaps[leap-1].taiutc;
  }  
  return 0;
}
  
int LeapSeconds(const unsigned int aGps){

  if((int)aGps<leaps[0].gpssec) return 0;

  /* scan leap second table and locate the appropriate interval */
  unsigned int leap = 1;
  for (; leap < numleaps; ++leap)
    if (aGps<(unsigned int)leaps[leap].gpssec) return leaps[leap-1].taiutc;

  return leaps[leap-1].taiutc;
}

int LeapSecondsUTC( const struct tm *aUtc){
    
  double jd = JulianDay(aUtc);

  /* scan leap second table and locate the appropriate interval */
  unsigned int leap = 1;
  for (; leap < numleaps; ++leap)
    if (jd < leaps[leap].jd) return leaps[leap-1].taiutc;

  return leaps[leap-1].taiutc;
}

struct tm * GPSToUTC(struct tm *aUtc, unsigned int aGps){
  int leapsec = LeapSeconds(aGps);
  time_t unixsec  = (int)aGps - leapsec + EPOCH_GPS_TAI_UTC; /* get rid of leap seconds */
  unixsec += EPOCH_UNIX_GPS; /* change to unix epoch */
  memset(aUtc, 0, sizeof( *aUtc ) ); /* blank out utc structure */
  aUtc = gmtime_r( &unixsec, aUtc ); /* FIXME: use gmtime ?? */
  /* now check to see if we need to add a 60th second to UTC */
  if (delta_tai_utc(aGps) > 0)
    aUtc->tm_sec += 1; /* delta only ever is one, right?? */
  return aUtc;
}

unsigned int UTCToGPS(const struct tm *aUtc){

  /* compute leap seconds */
  int leapsec = LeapSecondsUTC(aUtc);
  time_t unixsec = aUtc->tm_sec + aUtc->tm_min*60 + aUtc->tm_hour*3600
    + aUtc->tm_yday*86400 + (aUtc->tm_year-70)*31536000
    + ((aUtc->tm_year-69)/4)*86400 - ((aUtc->tm_year-1)/100)*86400
    + ((aUtc->tm_year+299)/400)*86400;
  int gpssec  = unixsec;
  gpssec -= EPOCH_UNIX_GPS; /* change to gps epoch */
  gpssec += leapsec - EPOCH_GPS_TAI_UTC;
  /* now check to see if this is an additional leap second */
  if ( aUtc->tm_sec == 60 ) --gpssec;

  if(gpssec<0) return 0;
  return (unsigned int)gpssec;
}

double JulianDay(const struct tm *aUtc){
  const int sec_per_day = 60 * 60 * 24; /* seconds in a day */
  
  /* this routine only works for dates after 1900 */
  if (aUtc->tm_year <= 0) return 0.0;

  int year  = aUtc->tm_year + 1900;
  int month = aUtc->tm_mon + 1;     /* month is in range 1-12 */
  int day   = aUtc->tm_mday;        /* day is in range 1-31 */
  int sec   = aUtc->tm_sec + 60*(aUtc->tm_min + 60*(aUtc->tm_hour)); /* seconds since midnight */
  
  double jd = (double)(367*year - 7*(year+ (month + 9)/12)/4 + 275*month/9 + day + 1721014);

  /* note: Julian days start at noon: subtract half a day */
  jd += (double)sec/(double)sec_per_day - 0.5;
  
  return jd;
}

string GetTimeStampFromGps(const unsigned int aGps){

  struct tm utc;
  GPSToUTC(&utc, aGps);
  char buffer [80];
  strftime(buffer, 80, "%Y-%b-%d %H:%M:%S" , &utc);
  return (string)buffer;
}
