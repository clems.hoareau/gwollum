# -- Time library -----------

set(GWOLLUM_TIME_HEADERS Date.h LeapSeconds.h TimeDelay.h)

add_library(
  Time
  SHARED
  CivilTime.cc
  SiderealTime.cc
  TimeDelay.cc
  )
target_link_libraries(
  Time
  CUtils
  )
set_target_properties(
  Time
  PROPERTIES
  PUBLIC_HEADER "${GWOLLUM_TIME_HEADERS}"
  )
install(
  TARGETS Time
  RUNTIME DESTINATION ${RUNTIME_DESTINATION}
  LIBRARY DESTINATION ${LIBRARY_DESTINATION}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  )
