//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __Search__
#define __Search__

#include "IO.h"
#include "SearchRead.h"

using namespace std;

/**
 * Perform and evaluate a GW search.
 * \author    Florent Robinet
 */
class Search: public GwollumPlot {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the Search class.
   * A GW search is conducted over search files produced with the MorphMatch class. These files must be distributed in a set of directories called "search directories". The coinc events in the search files in a given search directory must be time-sorted (usually, 1 search directory = 1 time offset). Search directories can contain either non-injection (Rank class) or injection (InjCoi) files. 
   *
   * This constructor takes an option file as argument to specify the path to search directories. Additionnaly, a binning must be specified to build the search plots.
   * @param aOptionFile path to option file
   * @param aVerbose verbosity level
   */
  Search(const int aVerbose=0);

  /**
   * Destructor of the Search class.
   */
  virtual ~Search(void);
  /**
     @}
  */
  
  inline void SetDetectionMorphMatchThreshold(const double aThr){
    h1_monitor->SetBinContent(7,aThr);
  }
  inline void SetDetectionSumw2Threshold(const double aThr){
    h1_monitor->SetBinContent(8,aThr);
  }

  bool Process(const vector <string> aSearchReadFilePattern);
  bool ProcessSearch(const string aSearchFileName);
  
  void PrintPlots(const string aPlotNames);

  void WriteSearch(const string aOutDir, const string aOutFileName);
  void SetMorphMatchBins(const int aN);
  void SetSumw2Bins(const int aN, const double aMin, const double aMax);
  void SetFreqBins(const int aN, const double aMin, const double aMax);
  void SetInjAmpBins(const int aN, const double aMin, const double aMax);

  inline void SetMorphMatchSelection(const double aMin, const double aMax){
    MorphMatchSelect[0]=aMin;
    MorphMatchSelect[1]=aMax;
  };
  inline void SetMorphMatchShiftSelection(const double aMin, const double aMax){
    MorphMatchShiftSelect[0]=aMin;
    MorphMatchShiftSelect[1]=aMax;
  };
  inline void SetMorphMatchSumw2Selection(const double aMin, const double aMax){
    MorphMatchSumw2Select[0]=aMin;
    MorphMatchSumw2Select[1]=aMax;
  };
  inline void SetCoincFrequencySelection(const double aMin, const double aMax){
    CoincFreqSelect[0]=aMin;
    CoincFreqSelect[1]=aMax;
  };

 private:

  int verbosity;                    ///< verbosity level

  // general monitor
  TH1D *h1_monitor;

  // coinc selection
  double MorphMatchSelect[2];
  double MorphMatchSumw2Select[2];
  double MorphMatchShiftSelect[2];
  double CoincFreqSelect[2];

  // event plots
  void MakeMorphMatchPlots(void);
  TH1D *h1_morphmatch[3];
  void MakeMorphMatchSumw2Plots(void);
  TH2D *h2_morphmatch_sumw2[3];
  void MakeMorphMatchFreqPlots(void);
  TH2D *h2_morphmatch_freq0[3];
  TH2D *h2_morphmatch_freq1[3];

  // event rate plots
  void MakeRatePlots(void);
  TH1D *h1_morphmatch_rate[3];
  TH2D *h2_morphmatch_sumw2_rate[3];

  // injection recovery plots
  void MakeInjectionsPlots(void);
  TH1D *h1_injamp_val;
  TH1D *h1_injamp_rec;

  // recovery efficiency plots
  void MakeEfficiencyPlots(void);
  TH1D *h1_injamp_eff;

  // contours
  TGraph *isocont[10];

  // binning
  int n_morphmatchbins;
  int n_sumw2bins;
  double *sumw2bins;
  int n_fbins;
  double *fbins;
  int n_injampbins;
  double *injampbins;

  ClassDef(Search,0)
};

#endif


