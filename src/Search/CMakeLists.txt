# -- Search library ---------

set(
    GWOLLUM_SEARCH_HEADERS
    Search.h
    SearchRead.h
    SearchTune.h
)

# build ROOT dictionary
include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/../Inject
    ${CMAKE_CURRENT_SOURCE_DIR}/../Streams
    ${CMAKE_CURRENT_SOURCE_DIR}/../Segments
    ${CMAKE_CURRENT_SOURCE_DIR}/../Time
    ${CMAKE_CURRENT_SOURCE_DIR}/../Triggers
    ${CMAKE_CURRENT_SOURCE_DIR}/../Utils
)
root_generate_dictionary(
    SearchDict
    LINKDEF LinkDef.h
    MODULE Search
    OPTIONS ${GWOLLUM_SEARCH_HEADERS} -I${CMAKE_CURRENT_SOURCE_DIR}
)

# compile C library
add_library(
    Search
    SHARED
    Search.cc
    SearchDict.cxx
    SearchRead.cc
    SearchTune.cc
)
target_include_directories(
    Search
    PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
)
target_link_libraries(
    Search
    Inject
)
set_target_properties(
    Search PROPERTIES
    PUBLIC_HEADER "${GWOLLUM_SEARCH_HEADERS}"
)

# -- executables ------------

add_executable(
    runsearch
    runsearch.cc
)
target_link_libraries(
    runsearch
    ${ROOT_Core_LIBRARY}
    Search
)

# -- installation -----------

# install library
install(
    TARGETS
    Search
    RUNTIME DESTINATION ${RUNTIME_DESTINATION}
    LIBRARY DESTINATION ${LIBRARY_DESTINATION}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)

# install ROOT PCM
install(
    FILES
    ${CMAKE_CURRENT_BINARY_DIR}/libSearch_rdict.pcm
    ${CMAKE_CURRENT_BINARY_DIR}/libSearch.rootmap
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

# install executable(s)
install(
    TARGETS
    runsearch
    DESTINATION ${CMAKE_INSTALL_BINDIR}
)
