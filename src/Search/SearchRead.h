//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __SearchRead__
#define __SearchRead__

#include "GwollumPlot.h"
#include "ReadTriggers.h"
#include "InjRea.h"

using namespace std;

/**
 * Read output files from a search. 
 * These files must be produced with the MorphMatch (or the derived InjCoi) class and contain coinc events. This class is able to detect the presence (or not) of injections with the InjRea class from which it inherits.
 *
 * This class can be used interactively to plot coinc events and associated injection parameters.
 * \author    Florent Robinet
 */
class SearchRead: public GwollumPlot, public Chain, public InjRea {
 
 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the SearchRead class.
   * Coinc events are loaded. Cluster trees (clusters_0 and clusters_1) are loaded and added as friends. They are aliased as "c0" and "c1". If injections are found, they are connected to the coinc as well.
   * @param aFilePattern coinc file pattern, morphmatch parameters must exist
   * @param aVerbose verbosity level
   */
  SearchRead(const string aFilePattern, const int aVerbose=0);

  /**
   * Destructor of the SearchRead class.
   */
  virtual ~SearchRead(void);
  /**
     @}
  */

  /**
   * Draws clusters and coinc parameters.
   * This function is an overload of the ROOT function: TChain::Draw().
   */
  inline Long64_t Draw(const string varexp, const string selection="", string option=""){
    Wpad->cd();
    return Chain::Draw(varexp.c_str(), selection.c_str(), option.c_str());
  };
  
  /**
   * Returns the event time for trigger sample 0.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincTime0(void) {return triggers_0->Ttime;};

  /**
   * Returns the event time for trigger sample 1.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincTime1(void) {return triggers_1->Ttime;};

  /**
   * Returns the event SNR for trigger sample 0.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincSNR0(void) {return triggers_0->Tsnr;};

  /**
   * Returns the event SNR for trigger sample 1.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincSNR1(void) {return triggers_1->Tsnr;};

  /**
   * Returns the event frequency for trigger sample 0.
   */
  inline double GetCoincFrequency0(void) {return triggers_0->Tfreq;};

  /**
   * Returns the event frequency for trigger sample 1.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincFrequency1(void) {return triggers_1->Tfreq;};

  /**
   * Returns the time offset for trigger sample 0.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincTimeOffset0(void) {return toffset_0;};

  /**
   * Returns the time offset for trigger sample 1.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincTimeOffset1(void) {return toffset_1;};

  /**
   * Returns the morphology match of the current coinc.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincMorphMatch(void) {return CoMatch;};
  
  /**
   * Returns the morphology sum of weigth squared of the current coinc.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincMorphMatchSumw2(void) {return CoMatchSumw2;};
  
  /**
   * Returns the morphology match time shift of the current coinc.
   * Use GetEntry() to load a given coinc event.
   */
  inline double GetCoincMorphMatchShift(void) {return CoMatchShift;};
      
  /**
   * Returns true if this coinc best matches an injection.
   * Always returns true if there is no injections.
   */
  inline bool IsInjectionBestMatch(void) {return injbestmatch;};
    
  /**
   * Returns the matching injection index.
   * When the coinc matches an injection, the injection index is returned. If the coinc does not match ay injection, -1 is returned.
   */
  inline int GetCoincInjectionIndex(void){
    if(CoInjIndex>=0) return CoInjIndex+InjRea::GetTreeOffset()[TChain::GetTreeNumber()];
    return -1;
  };
    
  /**
   * Loads a given coinc.
   * If this coinc matches an injection, injection parameters are also loaded.
   * Use GetCoincInjectionIndex()<0 to test whether there is such a match.
   * @param aCoincIndex coinc index
   */
  inline int GetEntry(Long64_t aCoincIndex = 0, Int_t getall = 0){
    int res = TChain::GetEntry(aCoincIndex,getall);
    InjRea::LoadInjection(GetCoincInjectionIndex());
    return res;
  };

  /**
   * Returns the class status.
   */
  inline double GetStatus(void){ return status_OK; };

  /**
   * Returns the coinc livetime.
   */
  inline double GetCoincLiveTime(void) {
    return triggers_0->GetSegments()->GetLiveTime();
  };


 protected:
  int verbosity;               ///< verbosity level
  bool status_OK;              ///< class status
  
 private:

  ReadTriggers *triggers_0;    ///< trigger sample 0
  ReadTriggers *triggers_1;    ///< trigger sample 1
  double toffset_0;            ///< time offset 0
  double toffset_1;            ///< time offset 1

  double CoMatch,              ///< Coinc match (TTree)
    CoMatchSumw2,              ///< Coinc match sum of w2 (TTree)
    CoMatchShift;              ///< Coinc match time shift (TTree)

  // INJECTION <---> COINC MATCH
  Chain *coincmap;             ///< mapping tree
  int CoInjIndex;              ///< injection index

  TTree *tinj;
  bool injbestmatch;

  ClassDef(SearchRead,0)
};

#endif


