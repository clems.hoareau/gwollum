//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "Search.h"

ClassImp(Search)

////////////////////////////////////////////////////////////////////////////////////
Search::Search(const int aVerbose): GwollumPlot("Search","FIRE") {
////////////////////////////////////////////////////////////////////////////////////

  SetGridx(1); SetGridy(1);
  verbosity=aVerbose;

  // search monitor definition
  h1_monitor = new TH1D("h1_monitor","Search monitor",8,1,9);
  h1_monitor->GetXaxis()->SetBinLabel(1,"zero-lag live-time");
  h1_monitor->GetXaxis()->SetBinLabel(2,"background live-time");
  h1_monitor->GetXaxis()->SetBinLabel(3,"injection live-time");
  h1_monitor->GetXaxis()->SetBinLabel(4,"zero-lag events above threshold");
  h1_monitor->GetXaxis()->SetBinLabel(5,"background events above threshold");
  h1_monitor->GetXaxis()->SetBinLabel(6,"injection events above threshold");
  h1_monitor->GetXaxis()->SetBinLabel(7,"Morphmatch threshold");
  h1_monitor->GetXaxis()->SetBinLabel(8,"#sum w^{2} threshold");
 
  // no selection
  SetMorphMatchSelection(-1.0, 2.0);
  SetMorphMatchSumw2Selection(-1.0, 1e20);
  SetMorphMatchShiftSelection(-1e20, 1e20);
  SetCoincFrequencySelection(-1.0, 1e10);

  // dummy binning
  n_morphmatchbins=1;
  n_sumw2bins=1;       sumw2bins = new double [2];  sumw2bins[0]=1.0;  sumw2bins[1]=10.0;
  n_fbins=1;           fbins = new double [2];      fbins[0]=1.0;      fbins[1]=10.0;
  n_injampbins=1;      injampbins = new double [2]; injampbins[0]=1.0; injampbins[1]=10.0;
      
  // dummy plots
  for(int i=0; i<3; i++){
    h1_morphmatch[i] = new TH1D();
    h2_morphmatch_sumw2[i] = new TH2D();
    h2_morphmatch_freq0[i] = new TH2D();
    h2_morphmatch_freq1[i] = new TH2D();

    h1_morphmatch_rate[i] = new TH1D();
    h2_morphmatch_sumw2_rate[i] = new TH2D();
  }
  h1_injamp_val = new TH1D();
  h1_injamp_rec = new TH1D();
  h1_injamp_eff = new TH1D();
  for(int i=0; i<10; i++) isocont[i] = new TGraph();

  // make default plots
  MakeMorphMatchPlots();
  MakeMorphMatchSumw2Plots();
  MakeMorphMatchFreqPlots();
  MakeRatePlots();
  MakeInjectionsPlots();
}

////////////////////////////////////////////////////////////////////////////////////
Search::~Search(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbosity>1) cout<<"Search::~Search"<<endl;

  delete h1_monitor;
 
  for(int i=0; i<3; i++){
    delete h1_morphmatch[i];
    delete h2_morphmatch_sumw2[i];
    delete h2_morphmatch_freq0[i];
    delete h2_morphmatch_freq1[i];
    delete h1_morphmatch_rate[i];
    delete h2_morphmatch_sumw2_rate[i];
  }
  
  delete h1_injamp_val;
  delete h1_injamp_rec;
  delete h1_injamp_eff;
  for(int i=0; i<10; i++) delete isocont[i];

  delete sumw2bins;
  delete fbins;
  delete injampbins;

}

////////////////////////////////////////////////////////////////////////////////////
bool Search::Process(const vector <string> aSearchReadFilePattern){
////////////////////////////////////////////////////////////////////////////////////

  // locals
  SearchRead *SR;
  int t;
  double morphmatch_thr = h1_monitor->GetBinContent(7);
  double sumw2_thr = h1_monitor->GetBinContent(8);

  // loop over searches
  for(int s=0; s<(int)aSearchReadFilePattern.size(); s++){
   
    // get search files
    SR = new SearchRead(aSearchReadFilePattern[s]);
    if(!SR->GetStatus()){
      delete SR;
      continue;
    }
   
    // no live-time
    if(SR->GetCoincLiveTime()==0.0){
      delete SR;
      continue;
    }
   
    // identify type
    if(SR->InjRea::GetNInjections()>0) t=2;// injections
    else if(SR->GetCoincTimeOffset0()==0.0&&SR->GetCoincTimeOffset1()==0.0) t=0;// zero-lag
    else t=1;// background

    // update livetime
    h1_monitor->SetBinContent(t+1,h1_monitor->GetBinContent(t+1)+SR->GetCoincLiveTime());

    // valid injections
    for(int i=0; i<SR->InjRea::GetNInjections(); i++){
      SR->InjRea::LoadInjection(i);
      h1_injamp_val->Fill(SR->InjRea::GetInjectionAmplitude());
    }
    
    // loop over coincs
    for(int c=0; c<SR->GetEntries(); c++){
      SR->GetEntry(c);

      // select bestmatch coinc (only for injections)
      if(!SR->IsInjectionBestMatch()) continue;

      // coinc selection
      if(SR->GetCoincFrequency0()<CoincFreqSelect[0]) continue;
      if(SR->GetCoincFrequency0()>=CoincFreqSelect[1]) continue;
      if(SR->GetCoincFrequency1()<CoincFreqSelect[0]) continue;
      if(SR->GetCoincFrequency1()>=CoincFreqSelect[1]) continue;
      if(SR->GetCoincMorphMatch()<MorphMatchSelect[0]) continue;
      if(SR->GetCoincMorphMatch()>=MorphMatchSelect[1]) continue;
      if(SR->GetCoincMorphMatchSumw2()<MorphMatchSumw2Select[0]) continue;
      if(SR->GetCoincMorphMatchSumw2()>=MorphMatchSumw2Select[1]) continue;
      if(SR->GetCoincMorphMatchShift()<MorphMatchShiftSelect[0]) continue;
      if(SR->GetCoincMorphMatchShift()>=MorphMatchShiftSelect[1]) continue;
      
      // fill plots
      h1_morphmatch[t]->Fill(SR->GetCoincMorphMatch());
      h2_morphmatch_sumw2[t]->Fill(SR->GetCoincMorphMatchSumw2(),SR->GetCoincMorphMatch());
      h2_morphmatch_freq0[t]->Fill(SR->GetCoincFrequency0(),SR->GetCoincMorphMatch());
      h2_morphmatch_freq1[t]->Fill(SR->GetCoincFrequency1(),SR->GetCoincMorphMatch());

      // detection cut
      if(SR->GetCoincMorphMatchSumw2()>=sumw2_thr&&SR->GetCoincMorphMatch()>=morphmatch_thr){
	h1_monitor->Fill(4.5+(double)t);

	// injection is recovered
	if(SR->GetCoincInjectionIndex()>=0){
	  h1_injamp_rec->Fill(SR->GetInjectionAmplitude());
	}
      }

    }
    delete SR;
  }

  // derived plots
  MakeRatePlots();
  MakeEfficiencyPlots();


  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Search::ProcessSearch(const string aSearchFileName){
////////////////////////////////////////////////////////////////////////////////////

  string si[3] = {"0","1","2"};
  
  TFile fin(aSearchFileName.c_str(),"READ");
  if(fin.IsZombie()) return false;

  // get histograms from file
  for(int i=0; i<3; i++){

    delete h1_morphmatch[i];
    fin.GetObject(("h1_morphmatch_"+si[i]).c_str(),h1_morphmatch[i]);
    if (h1_morphmatch[i]) h1_morphmatch[i]->SetDirectory(0);
    else return false;

    delete h2_morphmatch_sumw2[i];
    fin.GetObject(("h2_morphmatch_sumw2_"+si[i]).c_str(),h2_morphmatch_sumw2[i]);
    if (h2_morphmatch_sumw2[i]) h2_morphmatch_sumw2[i]->SetDirectory(0);
    else return false;

    delete h2_morphmatch_freq0[i];
    fin.GetObject(("h2_morphmatch_freq0_"+si[i]).c_str(),h2_morphmatch_freq0[i]);
    if (h2_morphmatch_freq0[i]) h2_morphmatch_freq0[i]->SetDirectory(0);
    else return false;

    delete h2_morphmatch_freq1[i];
    fin.GetObject(("h2_morphmatch_freq1_"+si[i]).c_str(),h2_morphmatch_freq1[i]);
    if (h2_morphmatch_freq1[i]) h2_morphmatch_freq1[i]->SetDirectory(0);
    else return false;

  }
  
  delete h1_monitor;
  fin.GetObject("h1_monitor",h1_monitor);
  if (h1_monitor) h1_monitor->SetDirectory(0);
  else return false;

  delete h1_injamp_val;
  fin.GetObject("h1_injamp_val",h1_injamp_val);
  if (h1_injamp_val) h1_injamp_val->SetDirectory(0);
  else return false;

  delete h1_injamp_rec;
  fin.GetObject("h1_injamp_rec",h1_injamp_rec);
  if (h1_injamp_rec) h1_injamp_rec->SetDirectory(0);
  else return false;

  fin.Close();


  // derived plots
  MakeRatePlots();
  MakeEfficiencyPlots();

  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::PrintPlots(const string aPlotNames){
////////////////////////////////////////////////////////////////////////////////////

  // add livetime info
  stringstream ss;
  for(int i=0; i<3; i++){
    ss<<" (live-time = "<<fixed<<h1_monitor->GetBinContent(1+i)<<"s)";
    h1_morphmatch_rate[i]->SetTitle(((string)(h1_morphmatch[i]->GetTitle())+ss.str()).c_str());
    h2_morphmatch_sumw2_rate[i]->SetTitle(((string)(h2_morphmatch_sumw2[i]->GetTitle())+ss.str()).c_str());
    ss.clear(); ss.str("");
  }

  // add FAR info
  if(h1_monitor->GetBinContent(2)){
    ss<<" (FAR = "<<scientific<<h1_monitor->GetBinContent(5)/h1_monitor->GetBinContent(2)<<" Hz)";
    h1_injamp_eff->SetTitle(((string)(h1_injamp_rec->GetTitle())+ss.str()).c_str());
    ss.clear(); ss.str("");
  }

  // FAR contours
  for(int i=0; i<10; i++){
    delete isocont[i];
    isocont[i] = new TGraph(h2_morphmatch_sumw2_rate[1]->GetNbinsX());
    isocont[i]->SetMarkerColor(1); isocont[i]->SetLineColor(1); isocont[i]->SetLineWidth(1);
    for(int bx=1; bx<=h2_morphmatch_sumw2_rate[1]->GetNbinsX(); bx++){
      for(int by=0; by<=h2_morphmatch_sumw2_rate[1]->GetNbinsY(); by++){
	isocont[i]->SetPoint(bx-1,h2_morphmatch_sumw2_rate[1]->GetXaxis()->GetBinCenterLog(bx),h2_morphmatch_sumw2_rate[1]->GetYaxis()->GetBinUpEdge(by));
	if(h2_morphmatch_sumw2_rate[1]->GetBinContent(bx,by+1)<pow(10.0,-i)) break;
      }
    }
  }

  double bg_min_rate = (1.0/h1_monitor->GetBinContent(2))/10.0;
    
  vector <string> plots = SplitString (aPlotNames, ':');

  string ssame;
  bool first = true;
  for(int p=0; p<(int)plots.size(); p++){
    if(first) ssame="";
    else      ssame="same";

    if(!plots[p].compare("h1_morphmatch_0")){
      Draw(h1_morphmatch[0],ssame); first=false;
    }
    else if(!plots[p].compare("h1_morphmatch_1")){
      Draw(h1_morphmatch[1],ssame); first=false;
    }
    else if(!plots[p].compare("h1_morphmatch_2")){
      Draw(h1_morphmatch[2],ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_sumw2_0")){
      h2_morphmatch_sumw2[0]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_sumw2[0]->GetMaximum());
      Draw(h2_morphmatch_sumw2[0],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_sumw2_1")){
      h2_morphmatch_sumw2[1]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_sumw2[1]->GetMaximum());
      Draw(h2_morphmatch_sumw2[1],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_sumw2_2")){
      h2_morphmatch_sumw2[2]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_sumw2[2]->GetMaximum());
      Draw(h2_morphmatch_sumw2[2],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_sumw2_rate_0")){
      h2_morphmatch_sumw2_rate[0]->GetZaxis()->SetRangeUser(bg_min_rate,h2_morphmatch_sumw2_rate[0]->GetMaximum());
      Draw(h2_morphmatch_sumw2_rate[0],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_sumw2_rate_1")){
      h2_morphmatch_sumw2_rate[1]->GetZaxis()->SetRangeUser(bg_min_rate,h2_morphmatch_sumw2_rate[1]->GetMaximum());
      Draw(h2_morphmatch_sumw2_rate[1],"COLZ"+ssame); first=false;
      for(int i=0; i<10; i++) Draw(isocont[i],"Csame");
      gPad->RedrawAxis();
    }
    else if(!plots[p].compare("h2_morphmatch_sumw2_rate_2")){
      h2_morphmatch_sumw2_rate[2]->GetZaxis()->SetRangeUser(bg_min_rate,h2_morphmatch_sumw2_rate[2]->GetMaximum());
      Draw(h2_morphmatch_sumw2_rate[2],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_freq0_0")){
      h2_morphmatch_freq0[0]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_freq0[0]->GetMaximum());
      Draw(h2_morphmatch_freq0[0],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_freq0_1")){
      h2_morphmatch_freq0[1]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_freq0[1]->GetMaximum());
      Draw(h2_morphmatch_freq0[1],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_freq0_2")){
      h2_morphmatch_freq0[2]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_freq0[2]->GetMaximum());
      Draw(h2_morphmatch_freq0[2],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_freq1_0")){
      h2_morphmatch_freq1[0]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_freq1[0]->GetMaximum());
      Draw(h2_morphmatch_freq1[0],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_freq1_1")){
      h2_morphmatch_freq1[1]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_freq1[1]->GetMaximum());
      Draw(h2_morphmatch_freq1[1],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h2_morphmatch_freq1_2")){
      h2_morphmatch_freq1[2]->GetZaxis()->SetRangeUser(0.5,h2_morphmatch_freq1[2]->GetMaximum());
      Draw(h2_morphmatch_freq1[2],"COLZ"+ssame); first=false;
    }
    else if(!plots[p].compare("h1_injamp_val")){
      Draw(h1_injamp_val,ssame); first=false;
    }
    else if(!plots[p].compare("h1_injamp_rec")){
      Draw(h1_injamp_rec,ssame); first=false;
    }
    else if(!plots[p].compare("h1_injamp_eff")){
      Draw(h1_injamp_eff,ssame); first=false;
    }
    else continue;
  }
  
  plots.clear();
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::WriteSearch(const string aOutDir, const string aOutFileName){
////////////////////////////////////////////////////////////////////////////////////

  if(!IsDirectory(aOutDir)) return;

  TFile *fout = new TFile((aOutDir+"/"+aOutFileName).c_str(),"recreate");
  if(fout->IsZombie()) return;
  fout->cd();
  for(int i=0; i<3; i++){
    h1_morphmatch[i]->Write();
    h2_morphmatch_sumw2[i]->Write();
    h2_morphmatch_freq0[i]->Write();
    h2_morphmatch_freq1[i]->Write();
  }
  h1_monitor->Write();
  h1_injamp_val->Write();
  h1_injamp_rec->Write();
  fout->Close();

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::MakeMorphMatchPlots(void){
////////////////////////////////////////////////////////////////////////////////////
  delete h1_morphmatch[0];
  h1_morphmatch[0] = new TH1D("h1_morphmatch_0", "Candidate morphmatch",n_morphmatchbins,0,1);
  h1_morphmatch[0]->GetXaxis()->SetTitle("Morphmatch");
  h1_morphmatch[0]->GetYaxis()->SetTitle("Number of events / bin");
  h1_morphmatch[0]->SetLineColor(5);// FIXME: redundant with SearchRead color code

  delete h1_morphmatch[1];
  h1_morphmatch[1] = new TH1D("h1_morphmatch_1", "Background morphmatch",n_morphmatchbins,0,1);
  h1_morphmatch[1]->GetXaxis()->SetTitle("Morphmatch");
  h1_morphmatch[1]->GetYaxis()->SetTitle("Number of events / bin");
  h1_morphmatch[1]->SetLineColor(0);// FIXME: redundant with SearchRead color code

  delete h1_morphmatch[2];
  h1_morphmatch[2] = new TH1D("h1_morphmatch_2", "Signal morphmatch",n_morphmatchbins,0,1);
  h1_morphmatch[2]->GetXaxis()->SetTitle("Morphmatch");
  h1_morphmatch[2]->GetYaxis()->SetTitle("Number of events / bin");
  h1_morphmatch[2]->SetLineColor(2);// FIXME: redundant with SearchRead color code

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::MakeMorphMatchSumw2Plots(void){
////////////////////////////////////////////////////////////////////////////////////
  delete h2_morphmatch_sumw2[0];
  h2_morphmatch_sumw2[0] = new TH2D("h2_morphmatch_sumw2_0", "Candidate morphmatch vs. #sum w^{2}",n_sumw2bins,sumw2bins,n_morphmatchbins,0,1);
  h2_morphmatch_sumw2[0]->GetXaxis()->SetTitle("#sum w^{2}");
  h2_morphmatch_sumw2[0]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_sumw2[0]->GetZaxis()->SetTitle("Number of events / bin");

  delete h2_morphmatch_sumw2[1];
  h2_morphmatch_sumw2[1] = new TH2D("h2_morphmatch_sumw2_1", "Background morphmatch vs. #sum w^{2}",n_sumw2bins,sumw2bins,n_morphmatchbins,0,1);
  h2_morphmatch_sumw2[1]->GetXaxis()->SetTitle("#sum w^{2}");
  h2_morphmatch_sumw2[1]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_sumw2[1]->GetZaxis()->SetTitle("Number of events / bin");

  delete h2_morphmatch_sumw2[2];
  h2_morphmatch_sumw2[2] = new TH2D("h2_morphmatch_sumw2_2", "Signal morphmatch vs. #sum w^{2}",n_sumw2bins,sumw2bins,n_morphmatchbins,0,1);
  h2_morphmatch_sumw2[2]->GetXaxis()->SetTitle("#sum w^{2}");
  h2_morphmatch_sumw2[2]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_sumw2[2]->GetZaxis()->SetTitle("Number of events / bin");

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::MakeMorphMatchFreqPlots(void){
////////////////////////////////////////////////////////////////////////////////////
  delete h2_morphmatch_freq0[0];
  h2_morphmatch_freq0[0] = new TH2D("h2_morphmatch_freq0_0", "Candidate morphmatch vs. frequency_0",n_fbins,fbins,n_morphmatchbins,0,1);
  h2_morphmatch_freq0[0]->GetXaxis()->SetTitle("Frequency [Hz]");
  h2_morphmatch_freq0[0]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_freq0[0]->GetZaxis()->SetTitle("Number of events / bin");

  delete h2_morphmatch_freq0[1];
  h2_morphmatch_freq0[1] = new TH2D("h2_morphmatch_freq0_1", "Background morphmatch vs. frequency_0",n_fbins,fbins,n_morphmatchbins,0,1);
  h2_morphmatch_freq0[1]->GetXaxis()->SetTitle("Frequency [Hz]");
  h2_morphmatch_freq0[1]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_freq0[1]->GetZaxis()->SetTitle("Number of events / bin");

  delete h2_morphmatch_freq0[2];
  h2_morphmatch_freq0[2] = new TH2D("h2_morphmatch_freq0_2", "Signal morphmatch vs. frequency_0",n_fbins,fbins,n_morphmatchbins,0,1);
  h2_morphmatch_freq0[2]->GetXaxis()->SetTitle("Frequency [Hz]");
  h2_morphmatch_freq0[2]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_freq0[2]->GetZaxis()->SetTitle("Number of events / bin");

  delete h2_morphmatch_freq1[0];
  h2_morphmatch_freq1[0] = new TH2D("h2_morphmatch_freq1_0", "Candidate morphmatch vs. frequency_1",n_fbins,fbins,n_morphmatchbins,0,1);
  h2_morphmatch_freq1[0]->GetXaxis()->SetTitle("Frequency [Hz]");
  h2_morphmatch_freq1[0]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_freq1[0]->GetZaxis()->SetTitle("Number of events / bin");

  delete h2_morphmatch_freq1[1];
  h2_morphmatch_freq1[1] = new TH2D("h2_morphmatch_freq1_1", "Background morphmatch vs. frequency_1",n_fbins,fbins,n_morphmatchbins,0,1);
  h2_morphmatch_freq1[1]->GetXaxis()->SetTitle("Frequency [Hz]");
  h2_morphmatch_freq1[1]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_freq1[1]->GetZaxis()->SetTitle("Number of events / bin");

  delete h2_morphmatch_freq1[2];
  h2_morphmatch_freq1[2] = new TH2D("h2_morphmatch_freq1_2", "Signal morphmatch vs. frequency_1",n_fbins,fbins,n_morphmatchbins,0,1);
  h2_morphmatch_freq1[2]->GetXaxis()->SetTitle("Frequency [Hz]");
  h2_morphmatch_freq1[2]->GetYaxis()->SetTitle("Morphmatch");
  h2_morphmatch_freq1[2]->GetZaxis()->SetTitle("Number of events / bin");

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::MakeRatePlots(void){
////////////////////////////////////////////////////////////////////////////////////

  delete h1_morphmatch_rate[0];
  h1_morphmatch_rate[0] = (TH1D*)h1_morphmatch[0]->Clone("h1_morphmatch_rate_0");
  h1_morphmatch_rate[0]->GetYaxis()->SetTitle("Event rate / bin [Hz]");
  h1_morphmatch_rate[0]->Scale(1.0/h1_monitor->GetBinContent(1));

  delete h1_morphmatch_rate[1];
  h1_morphmatch_rate[1] = (TH1D*)h1_morphmatch[1]->Clone("h1_morphmatch_rate_1");
  h1_morphmatch_rate[1]->GetYaxis()->SetTitle("Event rate / bin [Hz]");
  h1_morphmatch_rate[1]->Scale(1.0/h1_monitor->GetBinContent(2));

  delete h1_morphmatch_rate[2];
  h1_morphmatch_rate[2] = (TH1D*)h1_morphmatch[2]->Clone("h1_morphmatch_rate_2");
  h1_morphmatch_rate[2]->GetYaxis()->SetTitle("Event rate / bin [Hz]");
  h1_morphmatch_rate[2]->Scale(1.0/h1_monitor->GetBinContent(3));

  delete h2_morphmatch_sumw2_rate[0];
  h2_morphmatch_sumw2_rate[0] = (TH2D*)h2_morphmatch_sumw2[0]->Clone("h2_morphmatch_sumw2_rate_0");
  h2_morphmatch_sumw2_rate[0]->GetZaxis()->SetTitle("Event rate / bin [Hz]");
  h2_morphmatch_sumw2_rate[0]->Scale(1.0/h1_monitor->GetBinContent(1));

  delete h2_morphmatch_sumw2_rate[1];
  h2_morphmatch_sumw2_rate[1] = (TH2D*)h2_morphmatch_sumw2[1]->Clone("h2_morphmatch_sumw2_rate_1");
  h2_morphmatch_sumw2_rate[1]->GetZaxis()->SetTitle("Event rate / bin [Hz]");
  h2_morphmatch_sumw2_rate[1]->Scale(1.0/h1_monitor->GetBinContent(2));

  delete h2_morphmatch_sumw2_rate[2];
  h2_morphmatch_sumw2_rate[2] = (TH2D*)h2_morphmatch_sumw2[2]->Clone("h2_morphmatch_sumw2_rate_2");
  h2_morphmatch_sumw2_rate[2]->GetZaxis()->SetTitle("Event rate / bin [Hz]");
  h2_morphmatch_sumw2_rate[2]->Scale(1.0/h1_monitor->GetBinContent(3));

  return;
}


////////////////////////////////////////////////////////////////////////////////////
void Search::MakeInjectionsPlots(void){
////////////////////////////////////////////////////////////////////////////////////
  delete h1_injamp_val;
  h1_injamp_val = new TH1D("h1_injamp_val", "Valid injection amplitude",n_injampbins,injampbins);
  h1_injamp_val->GetXaxis()->SetTitle("injection amplitude");
  h1_injamp_val->GetYaxis()->SetTitle("Number of injections / bin");
  h1_injamp_val->SetLineColor(h1_morphmatch[0]->GetLineColor());

  delete h1_injamp_rec;
  h1_injamp_rec = new TH1D("h1_injamp_rec", "Recovered injection amplitude",n_injampbins,injampbins);
  h1_injamp_rec->GetXaxis()->SetTitle("injection amplitude");
  h1_injamp_rec->GetYaxis()->SetTitle("Number of injections / bin");
  h1_injamp_rec->SetLineColor(h1_morphmatch[0]->GetLineColor());

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::MakeEfficiencyPlots(void){
////////////////////////////////////////////////////////////////////////////////////

  delete h1_injamp_eff;
  h1_injamp_eff = (TH1D*)h1_injamp_rec->Clone("h1_injamp_eff");
  h1_injamp_eff->SetTitle("Injection recovery efficiency");
  h1_injamp_eff->GetYaxis()->SetTitle("Injection recovery efficiency");
  h1_injamp_eff->Divide(h1_injamp_val);

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::SetMorphMatchBins(const int aN){
////////////////////////////////////////////////////////////////////////////////////
  n_morphmatchbins=fabs(aN);
  MakeMorphMatchPlots();
  MakeMorphMatchSumw2Plots();
  MakeMorphMatchFreqPlots();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::SetSumw2Bins(const int aN, const double aMin, const double aMax){
////////////////////////////////////////////////////////////////////////////////////
  n_sumw2bins=fabs(aN);
  delete sumw2bins;
  sumw2bins = new double [n_sumw2bins+1];
  for(int b=0; b<=n_sumw2bins; b++)
    sumw2bins[b] = aMin*pow(10.0,b*log10(aMax/aMin)/(double)n_sumw2bins);

  MakeMorphMatchSumw2Plots();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::SetFreqBins(const int aN, const double aMin, const double aMax){
////////////////////////////////////////////////////////////////////////////////////
  n_fbins=fabs(aN);
  delete fbins;
  fbins = new double [n_fbins+1];
  for(int b=0; b<=n_fbins; b++)
    fbins[b] = aMin*pow(10.0,b*log10(aMax/aMin)/(double)n_fbins);

  MakeMorphMatchFreqPlots();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Search::SetInjAmpBins(const int aN, const double aMin, const double aMax){
////////////////////////////////////////////////////////////////////////////////////
  n_injampbins=fabs(aN);
  delete injampbins;
  injampbins = new double [n_injampbins+1];
  for(int b=0; b<=n_injampbins; b++)
    injampbins[b] = aMin*pow(10.0,b*log10(aMax/aMin)/(double)n_injampbins);

  MakeInjectionsPlots();
  return;
}


/*
////////////////////////////////////////////////////////////////////////////////////
void Search::MakeCumulative(const int aType){
////////////////////////////////////////////////////////////////////////////////////
  dist1d[1][aType]->SetBinContent(dist1d[1][aType]->GetNbinsX()+1,dist1d[0][aType]->GetBinContent(dist1d[0][aType]->GetNbinsX()+1));// overflow
  for(int b=dist1d[1][aType]->GetNbinsX(); b>=0; b--)
    dist1d[1][aType]->SetBinContent(b, dist1d[0][aType]->GetBinContent(b)+dist1d[1][aType]->GetBinContent(b+1));
  return;
}
*/
