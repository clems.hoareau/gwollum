//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "SearchTune.h"

ClassImp(SearchTune)

////////////////////////////////////////////////////////////////////////////////////
SearchTune::SearchTune(const string aBgFilePattern, const string aInjFilePattern): GwollumPlot("SearchTune") {
////////////////////////////////////////////////////////////////////////////////////

  SetGridx(1); SetGridy(1);
  BgFilePattern=aBgFilePattern;
  InjFilePattern=aInjFilePattern;
  
  Sbg = new SearchRead(BgFilePattern);
  sleep(1);
  Sinj = new SearchRead(InjFilePattern);

  bg_leg = new TH1I("bg_leg","BACKGROUND",1,0,1);
  inj_leg = new TH1I("inj_leg","SIGNAL",1,0,1);
  bg_leg->SetLineColor(Sbg->GetLineColor());
  inj_leg->SetLineColor(Sinj->GetLineColor());
  AddLegendEntry(bg_leg,"BACKGROUND","L");
  AddLegendEntry(inj_leg,"SIGNAL","L");
  DrawLegend();
}

////////////////////////////////////////////////////////////////////////////////////
SearchTune::~SearchTune(void){
////////////////////////////////////////////////////////////////////////////////////
  delete Sbg;
  delete Sinj;
  delete bg_leg;
  delete inj_leg;
}
