//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __SearchTune__
#define __SearchTune__

#include "SearchRead.h"

using namespace std;

/**
 * Tune a GW search.
 * \author    Florent Robinet
 */
class SearchTune: public GwollumPlot {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the SearchTune class.
   * 
   * @param aBgFilePattern file pattern to background events
   * @param aInjFilePattern file pattern to injection events
   * @param aVerbose verbosity level
   */
  SearchTune(const string aBgFilePattern, const string aInjFilePattern);

  /**
   * Destructor of the SearchTune class.
   */
  virtual ~SearchTune(void);
  /**
     @}
  */

  inline Long64_t DrawBg(const string varexp, const string selection="", string option=""){
    Wpad->cd();
    return Sbg->TChain::Draw(varexp.c_str(), selection.c_str(), option.c_str());
  };

  inline Long64_t DrawInj(const string varexp, const string selection="", string option=""){
    Wpad->cd();
    return Sinj->TChain::Draw(varexp.c_str(), selection.c_str(), option.c_str());
  };

  inline Long64_t ScanBg(const char* varexp = "", const char* selection = "", Option_t* option = "", Long64_t nentries = 1000000000, Long64_t firstentry = 0){
    return Sbg->Scan(varexp,selection,option,nentries,firstentry);
  }

  inline Long64_t ScanInj(const char* varexp = "", const char* selection = "", Option_t* option = "", Long64_t nentries = 1000000000, Long64_t firstentry = 0){
    return Sinj->Scan(varexp,selection,option,nentries,firstentry);
  }
  
 private:

  // OPTIONS
  string BgFilePattern;
  string InjFilePattern;

  SearchRead *Sbg;
  SearchRead *Sinj;

  TH1I *bg_leg;
  TH1I *inj_leg;

  ClassDef(SearchTune,0)
};

#endif


