//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "SearchRead.h"
#include "Search.h"

using namespace std;

int main (int argc, char* argv[]){

  // check the argument
  /*
  if(argc!=2){
    cerr<<endl;
    cerr<<argv[0]<<":"<<endl;
    cerr<<"This program runs an GW search."<<endl;
    cerr<<endl;
    cerr<<"Usage:"<<endl; 
    cerr<<endl;
    cerr<<"runsearch [path to option file]"<<endl;
    cerr<<"|__ runs the search."<<endl; 
    cerr<<endl;
    return 1;
  }
  */
  // noninj arguments
  //string optionfile = (string)argv[1];

  vector <string> infiles;
  infiles.push_back("/home/florent/Analysis/Omicron/Search/coinc/0.0/*.root");
  infiles.push_back("/home/florent/Analysis/Omicron/Search/coinc/103.630846211438/*.root");
  infiles.push_back("/home/florent/Analysis/Omicron/Search/coinc/108.565648411983/*.root");
  infiles.push_back("/home/florent/Analysis/Omicron/Search/coinc/113.500450612528/*.root");
  infiles.push_back("/home/florent/Analysis/Omicron/Search/coinc/118.435252813072/*.root");
  infiles.push_back("/home/florent/Analysis/Omicron/Search/coinc/123.370055013617/*.root");
  infiles.push_back("/home/florent/Analysis/Omicron/Search/coinc/128.304857214162/*.root");
  infiles.push_back("/home/florent/Analysis/Omicron/Search/coinc_sg235/output/*.root");


  Search *S = new Search();
  S->SetMorphMatchBins(80);
  S->SetSumw2Bins(100,0.01,100000);
  S->SetFreqBins(100,64,1024);
  S->SetInjAmpBins(80,1e-22,1e-20);
  
  // selection
  S->SetMorphMatchSelection(0.01,1.1);
  S->SetCoincFrequencySelection(64,10000);

  // detection thr = big dog
  S->SetDetectionMorphMatchThreshold(0.4176654);
  S->SetDetectionSumw2Threshold(1.6315421);

  // process
  S->Process(infiles);
  S->WriteSearch(".","mysearch.root");
  //S->ProcessSearch("./search.root");
  
  // match
  /*
  S->SetLogx(0); S->SetLogy(1);
  S->PrintPlots("h1_morphmatch_1:h1_morphmatch_0");
  S->Print("h1_morphmatch.gif");
  S->PrintPlots("h1_morphmatch_2");
  S->Print("h1_morphmatch_inj.gif");
  */
  // match vs sumw2
  S->SetLogx(1); S->SetLogy(0); S->SetLogz(1);
  S->PrintPlots("h2_morphmatch_sumw2_0");
  S->Print("h2_morphmatch_sumw2_0lag.gif");
  S->PrintPlots("h2_morphmatch_sumw2_1");
  S->Print("h2_morphmatch_sumw2_bg.gif");
  S->PrintPlots("h2_morphmatch_sumw2_2");
  S->Print("h2_morphmatch_sumw2_inj.gif");

  S->PrintPlots("h2_morphmatch_sumw2_rate_0");
  S->Print("h2_morphmatch_sumw2_rate_0lag.gif");
  S->PrintPlots("h2_morphmatch_sumw2_rate_1");
  S->Print("h2_morphmatch_sumw2_rate_bg.gif");
  S->PrintPlots("h2_morphmatch_sumw2_rate_2");
  S->Print("h2_morphmatch_sumw2_rate_inj.gif");
  
  // efficiency
  S->SetLogx(1); S->SetLogy(0);
  S->PrintPlots("h1_injamp_val");
  S->Print("h1_injamp_val.gif");
  S->PrintPlots("h1_injamp_rec");
  S->Print("h1_injamp_rec.gif");
  S->PrintPlots("h1_injamp_eff");
  S->Print("h1_injamp_eff.gif");

  delete S;
  return 0;
}

