/**
 * @file 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Coinc2.h"

ClassImp(Coinc2)

////////////////////////////////////////////////////////////////////////////////////
Coinc2::Coinc2(const unsigned int aVerbose): GwollumPlot("coinc2","STANDARD"){
////////////////////////////////////////////////////////////////////////////////////

  // inputs
  verbosity=aVerbose;

  // init
  coinc_dt = 0.0;
  CoSeg = new Segments();
  CoTag = new bool [0];
   
  for(unsigned int i=0; i<COINC2_NT; i++){
    triggers[i] = NULL;
    toffset[i] = 0.0;
    nactive[i] = 0;
    snrmin[i] = -1.0;
    freqmin[i] = -1.0;
    freqmax[i] = 1e20;
    CoC[i].clear();
  }
  hc_snrfrac[0] = new TH1D();
  hc_snrfrac[0]->SetName("snrfrac0");
  hc_snrfrac[1] = new TH1D();
  hc_snrfrac[1]->SetName("snrfrac1");
  hc_freqfrac[0] = new TH1D();
  hc_freqfrac[0]->SetName("freqfrac0");
  hc_freqfrac[1] = new TH1D();
  hc_freqfrac[1]->SetName("freqfrac1");
  stringstream ss;
  for(unsigned int i=0; i<4; i++){
    ss<<i;
    gc_snrtime[i] = new TGraph();
    gc_snrtime[i]->SetName(("snrtime"+ss.str()).c_str());
    gc_freqtime[i] = new TGraph();
    gc_freqtime[i]->SetName(("freqtime"+ss.str()).c_str());
  }
  gc_snrsnr = new TGraph();
  gc_snrsnr->SetName("snrsnr");
  gc_freqfreq = new TGraph();
  gc_freqfreq->SetName("freqfreq");

}

////////////////////////////////////////////////////////////////////////////////////
Coinc2::~Coinc2(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbosity>1) cout<<"Coinc2::~Coinc2"<<endl;
  delete CoSeg;
  delete CoTag;
  for(unsigned int i=0; i<COINC2_NT; i++) CoC[i].clear();
  delete hc_snrfrac[0];
  delete hc_snrfrac[1];
  delete hc_freqfrac[0];
  delete hc_freqfrac[1];
  for(unsigned int i=0; i<4; i++){
    delete gc_snrtime[i];
    delete gc_freqtime[i];
  }
  delete gc_snrsnr;
  delete gc_freqfreq;
}

////////////////////////////////////////////////////////////////////////////////////
bool Coinc2::SetTriggers(ReadTriggers *aTrigger0, ReadTriggers *aTrigger1,
                         const double aTimeOffset0, const double aTimeOffset1,
                         Segments *aValidSegments){
////////////////////////////////////////////////////////////////////////////////////
  if(aTrigger0==NULL || !aTrigger0->GetStatus()){
    cerr<<"Coinc2::SetTriggers: the trigger set 0 is corrupted"<<endl;
    return false;
  }
  if(aTrigger1==NULL || !aTrigger1->GetStatus()){
    cerr<<"Coinc2::SetTriggers: the trigger set 1 is corrupted"<<endl;
    return false;
  }
  if(aValidSegments!=NULL&&!aValidSegments->GetStatus()){
    cerr<<"Coinc2::SetTriggers: the input Segments object is corrupted"<<endl;
    return false;
  }
  if(verbosity) cout<<"Coinc2::SetTriggers: sets new input trigger sets"<<endl;

  // link trigger objects
  triggers[0]=aTrigger0;
  triggers[1]=aTrigger1;
  toffset[0]=aTimeOffset0;
  toffset[1]=aTimeOffset1;

  // reset coinc segments
  delete CoSeg;

  // ************ MAKE COINC SEGMENTS ************
  // STEP1: take the trigger0 segments
  CoSeg = new Segments(triggers[0]->GetStarts(), triggers[0]->GetEnds());

  // STEP2: restrict to valid segments (if any)
  if(aValidSegments!=NULL){
    if(!CoSeg->Intersect(aValidSegments)){
      cerr<<"Coinc2::SetTriggers: the input Segments cannot be intersected"<<endl;
      return false;
    }
  }

  // STEP3: apply time offset
  if(!CoSeg->ApplyPadding(toffset[0], toffset[0])){
    cerr<<"Coinc2::SetTriggers: the coinc Segments cannot be time-shifted"<<endl;
    return false;
  }

  // STEP4: take the trigger1 segments
  Segments *tmp = new Segments(triggers[1]->GetStarts(), triggers[1]->GetEnds());

  // STEP5: restrict to valid segments (if any)
  if(aValidSegments!=NULL){
    if(!tmp->Intersect(aValidSegments)){
      cerr<<"Coinc2::SetTriggers: the input Segments cannot be intersected"<<endl;
      delete tmp;
      return false;
    }
  }

  // STEP6: apply time offset
  if(!tmp->ApplyPadding(toffset[1], toffset[1])){
    cerr<<"Coinc2::SetTriggers: the coinc Segments cannot be time-shifted"<<endl;
    delete tmp;
    return false;
  }
  
  // STEP6: intersect both segments
  if(!CoSeg->Intersect(tmp)){
    cerr<<"Coinc2::SetTriggers: the coinc Segments cannot be intersected"<<endl;
    delete tmp;
    return false;
  }
  delete tmp;
  if(verbosity>1) cout<<"Coinc2::SetTriggerSamples: coincident live-time = "<<CoSeg->GetLiveTime()<<" s"<<endl;
  //***********************************************

  // reset coincs
  delete CoTag;
  CoTag = new bool [0];
  CoC[0].clear();
  CoC[1].clear();
  
  // select active clusters
  SelectClusters(0);
  SelectClusters(1);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
unsigned int Coinc2::MakeCoinc(void){
////////////////////////////////////////////////////////////////////////////////////

  // no active clusters --> nothing to do
  if(nactive[0]==0) return 0;
  if(nactive[1]==0) return 0;

  // reset coinc
  for(unsigned int i=0; i<COINC2_NT; i++) CoC[i].clear();
  
  // locals
  double ts0, te0, ts1, te1;
  unsigned int c1start=0;
  if(verbosity) cout<<"Coinc2::MakeCoinc: run the coincidence algo..."<<endl;

  // loop over trigger sample #0
  for(unsigned int c0=0; c0<triggers[0]->GetClusterN(); c0++){

    // skip unactive clusters
    if(triggers[0]->GetClusterTag(c0)<0) continue;

    // effective cluster duration
    ts0=GetCoincTimeWindowStart(0, c0);
    te0=GetCoincTimeWindowEnd(0, c0);

    // loop over trigger sample #1
    for(unsigned int c1=c1start; c1<triggers[1]->GetClusterN(); c1++){

      if(c1>0) c1start=c1-1;// assumes clusters with no time overlap

      // skip unactive clusters
      if(triggers[1]->GetClusterTag(c1)<0) continue;
       
      // effective cluster duration
      ts1=GetCoincTimeWindowStart(1, c1);
      te1=GetCoincTimeWindowEnd(1, c1);

      // time coincidence
      if(te1<=ts0) continue;// before
      if(ts1>te0) break;  // after

      // save coinc
      CoC[0].push_back(c0);
      CoC[1].push_back(c1);
    }
  }
  
  // init coinc tag
  delete CoTag;
  CoTag = new bool [CoC[0].size()];
  for(unsigned int c=0; c<CoC[0].size(); c++) CoTag[c]=true;
      
  if(verbosity>1) cout<<"Coinc2::MakeCoinc: "<<CoC[0].size()<<" coinc events found"<<endl;
  return CoC[0].size();
}

////////////////////////////////////////////////////////////////////////////////////
void Coinc2::PrintCoinc(void){
////////////////////////////////////////////////////////////////////////////////////
  for(unsigned int c=0; c<CoC[0].size(); c++){
    cout<<"*********************************************************************"<<endl;
    cout<<"coinc_"<<c<<endl;
    cout<<"  "<<triggers[0]->GetName()<<": t="<<triggers[0]->GetClusterTime(CoC[0][c])<<" s, f="<<triggers[0]->GetClusterFrequency(CoC[0][c])<<" Hz, snr="<<triggers[0]->GetClusterSnr(CoC[0][c])<<endl;;
    cout<<"  "<<triggers[1]->GetName()<<": t="<<triggers[1]->GetClusterTime(CoC[1][c])<<" s, f="<<triggers[1]->GetClusterFrequency(CoC[1][c])<<", snr="<<triggers[1]->GetClusterSnr(CoC[1][c])<<endl;;
  }
  cout<<"*********************************************************************"<<endl;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Coinc2::PrintCoincNot(const unsigned int aSampleIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(triggers[0]==NULL || triggers[1]==NULL) return;

  // loop over trigger sample
  unsigned int costart=0;
  bool found=false;
  unsigned int si = aSampleIndex%COINC2_NT;
  for(unsigned int c=0; c<triggers[si]->GetClusterN(); c++){
    if(triggers[si]->GetClusterTag(c)<0) continue;
    found=false;
    
    // search for a coinc
    for(unsigned int co=costart; co<CoC[si].size(); co++){
      if(CoC[si][co]>c) break;
      if(c==CoC[si][co]){
        found=true;
        break;
      }
      costart=co;
    }
    
    // do not print
    if(found) continue;
    
    // print cluster
    cout<<triggers[si]->GetName()<<": t="<<triggers[si]->GetClusterTime(c)<<" s, f="<<triggers[si]->GetClusterFrequency(c)<<" Hz, snr="<<triggers[si]->GetClusterSnr(c)<<endl;
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool Coinc2::MakeComparators(void){
////////////////////////////////////////////////////////////////////////////////////
  if(triggers[0]==NULL || triggers[1]==NULL){
    cerr<<"Coinc2::MakeComparators: both trigger samples must be defined"<<endl;
    return false;
  }

  if(verbosity) cout<<"Coinc2::MakeComparators: fill comparators..."<<endl;

  // init comparators
  stringstream ss;
  for(unsigned int i=0; i<4; i++){
    ss<<i;
    delete gc_snrtime[i];
    gc_snrtime[i] = new TGraph();
    gc_snrtime[i]->SetName(("snrtime"+ss.str()).c_str());
    gc_freqtime[i] = new TGraph();
    gc_freqtime[i]->SetName(("freqtime"+ss.str()).c_str());
    ss.clear(); ss.str("");
  }
  delete gc_snrsnr;
  gc_snrsnr = new TGraph();
  gc_snrsnr->SetName("coinc_snrsnr");
  delete gc_freqfreq;
  gc_freqfreq = new TGraph();
  gc_freqfreq->SetName("coinc_freqfreq");

  // histograms
  TriggerSelect *TS;
  TS = new TriggerSelect(triggers[0], 0);
  TS->SetSnrResolution(20);
  TS->SetFrequencyResolution(20);
  delete hc_snrfrac[0];
  hc_snrfrac[0] = TS->GetTH1D("SNR");
  hc_snrfrac[0]->SetName("hc_snrfrac0");
  TH1D *hsnr0 = TS->GetTH1D("SNR");
  hsnr0->SetName("hsnr0");
  delete hc_freqfrac[0];
  hc_freqfrac[0] = TS->GetTH1D("Frequency");
  hc_freqfrac[0]->SetName("hc_freqfrac0");
  TH1D *hfreq0 = TS->GetTH1D("Frequency");
  hfreq0->SetName("hfreq0");
  delete TS;
  TS = new TriggerSelect(triggers[1], 0);
  TS->SetSnrResolution(20);
  TS->SetFrequencyResolution(20);
  delete hc_snrfrac[1];
  hc_snrfrac[1] = TS->GetTH1D("SNR");
  hc_snrfrac[1]->SetName("hc_snrfrac1");
  TH1D *hsnr1 = TS->GetTH1D("SNR");
  hsnr1->SetName("hsnr1");
  delete hc_freqfrac[1];
  hc_freqfrac[1] = TS->GetTH1D("Frequency");
  hc_freqfrac[1]->SetName("hc_freqfrac1");
  TH1D *hfreq1 = TS->GetTH1D("Frequency");
  hfreq1->SetName("hfreq1");
  delete TS;
  
  // fill trigger 0 plots
  double tstart=-1.0;
  for(unsigned int c0=0; c0<triggers[0]->GetClusterN(); c0++){
    if(triggers[0]->GetClusterTag(c0)<0) continue;
    if(tstart<0.0) tstart=triggers[0]->GetClusterTime(c0);
    gc_snrtime[0]->SetPoint(gc_snrtime[0]->GetN(),
                            triggers[0]->GetClusterTime(c0)-tstart, triggers[0]->GetClusterSnr(c0));
    gc_freqtime[0]->SetPoint(gc_freqtime[0]->GetN(),
                             triggers[0]->GetClusterTime(c0)-tstart, triggers[0]->GetClusterFrequency(c0));
    hsnr0->Fill(triggers[0]->GetClusterSnr(c0));
    hfreq0->Fill(triggers[0]->GetClusterFrequency(c0));
  }

  // fill trigger 1 plots
  for(unsigned int c1=0; c1<triggers[1]->GetClusterN(); c1++){
    if(triggers[1]->GetClusterTag(c1)<0) continue;
    if(tstart<0.0) tstart=triggers[1]->GetClusterTime(c1);
    gc_snrtime[1]->SetPoint(gc_snrtime[1]->GetN(),
                            triggers[1]->GetClusterTime(c1)-tstart, triggers[1]->GetClusterSnr(c1));
    gc_freqtime[1]->SetPoint(gc_freqtime[1]->GetN(),
                             triggers[1]->GetClusterTime(c1)-tstart, triggers[1]->GetClusterFrequency(c1));
    hsnr1->Fill(triggers[1]->GetClusterSnr(c1));
    hfreq1->Fill(triggers[1]->GetClusterFrequency(c1));
  }

  // fill coinc comparators
  for(unsigned int co=0; co<CoC[0].size(); co++){
    if(!CoTag[co]) continue;// non-valid coinc

    gc_snrsnr->SetPoint(gc_snrsnr->GetN(),
                        triggers[0]->GetClusterSnr(CoC[0][co]), triggers[1]->GetClusterSnr(CoC[1][co]));
    gc_freqfreq->SetPoint(gc_freqfreq->GetN(),
                          triggers[0]->GetClusterFrequency(CoC[0][co]), triggers[1]->GetClusterFrequency(CoC[1][co]));
    gc_snrtime[2]->SetPoint(gc_snrtime[2]->GetN(),
                            triggers[0]->GetClusterTime(CoC[0][co])-tstart, triggers[0]->GetClusterSnr(CoC[0][co]));
    gc_freqtime[2]->SetPoint(gc_freqtime[2]->GetN(),
                             triggers[0]->GetClusterTime(CoC[0][co])-tstart, triggers[0]->GetClusterFrequency(CoC[0][co]));
    gc_snrtime[3]->SetPoint(gc_snrtime[3]->GetN(),
                            triggers[1]->GetClusterTime(CoC[1][co])-tstart, triggers[1]->GetClusterSnr(CoC[1][co]));
    gc_freqtime[3]->SetPoint(gc_freqtime[3]->GetN(),
                             triggers[1]->GetClusterTime(CoC[1][co])-tstart, triggers[1]->GetClusterFrequency(CoC[1][co]));
    hc_snrfrac[0]->Fill(triggers[0]->GetClusterSnr(CoC[0][co]));
    hc_snrfrac[1]->Fill(triggers[1]->GetClusterSnr(CoC[1][co]));
    hc_freqfrac[0]->Fill(triggers[0]->GetClusterFrequency(CoC[0][co]));
    hc_freqfrac[1]->Fill(triggers[1]->GetClusterFrequency(CoC[1][co]));
  }

  hc_snrfrac[0]->Divide(hsnr0);
  hc_snrfrac[1]->Divide(hsnr1);
  hc_freqfrac[0]->Divide(hfreq0);
  hc_freqfrac[1]->Divide(hfreq1);
  delete hsnr0;
  delete hsnr1;
  delete hfreq0;
  delete hfreq1;

  // cosmetics
  ss<<setprecision(13)<<tstart;
  hc_snrfrac[0]->SetTitle((triggers[0]->GetName()+" Fraction of coincident events").c_str());
  hc_snrfrac[0]->GetXaxis()->SetTitle("SNR");
  hc_snrfrac[0]->GetYaxis()->SetTitle("Fraction of coincident events");
  hc_snrfrac[1]->SetTitle((triggers[1]->GetName()+" Fraction of coincident events").c_str());
  hc_snrfrac[1]->GetXaxis()->SetTitle("SNR");
  hc_snrfrac[1]->GetZaxis()->SetTitle("Fraction of coincident events");
  hc_freqfrac[0]->SetTitle((triggers[0]->GetName()+" Fraction of coincident events").c_str());
  hc_freqfrac[0]->GetXaxis()->SetTitle("Frequency [Hz]");
  hc_freqfrac[0]->GetYaxis()->SetTitle("Fraction of coincident events");
  hc_freqfrac[1]->SetTitle((triggers[1]->GetName()+" Fraction of coincident events").c_str());
  hc_freqfrac[1]->GetXaxis()->SetTitle("Frequency [Hz]");
  hc_freqfrac[1]->GetZaxis()->SetTitle("Fraction of coincident events");
  gc_snrtime[0]->SetTitle(triggers[0]->GetName().c_str());
  gc_snrtime[0]->GetHistogram()->GetXaxis()->SetTitle(("Time after "+ss.str()+" [s]").c_str());
  gc_snrtime[0]->GetHistogram()->GetYaxis()->SetTitle("SNR");
  gc_snrtime[0]->SetMarkerStyle(4);
  gc_snrtime[1]->SetTitle(triggers[1]->GetName().c_str());
  gc_snrtime[1]->GetHistogram()->GetXaxis()->SetTitle(("Time after "+ss.str()+" [s]").c_str());
  gc_snrtime[1]->GetHistogram()->GetYaxis()->SetTitle("SNR");
  gc_snrtime[1]->SetMarkerStyle(4);
  gc_snrtime[2]->SetMarkerStyle(2);
  gc_snrtime[3]->SetMarkerStyle(2);
  gc_snrtime[2]->SetMarkerColor(2);
  gc_snrtime[3]->SetMarkerColor(2);
  gc_freqtime[0]->SetTitle(triggers[0]->GetName().c_str());
  gc_freqtime[0]->GetHistogram()->GetXaxis()->SetTitle(("Time after "+ss.str()+" [s]").c_str());
  gc_freqtime[0]->GetHistogram()->GetYaxis()->SetTitle("Frequency [Hz]");
  gc_freqtime[0]->SetMarkerStyle(4);
  gc_freqtime[1]->SetTitle(triggers[1]->GetName().c_str());
  gc_freqtime[1]->GetHistogram()->GetXaxis()->SetTitle(("Time after "+ss.str()+" [s]").c_str());
  gc_freqtime[1]->GetHistogram()->GetYaxis()->SetTitle("Frequency [Hz]");
  gc_freqtime[1]->SetMarkerStyle(4);
  gc_freqtime[2]->SetMarkerStyle(2);
  gc_freqtime[3]->SetMarkerStyle(2);
  gc_freqtime[2]->SetMarkerColor(2);
  gc_freqtime[3]->SetMarkerColor(2);
  gc_snrsnr->SetTitle(("Coinc events: "+triggers[0]->GetName()+" / "+triggers[1]->GetName()).c_str());
  gc_snrsnr->GetHistogram()->GetXaxis()->SetTitle((triggers[0]->GetName()+" SNR").c_str());
  gc_snrsnr->GetHistogram()->GetYaxis()->SetTitle((triggers[1]->GetName()+" SNR").c_str());
  gc_snrsnr->SetMarkerStyle(20);
  gc_snrsnr->SetMarkerColor(2);
  gc_freqfreq->SetTitle(("Coinc events: "+triggers[0]->GetName()+" / "+triggers[1]->GetName()).c_str());
  gc_freqfreq->GetHistogram()->GetXaxis()->SetTitle((triggers[0]->GetName()+" frequency [Hz]").c_str());
  gc_freqfreq->GetHistogram()->GetYaxis()->SetTitle((triggers[1]->GetName()+" frequency [Hz]").c_str());
  gc_freqfreq->SetMarkerStyle(20);
  gc_freqfreq->SetMarkerColor(2);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Coinc2::PrintComparators(const string aFileName){
////////////////////////////////////////////////////////////////////////////////////
  
  if(verbosity) cout<<"Coinc2::PrintComparators: print comparators..."<<endl;
  
  // print comparators
  Draw(hc_snrfrac[0]);
  SetLogx(1);
  SetLogy(0);
  SetGridx(1);
  SetGridy(1);
  hc_snrfrac[0]->GetXaxis()->SetMoreLogLabels();
  Print(aFileName+"_snrfrac0.png");

  Draw(hc_snrfrac[1]);
  SetLogx(1);
  SetLogy(0);
  SetGridx(1);
  SetGridy(1);
  hc_snrfrac[1]->GetXaxis()->SetMoreLogLabels();
  Print(aFileName+"_snrfrac1.png");
  
  Draw(hc_freqfrac[0]);
  SetLogx(1);
  SetLogy(0);
  SetGridx(1);
  SetGridy(1);
  hc_freqfrac[0]->GetXaxis()->SetMoreLogLabels();
  Print(aFileName+"_freqfrac0.png");

  Draw(hc_freqfrac[1]);
  SetLogx(1);
  SetLogy(0);
  SetGridx(1);
  SetGridy(1);
  hc_freqfrac[1]->GetXaxis()->SetMoreLogLabels();
  Print(aFileName+"_freqfrac1.png");
  
  if(gc_snrsnr->GetN()>0){
    Draw(gc_snrsnr,"AP");
    SetLogx(1);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_snrsnr->GetHistogram()->GetXaxis()->SetMoreLogLabels();
    gc_snrsnr->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    Print(aFileName+"_snrsnr.png");
  }
  
  if(gc_freqfreq->GetN()>0){
    Draw(gc_freqfreq,"AP");
    SetLogx(1);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_freqfreq->GetHistogram()->GetXaxis()->SetMoreLogLabels();
    gc_freqfreq->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    Print(aFileName+"_freqfreq.png");
  }
  
  if(gc_snrtime[0]->GetN()>0){
    Draw(gc_snrtime[0],"AP");
    if(gc_snrtime[2]->GetN()>0) Draw(gc_snrtime[2],"Psame");
    SetLogx(0);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_snrtime[0]->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    gc_snrtime[0]->GetHistogram()->GetXaxis()->SetNoExponent();
    AddLegendEntry(gc_snrtime[0],"Triggers","P");
    AddLegendEntry(gc_snrtime[2],"Coinc","P");
    DrawLegend();
    Print(aFileName+"_snrtime0.png");
    ResetLegend();
  }

  if(gc_snrtime[1]->GetN()>0){
    Draw(gc_snrtime[1],"AP");
    if(gc_snrtime[3]->GetN()>0) Draw(gc_snrtime[3],"Psame");
    SetLogx(0);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_snrtime[1]->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    gc_snrtime[1]->GetHistogram()->GetXaxis()->SetNoExponent();
    AddLegendEntry(gc_snrtime[1],"Triggers","P");
    AddLegendEntry(gc_snrtime[3],"Coinc","P");
    DrawLegend();
    Print(aFileName+"_snrtime1.png");
    ResetLegend();
  }

  if(gc_freqtime[0]->GetN()>0){
    Draw(gc_freqtime[0],"AP");
    if(gc_freqtime[2]->GetN()>0) Draw(gc_freqtime[2],"Psame");
    SetLogx(0);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_freqtime[0]->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    gc_freqtime[0]->GetHistogram()->GetXaxis()->SetNoExponent();
    AddLegendEntry(gc_freqtime[0],"Triggers","P");
    AddLegendEntry(gc_freqtime[2],"Coinc","P");
    DrawLegend();
    Print(aFileName+"_freqtime0.png");
    ResetLegend();
  }

  if(gc_freqtime[1]->GetN()>0){
    Draw(gc_freqtime[1],"AP");
    if(gc_freqtime[3]->GetN()>0) Draw(gc_freqtime[3],"Psame");
    SetLogx(0);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_freqtime[1]->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    gc_freqtime[1]->GetHistogram()->GetXaxis()->SetNoExponent();
    AddLegendEntry(gc_freqtime[1],"Triggers","P");
    AddLegendEntry(gc_freqtime[3],"Coinc","P");
    DrawLegend();
    Print(aFileName+"_freqtime1.png");
    ResetLegend();
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Coinc2::SelectClusters(const unsigned int aSampleIndex){
////////////////////////////////////////////////////////////////////////////////////

  nactive[aSampleIndex]=0;

  // loop over clusters
  for(unsigned int c=0; c<triggers[aSampleIndex]->GetClusterN(); c++){

    // tag --> ignore cluster
    if(triggers[aSampleIndex]->GetClusterTag(c)<0) continue;
    
    // check coinc segments
    if(!CoSeg->IsInsideSegment(triggers[aSampleIndex]->GetClusterTime(c)+toffset[aSampleIndex])){
      triggers[aSampleIndex]->SetClusterTag(c,-1);// discard
      continue;
    }

    // check SNR min
    if(triggers[aSampleIndex]->GetClusterSnr(c)<snrmin[aSampleIndex]){
      triggers[aSampleIndex]->SetClusterTag(c,-1);// discard
      continue;
    }
    
    // check frequency min
    if(triggers[aSampleIndex]->GetClusterFrequency(c)<freqmin[aSampleIndex]){
      triggers[aSampleIndex]->SetClusterTag(c,-1);// discard
      continue;
    }
    
    // check frequency max
    if(triggers[aSampleIndex]->GetClusterFrequency(c)>=freqmax[aSampleIndex]){
      triggers[aSampleIndex]->SetClusterTag(c,-1);// discard
      continue;
    }

    // one more active cluster
    nactive[aSampleIndex]++;
  }
    
  if(verbosity)
    cout<<"Coinc2::SelectClusters: trigger set #"<<aSampleIndex<<" = "<<triggers[aSampleIndex]->GetName()<<", time offset = "<<toffset[aSampleIndex]<<", "<<nactive[aSampleIndex]<<" active clusters"<<endl;

  return;
}

