//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __GlitchVeto__
#define __GlitchVeto__

#include "Coinc.h"
#include "TH2.h"
#include "TGraph.h"

using namespace std;

/**
 * Study glitch-to glitch coupling between 2 trigger data sets.
 * This class offers many functions to build a veto based on triggers (or glitches) found in a given data stream. In addition, some routines are implemented to study glitch-to-glitch coupling mechanisms and to produce effective vetoes.
 * 
 * Things you must know before using this class:
 * - Two main inputs are necessary for this class to work: A ReadTriggers object called "MAIN" defining the glitch sample you want to study or veto and a ReadTriggers object called "AUX" defining the auxiliary glitch sample you want to use to study the correlation with the main triggers or to create your veto.
 * - Both trigger samples must be clustered before using this class. See ReadTriggers::Clusterize().
 * - The main trigger object is used as a reference. This means the working time segments are given by the main triggers not by the aux triggers (a specific segment list can also be specified by the user)
 * - The cluster status of the Main ReadTriggers object is taken into account. Main clusters with a 'false' status are excluded. Aux. clusters in coincidence with a main cluster with a 'false' status are excluded. See ReadTriggers::SetClusterStatus() for more details.
 * - The ProcessGlitch2Glitch() function should always be called. It studies the glitch-to-glitch coincidences. In a second step it is possible to generate a veto with the MakeVeto() function.
 *
 * Here is the typical step-by-step use of this class:
 * - GlitchVeto(): constructor
 * - SetMainTriggers(): define main trigger sample
 * - SetAuxTriggers(): define aux trigger sample
 * - SetSegments(): optionnally define new reference segments
 * - ProcessGlitch2Glitch(): study the coupling between aux-main
 * - SetThreshold(): Define veto conditions
 * - MakeVeto(): Produce veto thresholds, veto segments and veto monitors
 * \author    Florent Robinet
 */
class GlitchVeto{

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the GlitchVeto class.
   * All the parameters are initialized:
   * - automatic binning
   * - no threshold to build vetoes
   * - no frequency selection to define the coincidence
   * @param averbose verbosity level
   */
  GlitchVeto(const int averbose=0);

  /**
   * Destructor of the GlitchVeto class.
   */
  virtual ~GlitchVeto(void);
  /**
     @}
  */

  /**
   * Sets the main trigger sample.
   * Input triggers are represented by a ReadTriggers object. It is mandatory that triggers are clustered before calling this function. The coincidence can only be performed over clusters. For more details, see ReadTriggers::Clusterize().
    * @param Amaintriggers pointer to the main trigger sample
   */
  bool SetMainTriggers(ReadTriggers *Amaintriggers);

  /**
   * Sets the aux trigger sample.
   * Input triggers are represented by a ReadTriggers object. It is mandatory that triggers are clustered before calling this function. The coincidence can only be performed over clusters. For more details, see ReadTriggers::Clusterize().
    * @param Aauxtriggers pointer to the aux trigger sample
   */
  bool SetAuxTriggers(ReadTriggers *Aauxtriggers);

  /**
   * Sets new reference segments
   * By default, the Main triggers segments are considered as the reference. It is possible to change the reference segments with this function.
   * The new segments will be intersected with the Main triggers segments before being used as a reference for the coincidence. Therefore, this function should only be called after the Main triggers being set.
   * @param aNewSeg pointer to the new Segments object
   */
  bool SetSegments(Segments *aNewSeg);

  /**
   * Study the glitch-to-glitch coupling between Main and Aux data streams. 
   * 3 trigger parameters are considered to characterize this coupling: time / frequency / SNR.
   * This function should always be called after both the main and aux trigger sample being defined.
   * Four internal steps are performed:
   * - the parameter space is reset
   * - parameters are binned
   * - the coincidence between the 2 streams is performed
   * - the tuning monitors are filled
   */
  bool ProcessGlitch2Glitch(void);

  /**
   * Sets a condition to define the veto.
   * 4 types of conditions can be defined with a keyword and a threshold value: 'aVariableName' > 'aThreshold', where aVariableName=
   * - "UP" stands for use-percentage
   * - "D"  stands for dead-time
   * - "ED" stands for efficiency over dead-time
   * - "NV" stands for the number of main cluster involved in a coincidence (Nveto)
   * - "NU" stands for the number of used aux cluster involved in a coincidence (Nuse)
   *
   * This function should be called several times to define a multi-condition veto.
   * @param aVariableName keyword to define on which variable the condition should be applied
   * @param aThreshold threshold value to define the veto condition
   */
  bool SetThreshold(const string aVariableName, const double aThreshold);

  /**
   * Produces veto thresholds, veto segments and veto monitors.
   * After the coupling is characterized (see ProcessGlitch2Glitch()), this function produces the veto based on the conditions defined with the SetThreshold() function. 3 steps are performed:
   * - A SNR threshold is optimized. This threshold is tuned to verify ALL the conditions defined with the SetThreshold() function.
   * - The veto segments are produced. Each veto segment is defined by the aux. clusters (start-end) above the optimized SNR threshold. Only the segments intersecting the reference segments are produced.
   * - the performance of the veto are measured (efficiency, use-percentage, dead-time...) and additional monitors are created.
   */
  bool MakeVeto(void); 
  
  /**
   * Writes a ROOT file report with all the monitors.
   * Here is the definition of all the monitors:
   * - (TH2D)"h_Astat": Aux cluster statistic in (aux) frequency and SNR bins.
   * - (TH2D)"h_Astat_cum": Aux cluster statistic in (aux) frequency and cumulative SNR bins.
   * - (TH2D)"h_Mstat": Main cluster statistic in (main) frequency and SNR bins.
   * - (TH2D)"h_Mstat_cum": Main cluster statistic in (main) frequency and cumulative SNR bins.
   * - (TH2D)"h_dead": Aux cluster dead time in (aux) frequency and SNR bins.
   * - (TH2D)"h_dead_cum": Aux cluster dead time in (aux) frequency and cumulative SNR bins.
   * - (TH2D)"h_d": Same as h_dead_cum but normalized by the reference livetime.
   * - (TH1D)"h_mr": Main cluster rate in time bins.
   * - (TH1D)"h_ar": Aux cluster rate in time bins.
   * - (TGraph)"g_Msnrtime": Main cluster SNR vs time scatter plot.
   * - (TGraph)"g_snrsnr": Coincident events represented as (Main) SNR vs (Aux) SNR scatter plot.
   * - (TH2D)"h_ff": Coincident events represented as (Main) SNR vs (Aux) SNR histogram.
   * - (TH2D)"h_use": Number of Aux clusters involved in a coincidence in (aux) frequency and cumulative SNR bins.
   * - (TH2D)"h_use_cum": cumulative number of Aux clusters involved in a coincidence in (aux) frequency and cumulative SNR bins.
   * - (TH2D)"h_nveto": Number of Main clusters involved in a coincidence in (aux) frequency and cumulative SNR bins.
   * - (TH2D)"h_nveto_cum": cumulative number of Main clusters involved in a coincidence in (aux) frequency and cumulative SNR bins.
   * - (TH2D)"h_up": Veto use-percentage in (aux) frequency and cumulative SNR bins.
   * - (TH2D)"h_e": Veto efficiency in (aux) frequency and cumulative SNR bins.
   * - (TH2D)"h_ed": Veto efficiency/dead-time in (aux) frequency and cumulative SNR bins.
   * - (TH1D)"h_Me": Veto efficiency in (main) frequency bins.
   * - (TH1D)"h_Ause": number of Aux clusters used in the veto in (aux) frequency bins.
   * - (TH1D)"h_vr": Veto rate in time bins.
   * - (TGraph)"g_snrsnr_veto": Coincident events used for the veto represented as (Main) SNR vs (Aux) SNR scatter plot.
   * - (TH2D)"h_ff_veto": Coincident events used for the veto represented as (Main) SNR vs (Aux) SNR histogram.
   * - (TH1D)"h_thr": Veto optimized threshold in (aux) frequency and bins.
   * - (Segments)"VetoSeg": Veto Segments object.
   * - (TTree)"Tfom": Veto performance numbers.
   * @param aRootFileName output ROOT file name
   */
  bool WriteReport(const string aRootFileName="./myreport.root");

  /**
   * Returns a pointer to a given monitor.
   * Internal monitors of the GlitchVeto class can be accessed. Do not delete or modify the return object since it is internally used by the class. Here are the name of the monitor which can be accessed:
   * - "h_Astat": Aux cluster statistic in (aux) frequency and SNR bins.
   * - "h_Astat_cum": Aux cluster statistic in (aux) frequency and cumulative SNR bins.
   * - "h_Mstat": Main cluster statistic in (main) frequency and SNR bins.
   * - "h_Mstat_cum": Main cluster statistic in (main) frequency and cumulative SNR bins.
   * - "h_dead": Aux cluster dead time in (aux) frequency and SNR bins.
   * - "h_dead_cum": Aux cluster dead time in (aux) frequency and cumulative SNR bins.
   *
   * NULL is returned if the monitor does not exist.
   * @param MonitorName name of the monitor to be returned
   */
  TH1* GetMonitor(const string MonitorName);

  /**
   * Returns the dead-time induced by all the aux cluster.
   */
  double GetFullDeadTime(void);

  /**
   * Returns the veto efficiency.
   * if normalized=0, the raw number of vetoed triggers is returned.
   * if normalized=1, the number of vetoed triggers is normalized by the total number of main triggers.
   */
  double GetVetoEfficiency(const bool normalized=true);

  /**
   * Returns the veto dead-time.
   */
  double GetVetoDeadTime(void);

  /**
   * Returns the veto use-percentage.
   */
  double GetVetoUsePercentage(void);

  /**
   * Returns a pointer to the current veto Segments object.
   * Do not delete or modify.
   */
  inline Segments* GetVetoSegments(void){ return VetoSeg; };


  /**
   * Fix the number of aux frequency bins.
   * By default (nfixbins=0), the frequency binning is automatically computed based on the available statistic. This function fixes the number of bins to nfixbins;
   * @param nfixbins number of frequency bins
   */
  inline void SetFixBins(const int nfixbins=0){ nfix=nfixbins; };

 private:
  
  int fverbose;                    ///< verbosity level
  bool status_OK;                  ///< init status

  // BINNING
  bool MakeBinning(const int nfixbins=0);///< make time-freq-snr binning
  double *tbins;                   ///< time bins
  double *fauxbins;                ///< frequency bins - aux
  double *fmainbins;               ///< frequency bins - main
  double *sauxbins;                ///< SNR bins - aux
  double *smainbins;               ///< SNR bins - main
  int nfix;                        ///< fixed number of bins
  int ntbins;                      ///< number of time bins
  int nfauxbins;                   ///< number of frequency bins - aux
  int nfmainbins;                  ///< number of frequency bins - main
  int nsauxbins;                   ///< number of SNR bins - aux
  int nsmainbins;                  ///< number of SNR bins - main

  // TRIGGERS
  ReadTriggers *MainTriggers;      ///< main triggers - DO NOT DELETE
  ReadTriggers *AuxTriggers;       ///< auxiliary triggers - DO NOT DELETE
  Coinc *coinc;                    ///< main-aux coinc, aux first
  vector <int> MainIndexes;        ///< coinc indexes for main
  vector <int> AuxIndexes;         ///< coinc indexes for aux.
  Segments *Seg;                   ///< final segments

  // PARAMETER SPACE
  bool ResetParameterSpace(void);  ///< resets parameter space.
  bool CreateParameterSpace(void); ///< Create monitors
  bool FillParameterSpace(void);   ///< Fill Monitors

  // MAIN CHANNEL
  int nmaintrig;                   ///< number of main triggers
  TH2D *h_Mstat, *h_Mstat_cum;     ///< raw statistics histos
  TH1D *h_mr;                      ///< rate over time
  TGraph *g_Msnrtime;              ///< snr vs time plot of main triggers

  // AUX CHANNEL
  TH2D *h_Astat, *h_Astat_cum,     ///< raw statistics histos
    *h_dead, *h_dead_cum, *h_d;    ///< dead-time histos
  TH1D *h_ar;                      ///< rate over time
  TGraph *g_snrsnr;                ///< snr/snr plot
  TH2D *h_ff;                      ///< f/f plot

  // VETO TUNING
  TH2D *h_nveto, *h_nveto_cum,     ///< nveto histos
    *h_use, *h_use_cum,            ///< use histos
    *h_up, *h_e, *h_ed;            ///< FOM histos
  
  // THRESHOLDS
  TH1D *h_thr;                     ///< SNR thresholds
  TH1I *h_thr_bin;                 ///< SNR thresholds bin number

  // VETO PERF
  TH1D *h_Me;                      ///< veto efficiency
  TH1D *h_Ause;                    ///< veto n-use
  TH1D *h_vr;                      ///< veto rate over time
  TGraph *g_snrsnr_veto;           ///< snr/snr plot
  TGraph *g_ff_veto;               ///< f/f plot
  TH2D *h_ff_veto;                 ///< f/f plot
  TGraph *g_Msnrtime_veto;         ///< snr vs time plot of main triggers

  // THRESHOLDS
  double GetUPthr(const double scalefactor=2);
  double UPthr;                    ///< use-percentage threshold
  double EDthr;                    ///< efficiency/dead-time threshold
  double Dthr;                     ///< dead-time threshold
  double NVthr;                    ///< number of vetoed clusters threshold
  double NUthr;                    ///< number of used clusters threshold

  // VETO
  Segments *VetoSeg;               ///< veto segments
  TTree *Tfom;                     ///< fom tree
  double fom_dt, fom_eff, fom_up;
  
  ClassDef(GlitchVeto,0)  
};

#endif


