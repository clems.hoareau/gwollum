//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "GlitchVeto.h"

ClassImp(GlitchVeto)

////////////////////////////////////////////////////////////////////////////////////
GlitchVeto::GlitchVeto(const int averbose){ 
////////////////////////////////////////////////////////////////////////////////////
  if(fverbose) cout<<"GlitchVeto::GlitchVeto: init a GlitchVeto object"<<endl;

  fverbose=averbose;
  status_OK=true;

  // nulling
  nfix=0;
  ntbins=0;
  nfauxbins=0;
  nsauxbins=0;
  nfmainbins=0;
  nsmainbins=0;
  nmaintrig=0;

  tbins=NULL;
  fauxbins=NULL;
  fmainbins=NULL;
  sauxbins=NULL;
  smainbins=NULL;

  h_Mstat=NULL; h_Mstat_cum=NULL;
  h_mr=NULL;
  g_Msnrtime=NULL;

  h_Astat=NULL; h_Astat_cum=NULL;
  h_dead=NULL; h_dead_cum=NULL; h_d=NULL;
  h_ar=NULL;
  g_snrsnr=NULL;  h_ff=NULL;

  h_nveto=NULL; h_nveto_cum=NULL;
  h_use=NULL; h_use_cum=NULL;
  h_up=NULL; h_e=NULL; h_ed=NULL;
  
  h_thr=NULL; h_thr_bin=NULL;
  h_Me=NULL;
  h_Ause=NULL;
  h_vr=NULL;
  g_snrsnr_veto=NULL;  g_ff_veto=NULL;  h_ff_veto=NULL; g_Msnrtime_veto=NULL;

  MainTriggers=NULL;
  AuxTriggers=NULL;
  VetoSeg=new Segments();

  // default no threshold
  UPthr=0;
  EDthr=-1;
  Dthr=-1;
  NVthr=-1;
  NUthr=-1;

  // Default segments
  Seg=new Segments();

  // init coincidence
  coinc=new Coinc(2,0,fverbose);
  coinc->SetDt(0);// The dt=0 is mandatory for the logic of this class! Do not change
  coinc->SetAf(1e20);

  // veto fom
  Tfom = new TTree("vetofom", "vetofom");
  Tfom->Branch("efficiency",&fom_eff,"efficiency/D");
  Tfom->Branch("deadtime",&fom_dt,"deadtime/D");
  Tfom->Branch("usepercentage",&fom_up,"usepercentage/D");
  
}

////////////////////////////////////////////////////////////////////////////////////
GlitchVeto::~GlitchVeto(void){
////////////////////////////////////////////////////////////////////////////////////
  if(fverbose) cout<<"GlitchVeto::~GlitchVeto"<<endl;
  ResetParameterSpace();
  delete coinc;
  delete Seg;
  delete VetoSeg;
  delete Tfom;
  MainIndexes.clear();
  AuxIndexes.clear();
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::ProcessGlitch2Glitch(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::ProcessGlitchToGlitch: the GlitchVeto object is corrupted"<<endl;
    return false;
  }
  if(AuxTriggers==NULL){
    cerr<<"GlitchVeto::ProcessGlitchToGlitch: the input aux. triggers are not set"<<endl;
    return false;
  }
  if(MainTriggers==NULL){
    cerr<<"GlitchVeto::ProcessGlitchToGlitch: the input main triggers are not set"<<endl;
    return false;
  }

  // Reset all
  if(fverbose) cout<<"GlitchVeto::ProcessGlitch2Glitch: reset parameter space..."<<endl;
  if(!ResetParameterSpace()){
    cerr<<"GlitchVeto::ProcessGlitchToGlitch: the reseting failed"<<endl;
    return false;
  }

  // make binning
  if(fverbose) cout<<"GlitchVeto::ProcessGlitch2Glitch: make binning..."<<endl;
  if(!MakeBinning(nfix)){
    cerr<<"GlitchVeto::ProcessGlitchToGlitch: the binning failed"<<endl;
    return false;
  }

  // Create parameter space
  if(fverbose) cout<<"GlitchVeto::ProcessGlitch2Glitch: create parameter space..."<<endl;
  if(!CreateParameterSpace()){
    cerr<<"GlitchVeto::ProcessGlitchToGlitch: the creating failed"<<endl;
    return false;
  }

  // make coinc
  if(fverbose) cout<<"GlitchVeto::ProcessGlitch2Glitch: make coinc..."<<endl;
  if(!coinc->MakeCoinc()){
    cerr<<"GlitchVeto::ProcessGlitchToGlitch: the coinc failed"<<endl;
    return false;
  }

  // retrieve coinc indexes
  MainIndexes.clear();
  AuxIndexes.clear();
  MainIndexes=coinc->GetCoincEvents(1,3);
  AuxIndexes=coinc->GetCoincEvents(0,3);
  
  // fill parameter space
  if(fverbose) cout<<"GlitchVeto::ProcessGlitch2Glitch: process..."<<endl;
  if(!FillParameterSpace()){
    cerr<<"GlitchVeto::ProcessGlitchToGlitch: the glitch2glitch analysis failed"<<endl;
    return false;
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::MakeVeto(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::MakeVeto: the GlitchVeto object is corrupted"<<endl;
    return false;
  }
  if(!nfauxbins||h_thr==NULL){
    cerr<<"GlitchVeto::MakeVeto: processing is not done"<<endl;
    return false;
  }

  double UPthr_updated;
  if(UPthr<0){
    UPthr_updated=GetUPthr(-UPthr);
    if(fverbose>1) cout<<"GlitchVeto::MakeVeto: Optimized UP threshold = "<<UPthr_updated<<endl;
  }
  else UPthr_updated=UPthr;

  // produce threshold
  if(fverbose) cout<<"GlitchVeto::MakeVeto: make SNR thresholds..."<<endl;
  h_thr->Reset();
  h_thr_bin->Reset();
  for(int f=1; f<=nfauxbins; f++){
    h_thr->SetBinContent(f,sauxbins[nsauxbins]+1);// no threshold
    h_thr_bin->SetBinContent(f,nsauxbins+1);

    for(int s=1; s<=nsauxbins; s++){
      if(h_up->GetBinContent(f,s)>UPthr_updated){// UP threshold
	if(h_ed->GetBinContent(f,s)>EDthr){// ED threshold
	  if(h_d->GetBinContent(f,s)>Dthr){// D threshold
	    if(h_nveto_cum->GetBinContent(f,s)>NVthr){// NV threshold
	      if(h_use_cum->GetBinContent(f,s)>NUthr){// NU threshold
		h_thr->SetBinContent(f,sauxbins[s-1]);
		h_thr_bin->SetBinContent(f,s);
		break;
	      }
	    }
	  }
	}
      }
    }
        
  }

  // produce veto segments
  if(fverbose) cout<<"GlitchVeto::MakeVeto: produce veto segments..."<<endl;
  delete VetoSeg;
  double esnr, efreq;
  vector <double> st, en;
  bool flagged_main;
  int c_prev=0;
  for(int e=0; e<AuxTriggers->GetNClusters(); e++){

    // Do not consider if it is in a coinc with a flagged main cluster
    flagged_main=false;
    for(int c=c_prev; c<(int)AuxIndexes.size()&&!flagged_main; c++){
      if(AuxIndexes[c]==e&&!MainTriggers->GetClusterTag(MainIndexes[c])) flagged_main=true;
      if(AuxIndexes[c]>e){
	c_prev=c;
	break;
      }
    }
    if(flagged_main) continue;

    efreq = AuxTriggers->GetClusterFrequency(e);
    esnr  = AuxTriggers->GetClusterSNR(e);
    if(efreq>=fauxbins[nfauxbins]) efreq=0.999*fauxbins[nfauxbins];
    if(esnr>=sauxbins[nsauxbins])  esnr=0.9999*sauxbins[nsauxbins];

    // this event should contribute to the veto
    if(esnr>h_thr->GetBinContent(h_thr->GetXaxis()->FindBin(efreq))){
      st.push_back(AuxTriggers->GetClusterTimeStart(e)); 
      en.push_back(AuxTriggers->GetClusterTimeEnd(e));
    }
  }
  VetoSeg = new Segments(st, en);
  VetoSeg->Intersect(Seg);
  st.clear();
  en.clear();

  // measure veto use and efficiency
  if(fverbose) cout<<"GlitchVeto::MakeVeto: measure veto efficiency on main triggers..."<<endl;
  int previous_mindex=-1, previous_aindex=-1, p=0, pm=0;
  double mfreq, mtime;
  h_Me->Reset();
  h_Ause->Reset();
  for(int c=0; c<(int)AuxIndexes.size(); c++){
    
    // Do not consider coincs with a flagged main cluster
    if(!MainTriggers->GetClusterTag(MainIndexes[c])) continue;

    // get aux event properties
    esnr  = AuxTriggers->GetClusterSNR(AuxIndexes[c]);
    efreq = AuxTriggers->GetClusterFrequency(AuxIndexes[c]);
    if(esnr>=sauxbins[nsauxbins])  esnr=0.9999*sauxbins[nsauxbins];
    if(efreq>=fauxbins[nfauxbins]) efreq=0.9999*fauxbins[nfauxbins];

    // below threshold -> skip
    if(esnr<=h_thr->GetBinContent(h_thr->GetXaxis()->FindBin(efreq))) continue;

    // outside segments
    if(!Seg->IsInsideSegment(AuxTriggers->GetClusterTimeEnd(AuxIndexes[c]))&&!Seg->IsInsideSegment(AuxTriggers->GetClusterTimeStart(AuxIndexes[c]))) continue; 
    if(!Seg->IsInsideSegment(MainTriggers->GetClusterTimeEnd(MainIndexes[c]))&&!Seg->IsInsideSegment(MainTriggers->GetClusterTimeStart(MainIndexes[c]))) continue; 

    // get main event properties
    mfreq = MainTriggers->GetClusterFrequency(MainIndexes[c]);
    mtime = MainTriggers->GetClusterTime(MainIndexes[c]);

    // saturation
    if(mfreq>=fmainbins[nfmainbins]) mfreq=0.9999*fmainbins[nfmainbins];
    
    if(MainIndexes[c]!=previous_mindex){
      h_Me->Fill(mfreq);
      h_vr->Fill(mtime);
      g_Msnrtime_veto->SetPoint(pm,mtime,MainTriggers->GetClusterSNR(MainIndexes[c]));
      pm++;
      previous_mindex=MainIndexes[c];
    }
    if(AuxIndexes[c]!=previous_aindex){
      h_Ause->Fill(efreq);
      previous_aindex=AuxIndexes[c];
    }

    // coinc plots
    g_snrsnr_veto->SetPoint(p,AuxTriggers->GetClusterSNR(AuxIndexes[c]),MainTriggers->GetClusterSNR(MainIndexes[c]));
    g_ff_veto->SetPoint(p,AuxTriggers->GetClusterFrequency(AuxIndexes[c]),MainTriggers->GetClusterFrequency(MainIndexes[c]));
    h_ff_veto->Fill(AuxTriggers->GetClusterFrequency(AuxIndexes[c]),MainTriggers->GetClusterFrequency(MainIndexes[c]));
    p++;
  }
  for(int f=1; f<=nfmainbins; f++) if(h_Mstat_cum->GetBinContent(f,1)) h_Me->SetBinContent(f,h_Me->GetBinContent(f)/h_Mstat_cum->GetBinContent(f,1));
  
  // time plots
  double lt;
  for(int b=1; b<=ntbins; b++){
    lt=Seg->GetLiveTime(tbins[b-1],tbins[b]);
    if(lt) h_vr->SetBinContent(b,h_vr->GetBinContent(b)/lt);
    else h_vr->SetBinContent(b,0);
  }
  
  // get veto FOM
  Tfom->Reset();
  fom_dt=GetVetoDeadTime();
  fom_eff=GetVetoEfficiency();
  fom_up=GetVetoUsePercentage();
  Tfom->Fill();

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
double GlitchVeto::GetUPthr(const double scalefactor){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK) return 0;
  if(AuxTriggers==NULL) return 0;
  
  // mean cluster duration in aux sample
  double meanclduration = AuxTriggers->GetClusterMeanDuration();
  if(meanclduration<=0) return 0;
  if(fverbose>1) cout<<"GlitchVeto::GetUPthr: mean aux. cluster duration = "<<meanclduration<<"s"<<endl;
  
  // number of fake coinc expected from Poisson processes
  double Nfake = h_Astat->GetEntries()*h_Mstat->GetEntries()*meanclduration/Seg->GetLiveTime();
  if(fverbose>1) cout<<"GlitchVeto::GetUPthr: expected number of coinc = "<<Nfake<<endl;
  
  return TMath::Min(1.0,scalefactor*Nfake/h_Astat->GetEntries());  
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::SetSegments(Segments *aNewSeg){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::SetSegments: the GlitchVeto object is corrupted"<<endl;
    return false;
  }
  if(aNewSeg==NULL){
    cerr<<"GlitchVeto::SetSegments: the input segments are null"<<endl;
    return false;
  }
  if(aNewSeg->GetNsegments()<=0){
    cerr<<"GlitchVeto::SetSegments: no input segments"<<endl;
    return false;
  }
  if(MainTriggers==NULL){
    cerr<<"GlitchVeto::SetSegments: the main triggers must be set first"<<endl;
    return false;
  }
  
  // copy segments structure
  if(fverbose) cout<<"GlitchVeto::SetSegments: sets new reference segments"<<endl;
  delete Seg; Seg = new Segments(aNewSeg->GetStarts(),aNewSeg->GetEnds());

  // intersect with main trigger segments
  Seg->Intersect(MainTriggers->GetSegments());
  
  return coinc->SetSegments(Seg);
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::SetAuxTriggers(ReadTriggers *Aauxtriggers){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::SetAuxTriggers: the GlitchVeto object is corrupted"<<endl;
    return false;
  }
  if(Aauxtriggers==NULL){
    cerr<<"GlitchVeto::SetAuxTriggers: the input triggers are null"<<endl;
    return false;
  }
  if(Aauxtriggers->GetNClusters()<=0){
    cerr<<"GlitchVeto::SetAuxTriggers: no input clusters"<<endl;
    return false;
  }
  if(fverbose) cout<<"GlitchVeto::SetAuxTriggers: sets aux triggers"<<endl;
  if(fverbose>1) cout<<"GlitchVeto::SetAuxTriggers: "<<Aauxtriggers->GetNTrig()<<" triggers"<<endl;
  if(fverbose>1) cout<<"GlitchVeto::SetAuxTriggers: "<<Aauxtriggers->GetNClusters()<<" clusters"<<endl;
  AuxTriggers=Aauxtriggers;
  return coinc->SetTriggers(0,AuxTriggers);
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::SetMainTriggers(ReadTriggers *Amaintriggers){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::SetMainTriggers: the GlitchVeto object is corrupted"<<endl;
    return false;
  }
  if(Amaintriggers==NULL){
    cerr<<"GlitchVeto::SetMainTriggers: the input triggers are null"<<endl;
    return false;
  }
  if(Amaintriggers->GetNClusters()<1){
    cerr<<"GlitchVeto::SetMainTriggers: no input clusters"<<endl;
    return false;
  }
  if(fverbose) cout<<"GlitchVeto::SetMainTriggers: sets main triggers"<<endl;
  if(fverbose>1) cout<<"GlitchVeto::SetMainTriggers: "<<Amaintriggers->GetNTrig()<<" triggers"<<endl;
  if(fverbose>1) cout<<"GlitchVeto::SetMainTriggers: "<<Amaintriggers->GetNClusters()<<" clusters"<<endl;
  MainTriggers=Amaintriggers;

  // copy segments structure
  if(fverbose>2) cout<<"GlitchVeto::SetMainTriggers: sets new segment reference"<<endl;
  delete Seg; Seg = new Segments(MainTriggers->GetSegments()->GetStarts(),MainTriggers->GetSegments()->GetEnds());
  
  return coinc->SetTriggers(1,MainTriggers);
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::SetThreshold(const string aVariableName, const double aThreshold){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::SetThreshold: the GlitcVeto object is corrupted"<<endl;
    return false;
  }
  
  if(!aVariableName.compare("UP")){
    UPthr=aThreshold;
    if(fverbose>1&&UPthr>=0) cout<<"GlitchVeto::SetThreshold: select UP > "<<UPthr<<endl;
    if(fverbose>1&&UPthr<0) cout<<"GlitchVeto::SetThreshold: select UP > **automatic**"<<endl;
  }
  else if(!aVariableName.compare("ED")){
    EDthr=aThreshold;
    if(fverbose>1) cout<<"GlitchVeto::SetThreshold: select ED > "<<EDthr<<endl;
  }
  else if(!aVariableName.compare("D")){
    Dthr=aThreshold;
    if(fverbose>1) cout<<"GlitchVeto::SetThreshold: select D > "<<Dthr<<endl;
  }
  else if(!aVariableName.compare("NV")){
    NVthr=aThreshold;
    if(fverbose>1) cout<<"GlitchVeto::SetThreshold: select NV > "<<NVthr<<endl;
  }
  else if(!aVariableName.compare("NU")){
    NUthr=aThreshold;
    if(fverbose>1) cout<<"GlitchVeto::SetThreshold: select NU > "<<NUthr<<endl;
  }
  else{
    cerr<<"GlitchVeto::SetThreshold: unknown variable name: "<<aVariableName<<endl;
    return false;
  }
    
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::WriteReport(const string aRootFileName){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::WriteReport: the GlitchVeto object is corrupted"<<endl;
    return false;
  }
  
  if(fverbose) cout<<"GlitchVeto::WriteReport: Write report in "<<aRootFileName<<endl;
  TTree *tt = VetoSeg->GetTree();
  TFile ff(aRootFileName.c_str(), "recreate");
  ff.cd();

  if(h_Mstat!=NULL)      h_Mstat->Write();
  if(h_Mstat_cum!=NULL)  h_Mstat_cum->Write();
  if(h_mr!=NULL)         h_mr->Write();
  if(g_Msnrtime!=NULL)   g_Msnrtime->Write();

  if(h_Astat!=NULL)      h_Astat->Write();
  if(h_Astat_cum!=NULL)  h_Astat_cum->Write();
  if(h_dead!=NULL)       h_dead->Write();
  if(h_dead_cum!=NULL)   h_dead_cum->Write();
  if(h_d!=NULL)          h_d->Write();
  if(h_ar!=NULL)         h_ar->Write();
  if(g_snrsnr!=NULL)     g_snrsnr->Write();
  if(h_ff!=NULL)         h_ff->Write();

  if(h_nveto!=NULL)     h_nveto->Write();
  if(h_nveto_cum!=NULL) h_nveto_cum->Write();
  if(h_use!=NULL);      h_use->Write();
  if(h_use_cum!=NULL)   h_use_cum->Write();
  if(h_up!=NULL)        h_up->Write();
  if(h_e!=NULL)         h_e->Write();
  if(h_ed!=NULL)        h_ed->Write();

  if(h_thr!=NULL)         h_thr->Write();
  if(h_Me!=NULL)          h_Me->Write();
  if(h_Ause!=NULL)        h_Ause->Write();
  if(h_vr!=NULL)          h_vr->Write();
  if(g_snrsnr_veto!=NULL) g_snrsnr_veto->Write();
  if(g_ff_veto!=NULL)     g_ff_veto->Write();
  if(h_ff_veto!=NULL)     h_ff_veto->Write();
  if(g_Msnrtime_veto!=NULL) g_Msnrtime_veto->Write();

  if(tt!=NULL) tt->Write();
  Tfom->Write();
  ff.Close();

  if(tt!=NULL) delete tt;
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
TH1* GlitchVeto::GetMonitor(const string MonitorName){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::GetMonitor: the GlitchVeto object is corrupted"<<endl;
    return NULL;
  }

  if(!MonitorName.compare("h_Astat"))           return h_Astat;
  else if(!MonitorName.compare("h_Astat_cum"))  return h_Astat_cum;
  else if(!MonitorName.compare("h_Mstat"))      return h_Mstat;
  else if(!MonitorName.compare("h_Mstat_cum"))  return h_Mstat_cum;
  else if(!MonitorName.compare("h_dead"))       return h_dead;
  else if(!MonitorName.compare("h_dead_cum"))   return h_dead_cum;
  else{
    cerr<<"GlitchVeto::GetMonitor: Monitor \""<<MonitorName<<"\" cannot be retrieved"<<endl;
    return NULL;
  }

  return NULL;
}

////////////////////////////////////////////////////////////////////////////////////
double GlitchVeto::GetFullDeadTime(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::GetFullDeadTime: the GlitchVeto object is corrupted"<<endl;
    return 0.0;
  }
  if(MainTriggers==NULL||nfauxbins<=0||h_dead_cum==NULL){
    cerr<<"GlitchVeto::GetFullDeadTime: cannot compute dead-time"<<endl;
    return 0.0;
  }

  double deadtime=0.0;  
  for(int f=1; f<=nfauxbins; f++) deadtime+=h_dead_cum->GetBinContent(f,1);
  
  if(Seg->GetLiveTime()) return deadtime/Seg->GetLiveTime();

  return 0.0;
}

////////////////////////////////////////////////////////////////////////////////////
double GlitchVeto::GetVetoEfficiency(const bool normalized){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::GetVetoEfficiency: the GlitchVeto object is corrupted"<<endl;
    return 0.0;
  }
  if(h_Me==NULL){
    cerr<<"GlitchVeto::GetVetoEfficiency: the veto was not produced"<<endl;
    return 0.0;
  }
  if(!nmaintrig) return 0.0;

  double efficiency=0.0;
  for(int f=1; f<=nfmainbins; f++) efficiency+=(h_Me->GetBinContent(f)*h_Mstat_cum->GetBinContent(f,1));
  if(!normalized) return efficiency;
  return efficiency/(double)nmaintrig;
}

////////////////////////////////////////////////////////////////////////////////////
double GlitchVeto::GetVetoDeadTime(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::GetVetoDeadTime: the GlitchVeto object is corrupted"<<endl;
    return 0.0;
  }
  if(MainTriggers==NULL||nfauxbins<=0||h_dead_cum==NULL||h_thr_bin==NULL){
    cerr<<"GlitchVeto::GetVetoDeadTime: cannot compute dead-time"<<endl;
    return 0.0;
  }

  double deadtime=0.0;
  for(int f=1; f<=nfauxbins; f++)
    deadtime+=h_dead_cum->GetBinContent(f,(int)h_thr_bin->GetBinContent(f));
  
  if(Seg->GetLiveTime())
    return deadtime/Seg->GetLiveTime();
  return 0.0;
}

////////////////////////////////////////////////////////////////////////////////////
double GlitchVeto::GetVetoUsePercentage(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"GlitchVeto::GetVetoUsePercentage: the GlitchVeto object is corrupted"<<endl;
    return 0.0;
  }
  if(MainTriggers==NULL||!nfauxbins||h_use_cum==NULL||h_thr_bin==NULL){
    cerr<<"GlitchVeto::GetVetoUsePercentage: cannot compute use-percentage"<<endl;
    return 0.0;
  }

  double up=0.0;
  double n=0.0;
  for(int f=1; f<=nfauxbins; f++){
    up+=h_Ause->GetBinContent(f);
    n+=h_Astat_cum->GetBinContent(f,(int)h_thr_bin->GetBinContent(f));
  }
  if(n>0) return up/n;
  
  return 0.0;
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::FillParameterSpace(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK) return false;

  double esnr, efreq, estart, eend;
  bool flagged_main;
  int c_prev=0;
  //**** AUX only events ************************
  if(fverbose) cout<<"GlitchVeto::FillParameterSpace: process aux. triggers"<<endl;
  for(int e=0; e<AuxTriggers->GetNClusters(); e++){

    // main live-time
    // this condition is necessary to meet what Coinc does
    if(!Seg->IsInsideSegment(AuxTriggers->GetClusterTimeEnd(e))&&!Seg->IsInsideSegment(AuxTriggers->GetClusterTimeStart(e))) continue;

    // Do not consider if it is in a coinc with a flagged main cluster
    flagged_main=false;
    for(int c=c_prev; c<(int)AuxIndexes.size()&&!flagged_main; c++){
      if(AuxIndexes[c]==e&&!MainTriggers->GetClusterTag(MainIndexes[c])) flagged_main=true;
      if(AuxIndexes[c]>e){
	c_prev=c;
	break;
      }
    }
    if(flagged_main) continue;
    
    // get event properties
    efreq =AuxTriggers->GetClusterFrequency(e);
    esnr  =AuxTriggers->GetClusterSNR(e);
    estart=AuxTriggers->GetClusterTimeStart(e);
    eend  =AuxTriggers->GetClusterTimeEnd(e);

    // saturation
    if(esnr>=sauxbins[nsauxbins])  esnr=0.9999*sauxbins[nsauxbins];
    if(efreq>=fauxbins[nfauxbins]) efreq=0.9999*fauxbins[nfauxbins];

    // fill histos
    h_ar->Fill(AuxTriggers->GetClusterTime(e));// rate
    h_Astat->Fill(efreq,esnr);                 // raw stat
    h_dead->Fill(efreq,esnr,eend-estart);      // dead-time
  }
  if(fverbose>1) cout<<"GlitchVeto::FillParameterSpace: "<<h_ar->GetEntries()<<" aux. clusters are considered"<<endl;

  //**** MAIN only events ************************
  if(fverbose) cout<<"GlitchVeto::FillParameterSpace: process main triggers"<<endl;
  nmaintrig=0;
  for(int e=0; e<MainTriggers->GetNClusters(); e++){

    // Do not consider flagged clusters
    if(!MainTriggers->GetClusterTag(e)) continue;
    
    if(!Seg->IsInsideSegment(MainTriggers->GetClusterTimeEnd(e))&&!Seg->IsInsideSegment(MainTriggers->GetClusterTimeStart(e))) continue; 

    // get event properties
    efreq =MainTriggers->GetClusterFrequency(e);
    esnr  =MainTriggers->GetClusterSNR(e);
 
    // saturation
    if(esnr>=smainbins[nsmainbins])  esnr=0.9999*smainbins[nsmainbins];
    if(efreq>=fmainbins[nfmainbins]) efreq=0.9999*fmainbins[nfmainbins];
    
    // fill histos
    h_Mstat->Fill(efreq,esnr);                  // raw stat
    h_mr->Fill(MainTriggers->GetClusterTime(e));// rate
    g_Msnrtime->SetPoint(nmaintrig, MainTriggers->GetClusterTime(e), esnr); 
    nmaintrig++;
  }
  if(fverbose>1) cout<<"GlitchVeto::FillParameterSpace: "<<h_Mstat->GetEntries()<<" main clusters are considered"<<endl;

  //**** COINC events ************************
  if(fverbose) cout<<"GlitchVeto::FillParameterSpace: process coinc triggers"<<endl;
  int previous_aindex=-1;
  int previous_mindex=-1;
  int previous_hbin=-1;
  int hbin;
  int previous_fbin=-1;
  int fbin, sbin, dummy;
  double mfreq, msnr;
  int p=0;
  for(int c=0; c<(int)AuxIndexes.size(); c++){

    // Do not consider coincs with a flagged main cluster
    if(!MainTriggers->GetClusterTag(MainIndexes[c])) continue;

    if(!Seg->IsInsideSegment(AuxTriggers->GetClusterTimeEnd(AuxIndexes[c]))&&!Seg->IsInsideSegment(AuxTriggers->GetClusterTimeStart(AuxIndexes[c]))) continue; 
    if(!Seg->IsInsideSegment(MainTriggers->GetClusterTimeEnd(MainIndexes[c]))&&!Seg->IsInsideSegment(MainTriggers->GetClusterTimeStart(MainIndexes[c]))) continue; 

    // get coinc event properties
    efreq =AuxTriggers->GetClusterFrequency(AuxIndexes[c]);
    esnr  =AuxTriggers->GetClusterSNR(AuxIndexes[c]);
    mfreq =MainTriggers->GetClusterFrequency(MainIndexes[c]);
    msnr  =MainTriggers->GetClusterSNR(MainIndexes[c]);

    // saturation
    if(esnr>=sauxbins[nsauxbins])    esnr=0.9999*sauxbins[nsauxbins];
    if(efreq>=fauxbins[nfauxbins])   efreq=0.9999*fauxbins[nfauxbins];
    if(msnr>=smainbins[nsmainbins])  msnr=0.9999*smainbins[nsmainbins];
    if(mfreq>=fmainbins[nfmainbins]) mfreq=0.9999*fmainbins[nfmainbins];

    // get f-s bin numbers
    hbin=h_nveto->FindBin(efreq,esnr);
    h_nveto->GetBinXYZ(hbin,fbin,sbin,dummy);

    if(MainIndexes[c]!=previous_mindex){// this event is new
      h_nveto->Fill(efreq,esnr);
      for(int s=1; s<=sbin; s++) h_nveto_cum->SetBinContent(fbin,s,h_nveto_cum->GetBinContent(fbin,s)+1);
    }
    else{
      if(hbin!=previous_hbin)// this event is not new but should be counted in a different bin
	h_nveto->Fill(efreq,esnr);
      
      if(fbin!=previous_fbin){// this event is not new but is in a different fbin
	for(int s=1; s<=sbin; s++) h_nveto_cum->SetBinContent(fbin,s,h_nveto_cum->GetBinContent(fbin,s)+1);
      }
    }
    
    // fill use
    if(AuxIndexes[c]!=previous_aindex)// aux cluster never counted before
      h_use->Fill(efreq,esnr);

    // fill coinc plots
    g_snrsnr->SetPoint(p,AuxTriggers->GetClusterSNR(AuxIndexes[c]),MainTriggers->GetClusterSNR(MainIndexes[c]));
    h_ff->Fill(AuxTriggers->GetClusterFrequency(AuxIndexes[c]),MainTriggers->GetClusterFrequency(MainIndexes[c]));
    p++;

    // save bin for next round
    previous_mindex=MainIndexes[c];
    previous_hbin=hbin;
    previous_fbin=fbin;
    previous_aindex=AuxIndexes[c];
  }
  
  // cumulative histos -> SNR threshold
  if(fverbose) cout<<"GlitchVeto::FillParameterSpace: build cumulative histos"<<endl;
  for(int f=1; f<=nfauxbins; f++){
   
    // init frequency band
    h_Astat_cum->SetBinContent(f,nsauxbins,h_Astat->GetBinContent(f,nsauxbins));
    h_dead_cum->SetBinContent(f,nsauxbins,h_dead->GetBinContent(f,nsauxbins));
    h_use_cum->SetBinContent(f,nsauxbins,h_use->GetBinContent(f,nsauxbins));
        
    // recursive over snr bins
    for(int s=nsauxbins-1; s>0; s--){
      h_Astat_cum->SetBinContent(f,s,h_Astat_cum->GetBinContent(f,s+1)+h_Astat->GetBinContent(f,s));
      h_dead_cum->SetBinContent(f,s,h_dead_cum->GetBinContent(f,s+1)+h_dead->GetBinContent(f,s));
      h_use_cum->SetBinContent(f,s,h_use_cum->GetBinContent(f,s+1)+h_use->GetBinContent(f,s));
    }

  }

  // same for main
  for(int f=1; f<=nfmainbins; f++){
    // init frequency band
    h_Mstat_cum->SetBinContent(f,nsmainbins,h_Mstat->GetBinContent(f,nsmainbins));
        
    // recursive over snr bins
    for(int s=nsmainbins-1; s>0; s--){
      h_Mstat_cum->SetBinContent(f,s,h_Mstat_cum->GetBinContent(f,s+1)+h_Mstat->GetBinContent(f,s));
    }
  }

  // processed histos
  if(fverbose) cout<<"GlitchVeto::FillParameterSpace: build proc histos"<<endl;
  if(h_up!=NULL) delete h_up;
  if(h_e!=NULL)  delete h_e;
  if(h_ed!=NULL) delete h_ed;
  if(h_d!=NULL)  delete h_d;
    
  // USE-PERCENTAGE
  h_up=(TH2D*)h_use_cum->Clone();
  h_up->Divide(h_Astat_cum);
  h_up->SetName("h_up");
  h_up->SetTitle("Use-percentage");
  h_up->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_up->GetYaxis()->SetTitle("Auxiliary SNR threshold");
  h_up->GetZaxis()->SetTitle("Use-percentage");
  
  // EFFICIENCY
  h_e=(TH2D*)h_nveto_cum->Clone();
  h_e->SetName("h_e");
  h_e->SetTitle("Rejection Efficiency");
  if(nmaintrig) h_e->Scale(1.0/(double)nmaintrig);
  else h_e->Scale(0.0);
  h_e->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_e->GetYaxis()->SetTitle("Auxiliary SNR threshold");
  h_e->GetZaxis()->SetTitle("Efficiency");

  // DEAD-TIME
  h_d=(TH2D*)h_dead_cum->Clone();
  h_d->SetName("h_d");
  h_d->SetTitle("Dead-time");
  if(Seg->GetLiveTime()) h_d->Scale(1.0/Seg->GetLiveTime());
  else h_d->Scale(0.0);
  h_d->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_d->GetYaxis()->SetTitle("Auxiliary SNR threshold");
  h_d->GetZaxis()->SetTitle("Dead-time");
   
  // EFFICIENCY / DEAD-TIME
  h_ed=(TH2D*)h_e->Clone();
  h_ed->SetName("h_ed");
  h_ed->SetTitle("Efficiency / Dead-time");
  h_ed->Divide(h_d);
  h_ed->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_ed->GetYaxis()->SetTitle("Auxiliary SNR threshold");
  h_ed->GetZaxis()->SetTitle("Efficiency / Dead-time");
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::MakeBinning(const int nfixbins){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK)         return false;
  if(AuxTriggers==NULL)  return false;
  if(MainTriggers==NULL) return false;

  //**************** AUX ****************
  if(fverbose) cout<<"GlitchVeto::MakeBinning: make aux. binning"<<endl;

  // frequency binning
  if(nfixbins<=0){
    nfauxbins=AuxTriggers->GetNClusters()/2000+3;
    if(nfauxbins>100) nfauxbins=100; // high stat
    if(AuxTriggers->GetNClusters()<10) nfauxbins=1; // low stat
  }
  else nfauxbins=nfixbins;
  if(fauxbins!=NULL); delete fauxbins;
  fauxbins = new double [nfauxbins+1];
  if(AuxTriggers->GetFrequencyMax()!=AuxTriggers->GetFrequencyMin()){
    for(int b=0; b<nfauxbins+1; b++) 
      fauxbins[b]=AuxTriggers->GetFrequencyMin()*pow(10.0,b*log10(1.2*AuxTriggers->GetFrequencyMax()/AuxTriggers->GetFrequencyMin())/(double)nfauxbins);
  }
  else{
    for(int b=0; b<nfauxbins+1; b++) 
      fauxbins[b]=1*pow(10.0,b*log10(1.2*10000.0/1.0)/(double)nfauxbins);
  }

  // snr binning
  nsauxbins=100;
  if(sauxbins!=NULL); delete sauxbins;
  sauxbins = new double [nsauxbins+1];
  for(int b=0; b<nsauxbins+1; b++)
    sauxbins[b]=3*pow(10.0,b*log10(100.0/3.0)/(double)nsauxbins);

  if(fverbose>1){
    cout<<"GlitchVeto::MakeBinning: (AUX) "<<nfauxbins<<" frequency bins "<<fauxbins[0]<<" - "<<fauxbins[nfauxbins]<<"Hz"<<endl;
    cout<<"GlitchVeto::MakeBinning: (AUX) "<<nsauxbins<<" SNR bins "<<sauxbins[0]<<" - "<<sauxbins[nsauxbins]<<endl;
  }

  //**************** MAIN ****************
  if(fverbose) cout<<"GlitchVeto::MakeBinning: make main binning"<<endl;

  // frequency binning
  if(nfixbins<=0){
    nfmainbins=MainTriggers->GetNClusters()/2000+3;
    if(nfmainbins>100) nfmainbins=100; // high stat
    if(MainTriggers->GetNClusters()<10) nfmainbins=1; // low stat
  }
  else nfmainbins=nfixbins;
  if(fmainbins!=NULL); delete fmainbins;
  fmainbins = new double [nfmainbins+1];
  if(MainTriggers->GetFrequencyMax()!=MainTriggers->GetFrequencyMin()){
    for(int b=0; b<nfmainbins+1; b++) 
      fmainbins[b]=MainTriggers->GetFrequencyMin()*pow(10.0,b*log10(1.2*MainTriggers->GetFrequencyMax()/MainTriggers->GetFrequencyMin())/(double)nfmainbins);
  }
  else{
    for(int b=0; b<nfmainbins+1; b++) 
      fmainbins[b]=1*pow(10.0,b*log10(1.2*10000.0/1.0)/(double)nfmainbins);
  }

  // snr binning
  nsmainbins=100;
  if(smainbins!=NULL); delete smainbins;
  smainbins = new double [nsmainbins+1];
  for(int b=0; b<nsmainbins+1; b++)
    smainbins[b]=3*pow(10.0,b*log10(100.0/3.0)/(double)nsmainbins);

  // time binning
  if(nfixbins<=0){
    ntbins=(int)((MainTriggers->GetTimeMax()-MainTriggers->GetTimeMin())/21600);
    if(!ntbins) ntbins=1;
    if(ntbins>500) ntbins=500;
  }
  else ntbins=nfixbins;
  double bwidth = (MainTriggers->GetTimeMax()-MainTriggers->GetTimeMin()) /ntbins;
  if(tbins!=NULL); delete tbins;
  tbins = new double [ntbins+1];
  for(int b=0; b<ntbins+1; b++)
    tbins[b]=MainTriggers->GetTimeMin()+b*bwidth;

  if(fverbose>1){
    cout<<"GlitchVeto::MakeBinning: "<<ntbins<<" time bins "<<tbins[0]<<" - "<<tbins[ntbins]<<endl;
    cout<<"GlitchVeto::MakeBinning: (MAIN) "<<nfmainbins<<" frequency bins "<<fmainbins[0]<<" - "<<fmainbins[nfmainbins]<<"Hz"<<endl;
    cout<<"GlitchVeto::MakeBinning: (MAIN) "<<nsmainbins<<" SNR bins "<<smainbins[0]<<" - "<<smainbins[nsmainbins]<<endl;
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::CreateParameterSpace(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK)              return false;
  if(!nsauxbins||!nsmainbins) return false;
  if(h_Mstat!=NULL)           return false;

  // main, raw stat
  h_Mstat = new TH2D("h_Mstat","Main clusters statistic",nfmainbins,fmainbins,nsmainbins,smainbins);
  h_Mstat->GetXaxis()->SetTitle("Main Frequency [Hz]");
  h_Mstat->GetYaxis()->SetTitle("Main SNR");
  h_Mstat->GetZaxis()->SetTitle("# events/bin");
  h_Mstat_cum = new TH2D("h_Mstat_cum","Main clusters statistic (cumulative)",nfmainbins,fmainbins,nsmainbins,smainbins);
  h_Mstat_cum->GetXaxis()->SetTitle("Main Frequency [Hz]");
  h_Mstat_cum->GetYaxis()->SetTitle("Main SNR threshold");
  h_Mstat_cum->GetZaxis()->SetTitle("# events/bin");

  // main rate
  h_mr = new TH1D("h_mr","Rate of main clusters",ntbins,tbins);
  h_mr->GetXaxis()->SetTitle("Time");
  h_mr->GetYaxis()->SetTitle("Rate [Hz]");
  
  // main snr vs. time
  g_Msnrtime = new TGraph();
  g_Msnrtime->SetName("g_Msnrtime");
  g_Msnrtime->SetTitle("Main clusters: SNR vs. time");

  // aux, raw stat
  h_Astat = new TH2D("h_Astat","Auxiliary clusters statistic",nfauxbins,fauxbins,nsauxbins,sauxbins);
  h_Astat->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_Astat->GetYaxis()->SetTitle("Auxiliary SNR");
  h_Astat->GetZaxis()->SetTitle("# events/bin");
  h_Astat_cum = new TH2D("h_Astat_cum","Auxiliary clusters statistic (cumulative)",nfauxbins,fauxbins,nsauxbins,sauxbins);
  h_Astat_cum->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_Astat_cum->GetYaxis()->SetTitle("Auxiliary SNR threshold");
  h_Astat_cum->GetZaxis()->SetTitle("# events/bin");

  // aux, dead-time
  h_dead = new TH2D("h_dead","Auxiliary Dead-time",nfauxbins,fauxbins,nsauxbins,sauxbins);
  h_dead->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_dead->GetYaxis()->SetTitle("Auxiliary SNR");
  h_dead->GetZaxis()->SetTitle("Dead-time [s]");
  h_dead_cum = new TH2D("h_dead_cum","Auxiliary Dead-time (cumulative)",nfauxbins,fauxbins,nsauxbins,sauxbins);
  h_dead_cum->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_dead_cum->GetYaxis()->SetTitle("Auxiliary SNR threshold");
  h_dead_cum->GetZaxis()->SetTitle("Dead-time [s]");

  // aux snr vs. time
  h_ar = new TH1D("h_ar","Rate of auxiliary clusters",ntbins,tbins);
  h_ar->GetXaxis()->SetTitle("Time");
  h_ar->GetYaxis()->SetTitle("Rate [Hz]");

  // snr/snr plot
  g_snrsnr = new TGraph();
  g_snrsnr->SetName("g_snrsnr");
  g_snrsnr->SetTitle("Coincidence SNR vs. SNR");

  // f/f plot
  h_ff = new TH2D("h_ff", "Coincidence frequency vs. frequency",nfauxbins,fauxbins,nfmainbins,fmainbins);

  // main nveto
  h_nveto = new TH2D("h_nveto","Number of vetoed main clusters",nfauxbins,fauxbins,nsauxbins,sauxbins);
  h_nveto->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_nveto->GetYaxis()->SetTitle("Auxiliary SNR");
  h_nveto->GetZaxis()->SetTitle("# vetoed events/bin");
  h_nveto_cum = new TH2D("h_nveto_cum","Number of vetoed main clusters",nfauxbins,fauxbins,nsauxbins,sauxbins);
  h_nveto_cum->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_nveto_cum->GetYaxis()->SetTitle("Auxiliary SNR threshold");
  h_nveto_cum->GetZaxis()->SetTitle("# vetoed events/bin");
  
  // aux use
  h_use = new TH2D("h_use","Number of used auxiliary clusters",nfauxbins,fauxbins,nsauxbins,sauxbins);
  h_use->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_use->GetYaxis()->SetTitle("Auxiliary SNR");
  h_use->GetZaxis()->SetTitle("# used events/bin");
  h_use_cum = new TH2D("h_use_cum","Number of  used auxiliary clusters",nfauxbins,fauxbins,nsauxbins,sauxbins);
  h_use_cum->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_use_cum->GetYaxis()->SetTitle("Auxiliary SNR threshold");
  h_use_cum->GetZaxis()->SetTitle("# used events/bin");
  
  // veto thresholds
  h_thr = new TH1D("h_thr","SNR threshold",nfauxbins,fauxbins);
  h_thr->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_thr->GetYaxis()->SetTitle("threshold");
  h_thr_bin = new TH1I("h_thr_bin","SNR bin",nfauxbins,fauxbins);

  // veto efficiency
  h_Me = new TH1D("h_Me","Veto rejection efficiency per main frequency bin",nfmainbins,fmainbins);
  h_Me->GetXaxis()->SetTitle("Main Frequency [Hz]");
  h_Me->GetYaxis()->SetTitle("Efficiency / bin");

  // veto use
  h_Ause = new TH1D("h_Ause","Veto use",nfauxbins,fauxbins);
  h_Ause->GetXaxis()->SetTitle("Auxiliary Frequency [Hz]");
  h_Ause->GetYaxis()->SetTitle("# used aux. clusters/bin");

  // veto efficiency over time
  h_vr = new TH1D("h_vr","Veto flagging rate over time",ntbins,tbins);
  h_vr->GetXaxis()->SetTitle("Time");
  h_vr->GetYaxis()->SetTitle("Veto flagging rate [Hz]");

  // snr/snr plot
  g_snrsnr_veto = new TGraph();
  g_snrsnr_veto->SetName("g_snrsnr_veto");
  g_snrsnr_veto->SetTitle("Coincidence SNR vs. SNR");
  g_snrsnr_veto->SetMarkerColor(2);

  // f/f plot
  g_ff_veto = new TGraph();
  g_ff_veto->SetName("g_ff_veto");
  g_ff_veto->SetTitle("Coincidence frequency vs. frequency");
  g_ff_veto->SetMarkerColor(2);
  h_ff_veto = new TH2D("h_ff_veto", "Coincidence frequency vs. frequency",nfauxbins,fauxbins,nfmainbins,fmainbins);

  // snr vs time plot of main triggers
  g_Msnrtime_veto = new TGraph();
  g_Msnrtime_veto->SetName("g_Msnrtime_veto");
  g_Msnrtime_veto->SetTitle("SNR over time of vetoed main clusters");
  g_Msnrtime_veto->SetMarkerColor(2);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchVeto::ResetParameterSpace(void){
////////////////////////////////////////////////////////////////////////////////////
  if(tbins!=NULL)           delete tbins;           tbins=NULL;
  if(fauxbins!=NULL)        delete fauxbins;        fauxbins=NULL;
  if(fmainbins!=NULL)       delete fmainbins;       fmainbins=NULL;
  if(sauxbins!=NULL)        delete sauxbins;        sauxbins=NULL;
  if(smainbins!=NULL)       delete smainbins;       smainbins=NULL;

  if(h_Mstat!=NULL)         delete h_Mstat;         h_Mstat=NULL;
  if(h_Mstat_cum!=NULL)     delete h_Mstat_cum;     h_Mstat_cum=NULL;
  if(h_mr!=NULL)            delete h_mr;            h_mr=NULL;
  if(g_Msnrtime!=NULL)      delete g_Msnrtime;      g_Msnrtime=NULL;
  if(h_Astat!=NULL)         delete h_Astat;         h_Astat=NULL;
  if(h_Astat_cum!=NULL)     delete h_Astat_cum;     h_Astat_cum=NULL;
  if(h_dead!=NULL)          delete h_dead;          h_dead=NULL;
  if(h_dead_cum!=NULL)      delete h_dead_cum;      h_dead_cum=NULL;
  if(h_d!=NULL)             delete h_d;             h_d=NULL;
  if(h_ar!=NULL)            delete h_ar;            h_ar=NULL;
  if(g_snrsnr!=NULL)        delete g_snrsnr;        g_snrsnr=NULL;
  if(h_ff!=NULL)            delete h_ff;            h_ff=NULL;

  if(h_nveto!=NULL)         delete h_nveto;         h_nveto=NULL;
  if(h_nveto_cum!=NULL)     delete h_nveto_cum;     h_nveto_cum=NULL;
  if(h_use!=NULL)           delete h_use;           h_use=NULL;
  if(h_use_cum!=NULL)       delete h_use_cum;       h_use_cum=NULL;
  if(h_up!=NULL)            delete h_up;            h_up=NULL;
  if(h_e!=NULL)             delete h_e;             h_e=NULL;
  if(h_ed!=NULL)            delete h_ed;            h_ed=NULL;
  
  if(h_thr!=NULL)           delete h_thr;           h_thr=NULL;
  if(h_thr_bin!=NULL)       delete h_thr_bin;       h_thr_bin=NULL;
  if(h_Me!=NULL)            delete h_Me;            h_Me=NULL;
  if(h_Ause!=NULL)          delete h_Ause;          h_Ause=NULL;
  if(h_vr!=NULL)            delete h_vr;            h_vr=NULL;
  if(g_snrsnr_veto!=NULL)   delete g_snrsnr_veto;   g_snrsnr_veto=NULL;
  if(g_ff_veto!=NULL)       delete g_ff_veto;       g_ff_veto=NULL;
  if(h_ff_veto!=NULL)       delete h_ff_veto;       h_ff_veto=NULL;
  if(g_Msnrtime_veto!=NULL) delete g_Msnrtime_veto; g_Msnrtime_veto=NULL;
  VetoSeg->Reset();

  ntbins=0;
  nfauxbins=0;
  nfmainbins=0;
  nsauxbins=0;
  nsmainbins=0;
  Tfom->Reset();
  nmaintrig=0;

  return true;
}
