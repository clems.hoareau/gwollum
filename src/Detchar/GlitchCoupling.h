//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __GlitchCoupling__
#define __GlitchCoupling__

#include "Coinc.h"
#include "TriggerSelect.h"
#include "TH2.h"
#include "TGraph.h"

using namespace std;

/**
 * Study and characterize glitch-to glitch coupling between 2 trigger data sets.
 * The coupling is characterized using 'monitors' which are histograms measuring the glitch coincidence rate as a function of the glitch parameters. This the reason why this class inherits from the Coinc class. The 2 trigger sets are defined by 2 ReadTriggers objects the user must provide with the SetTriggers() function. The trigger set indexed with '0' is called the 'target' sample while the index '1' designates the 'source' sample. The user must cluster the triggers before calling the SetTriggers() function. Only "TIME" clustering methods are supported.
 *
 * By default, the coupling is only characterized over the time common to both trigger samples. The user can reduce this time range using the Coinc::SetSegments() function.
 *
 * The minimum use of this class is defined by the following sequence of function calls:
 * - GlitchCoupling(): constructor.
 * - SetTriggers(): defines the trigger samples. It should be called twice: for the target and source samples.
 * - Coinc::SetSegments(): defines the time segments used for the characterization (optional). 
 * - Monitor(): builds the single trigger sample monitors. It should be called twice: for the target and source samples.
 *
 * After the coupling characterization, this class offers many functions to design a veto targetting the target triggers and using the source triggers.
 * 
 * \author    Florent Robinet
 */
class GlitchCoupling: public Coinc {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the GlitchCoupling class.
   * @param aVerbose verbosity level
   */
  GlitchCoupling(const int aVerbose=0);

  /**
   * Destructor of the GlitchCoupling class.
   */
  virtual ~GlitchCoupling(void);
  /**
     @}
  */

  /**
   * Sets input source/target triggers.
   * The input triggers must be clustered before calling this function.
   * @param aC input index: 0=target, 1=source
   * @param aTriggers input triggers
   */
  bool SetTriggers(const int aC, ReadTriggers *aTriggers);

  inline bool SetSegments(Segments *aSeg){ return Coinc::SetSegments(aSeg); };
  bool WriteMonitors(const string aRootFileName);

 private:

  // TRIGGERS
  ReadTriggers *triggers[2];        ///< input target/source triggers
  string triggertype[2];            ///< trigger type: source or target
  TriggerSelect* Tselect[2];        ///< trigger selection

  // MONITORS
  void ResetMonitors(const int aC);
  void Monitor(const int aC);
  TH2D *h_stat[2], *h_stat_cum[2]; ///< raw statistics histos
  TH2D *h_glitchtime[2], *h_glitchtime_cum[2]; ///< dead histos


  
  ClassDef(GlitchCoupling,0)  
};

#endif


