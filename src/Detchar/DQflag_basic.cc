//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "ReadTriggers.h"

using namespace std;

int main (int argc, char* argv[]){
  gErrorIgnoreLevel = 3000;

  // check the output directory
  if(argc!=5){
    cerr<<"usage:"<<endl;
    cerr<<argv[0]<<" [trigger pattern] [time padding] [flag file] [trigger selection]"<<endl; 
    cerr<<"[trigger pattern] full path to trigger files"<<endl; 
    cerr<<"[time padding] time padding to be added on both trigger ends (tstart and tend)"<<endl; 
    cerr<<"[flag file] path to the output file"<<endl; 
    cerr<<"[trigger selection]:"<<endl; 
    cerr<<"         \"snrmin;snrmax;freqmin;freqmax;clustering\""<<endl;
    cerr<<"         -1 = no selection"<<endl;
    cerr<<""<<endl;
    cerr<<"         clustering= YES or NO"<<endl;
    cerr<<""<<endl;
    return 1;
  }
  
  // arguments
  string t_pattern=(string)argv[1];
  double Padding=fabs(atof(argv[2]));
  string dqfilename=(string)argv[3];
  string sel=(string)argv[4];
  
  vector<string> ssel = SplitString(sel,';');
  if(ssel.size()!=5){
    cerr<<argv[0]<<": the selection must have the following format"<<endl;
    cerr<<"           \"snrmin;snrmax;freqmin;freqmax;clustering\""<<endl;
    cerr<<"           -1 = no selection"<<endl;
    cerr<<""<<endl;
    cerr<<"         clustering= YES or NO"<<endl;
    return 1;
  }
  
  // convert selection parameters
  double selection[4];
  for(int i=0; i<4; i++){
    istringstream iss(ssel[i]);
    iss>>selection[i];
  }
  
  // loading triggers
  cout<<argv[0]<<": loading triggers..."<<endl;
  ReadTriggers *triggers = new ReadTriggers(t_pattern, "", 1);
  int ntriggers = triggers->GetNTrig();
  if(!ntriggers){
    cerr<<argv[0]<<": no trigger"<<endl;
    delete triggers;
    return 2;
  }

  vector<double> st, en;

  // clustering if requested
  if(!ssel[4].compare("YES")){
    if(!triggers->Clusterize()){
      cerr<<argv[0]<<": clustering failed"<<endl;
      delete triggers;
      return 2;
    }
  
    // loop over clusters
    cout<<argv[0]<<": selecting clusters..."<<endl;
    for(int t=0; t<triggers->GetNCluster(); t++){
      
      // apply selection
      if(selection[0]+1&&triggers->GetClusterSNR(t)<selection[0]) continue;
      if(selection[1]+1&&triggers->GetClusterSNR(t)>=selection[1]) continue;
      if(selection[2]+1&&triggers->GetClusterFrequency(t)<selection[2]) continue;
      if(selection[3]+1&&triggers->GetClusterFrequency(t)>=selection[3]) continue;

      // add new segment
      st.push_back(floor((triggers->GetClusterTimeStart(t)-Padding)*100000)/100000); en.push_back(floor((triggers->GetClusterTimeEnd(t)+Padding)*100000)/100000);
      //FIXME: remove the floor thing at some point.
    }
  }

  // loop over triggers
  else{
    cout<<argv[0]<<": selecting triggers..."<<endl;
    for(int t=0; t<ntriggers; t++){
      
      // apply selection
      if(selection[0]+1&&triggers->GetTriggerSNR(t)<selection[0]) continue;
      if(selection[1]+1&&triggers->GetTriggerSNR(t)>=selection[1]) continue;
      if(selection[2]+1&&triggers->GetTriggerFrequency(t)<selection[2]) continue;
      if(selection[3]+1&&triggers->GetTriggerFrequency(t)>=selection[3]) continue;
      
      // add new segment
      st.push_back(floor((triggers->GetTriggerTimeStart(t)-Padding)*100000)/100000); en.push_back(floor((triggers->GetTriggerTimeEnd(t)+Padding)*100000)/100000);
      //FIXME: remove the floor thing at some point.
      
    }
  }

  delete triggers;
  
  cout<<argv[0]<<": producing DQ flag..."<<endl;
  Segments *DQ = new Segments(st,en);
  st.clear(); en.clear();

  // write DQ file
  cout<<argv[0]<<": writing DQ flag..."<<endl;
  DQ->Write(dqfilename);

  //cleaning
  delete DQ;
  cout<<argv[0]<<": DONE!"<<endl;

  // exit
  return 0;
}

