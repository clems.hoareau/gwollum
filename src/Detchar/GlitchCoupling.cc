//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "GlitchCoupling.h"

ClassImp(GlitchCoupling)

////////////////////////////////////////////////////////////////////////////////////
GlitchCoupling::GlitchCoupling(const int aVerbose): Coinc(2,0,aVerbose) { 
////////////////////////////////////////////////////////////////////////////////////
  
  // input triggers
  triggers[0]=NULL;
  triggers[1]=NULL;
  triggertype[0]="Target";
  triggertype[1]="Source";
  Tselect[0] = new TriggerSelect(verbose);
  Tselect[1] = new TriggerSelect(verbose);

  // monitors
  h_stat[0]           = new TH2D();
  h_stat[1]           = new TH2D();
  h_stat_cum[0]       = new TH2D();
  h_stat_cum[1]       = new TH2D();
  h_glitchtime[0]     = new TH2D();
  h_glitchtime[1]     = new TH2D();
  h_glitchtime_cum[0] = new TH2D();
  h_glitchtime_cum[1] = new TH2D();
  ResetMonitors(0);
  ResetMonitors(1);
}

////////////////////////////////////////////////////////////////////////////////////
GlitchCoupling::~GlitchCoupling(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbose>1) cout<<"GlitchCoupling::~GlitchCoupling"<<endl;
  for(int i=0; i<2; i++){
    delete Tselect[i];
    delete h_stat[i];
    delete h_stat_cum[i];
    delete h_glitchtime[i];
    delete h_glitchtime_cum[i];
  }
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchCoupling::SetTriggers(const int aC, ReadTriggers *aTriggers){
////////////////////////////////////////////////////////////////////////////////////
  if(aC<0||aC>1){
    cerr<<"GlitchCoupling::SetTriggers: the input index is either 0 (target) or 1 (source)"<<endl;
    return false;
  }
  if(aTriggers==NULL){
    cerr<<"GlitchCoupling::SetTriggers: the input triggers structure is NULL"<<endl;
    return false;
  }
  if(aTriggers->GetClusterStatus().compare("TIME_MAX")&&aTriggers->GetClusterStatus().compare("TIME_MEAN")){
    cerr<<"GlitchCoupling::SetTriggers: the GlitchCoupling class only works with TIME clusters"<<endl;
    return false;
  }
  
  // update triggers
  triggers[aC] = aTriggers;
  
  // update selection
  delete Tselect[aC];
  Tselect[aC] = new TriggerSelect(triggers[aC], verbose);
  Tselect[aC]->SetFrequencyResolution(TMath::Min(100,triggers[aC]->GetNClusters()/2000+3));
  Tselect[aC]->SetSNRRange(Tselect[aC]->GetSNRMin(),TMath::Max(100.0,2*Tselect[aC]->GetSNRMin()));
  Tselect[aC]->SetSNRResolution(50);

  // update coinc
  if(verbose) cout<<"GlitchCoupling::SetTriggers: make lags"<<endl;
  if(!Coinc::MakeLags(1,10)){// FIXME!
    cerr<<"GlitchCoupling::SetTriggers: cannot make lags"<<endl;
    return false;
  }
  if(verbose>1) cout<<"GlitchCoupling::SetTriggers: new time-shift = "<<Coinc::GetTimeOffset(aC,1)<<endl;
  if(!Coinc::SetTriggers(aC,triggers[aC])){
    cerr<<"GlitchCoupling::SetTriggers: cannot set triggers"<<endl;
    return false;
  }

  // update Monitors
  if(verbose) cout<<"GlitchCoupling::SetTriggers: reset monitors"<<endl;
  ResetMonitors(aC);
  if(verbose) cout<<"GlitchCoupling::SetTriggers: make monitors"<<endl;
  Monitor(aC);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void GlitchCoupling::ResetMonitors(const int aC){
////////////////////////////////////////////////////////////////////////////////////

  // statistics
  delete h_stat[aC];
  h_stat[aC]=Tselect[aC]->GetTH2D("Frequency","SNR");
  h_stat[aC]->SetName((triggertype[aC]+"_stat").c_str());
  if(triggers[aC]!=NULL) h_stat[aC]->SetTitle((triggers[aC]->GetStreamName()+": number of clusters").c_str());
  else h_stat[aC]->SetTitle((triggertype[aC]+": number of clusters").c_str());
 
  delete h_stat_cum[aC];
  h_stat_cum[aC]=Tselect[aC]->GetTH2D("Frequency","SNR");
  h_stat_cum[aC]->SetName((triggertype[aC]+"_stat_cum").c_str());
  if(triggers[aC]!=NULL) h_stat_cum[aC]->SetTitle((triggers[aC]->GetStreamName()+": cumulative number of clusters").c_str());
  else h_stat_cum[aC]->SetTitle((triggertype[aC]+": cumulative number of clusters").c_str());
 
  // glitch time
  delete h_glitchtime[aC];
  h_glitchtime[aC]=Tselect[aC]->GetTH2D("Frequency","SNR");
  h_glitchtime[aC]->SetName((triggertype[aC]+"_glitchtime").c_str());
  if(triggers[aC]!=NULL) h_glitchtime[aC]->SetTitle((triggers[aC]->GetStreamName()+": glitch time [s]").c_str());
  else h_glitchtime[aC]->SetTitle((triggertype[aC]+": glitch time [s]").c_str());
  
  delete h_glitchtime_cum[aC];
  h_glitchtime_cum[aC]=Tselect[aC]->GetTH2D("Frequency","SNR");
  h_glitchtime_cum[aC]->SetName((triggertype[aC]+"_glitchtime_cum").c_str());
  if(triggers[aC]!=NULL) h_glitchtime_cum[aC]->SetTitle((triggers[aC]->GetStreamName()+": cumulative glitch time [s]").c_str());
  else h_glitchtime_cum[aC]->SetTitle((triggertype[aC]+": cumulative glitch time [s]").c_str());
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GlitchCoupling::Monitor(const int aC){
////////////////////////////////////////////////////////////////////////////////////

  if(triggers[aC]==NULL) return ResetMonitors(aC);

  // init segments for each fbin
  Segments **fseg = new Segments* [h_stat[aC]->GetXaxis()->GetNbins()];
  for(int f=0; f<h_stat[aC]->GetXaxis()->GetNbins(); f++){
    fseg[f] = new Segments();
  }
  
  // fill monitors
  int x,y;
  for(int c=0; c<triggers[aC]->GetNClusters(); c++){
    h_stat[aC]->Fill(triggers[aC]->GetClusterFrequency(c),triggers[aC]->GetClusterSNR(c));

    // get bin
    x=h_stat[aC]->GetXaxis()->FindBin(triggers[aC]->GetClusterFrequency(c));
    y=h_stat[aC]->GetYaxis()->FindBin(triggers[aC]->GetClusterSNR(c));

    // tag glitch segment
    fseg[x-1]->AddSegment(triggers[aC]->GetClusterTimeStart(c),triggers[aC]->GetClusterTimeEnd(c),y);// FIXME: assumes non-overlapping clusters
  }

  // compute glitch time
  for(int f=1; f<=h_stat[aC]->GetXaxis()->GetNbins(); f++){
    
    for(int s=1; s<=h_stat[aC]->GetYaxis()->GetNbins()+1; s++)// include snr overflow
      h_glitchtime[aC]->SetBinContent(f,s,fseg[f-1]->GetLiveTimeWithTag(s));
    delete fseg[f-1];
  }
  delete fseg;
  
  // cumulative
  for(int f=1; f<=h_stat[aC]->GetXaxis()->GetNbins(); f++){
    
    // init with SNR overflow
    h_stat_cum[aC]->SetBinContent(f,h_stat[aC]->GetYaxis()->GetNbins()+1,h_stat[aC]->GetBinContent(f,h_stat[aC]->GetYaxis()->GetNbins()+1));
    h_glitchtime_cum[aC]->SetBinContent(f,h_glitchtime[aC]->GetYaxis()->GetNbins()+1,h_glitchtime[aC]->GetBinContent(f,h_glitchtime[aC]->GetYaxis()->GetNbins()+1));
        
    // go down in SNR
    for(int s=h_stat[aC]->GetYaxis()->GetNbins(); s>0; s--){
      h_stat_cum[aC]->SetBinContent(f,s,h_stat_cum[aC]->GetBinContent(f,s+1)+h_stat[aC]->GetBinContent(f,s));
      h_glitchtime_cum[aC]->SetBinContent(f,s,h_glitchtime_cum[aC]->GetBinContent(f,s+1)+h_glitchtime[aC]->GetBinContent(f,s));
    }
  }
 
  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool GlitchCoupling::WriteMonitors(const string aRootFileName){
////////////////////////////////////////////////////////////////////////////////////
  
  TFile wm(aRootFileName.c_str(), "recreate");
  if(wm.IsZombie()){
    cerr<<"GlitchCoupling::WriteMonitors: cannot open "<<aRootFileName<<endl;
    return false;
  }
  wm.cd();

  if(verbose) cout<<"GlitchCoupling::WriteReport: Write report in "<<aRootFileName<<endl;
  
  for(int i=0; i<2; i++){
    h_stat[i]->Write();
    h_stat_cum[i]->Write();
    h_glitchtime[i]->Write();
    h_glitchtime_cum[i]->Write();
  }

  wm.Close();
  return true;
}
