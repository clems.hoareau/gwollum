//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "EventMap.h"

ClassImp(EventMap)

////////////////////////////////////////////////////////////////////////////////////
EventMap::EventMap(const int aNmaps, const string aPattern, const string aDirectory, const int aVerbose) : GwollumPlot ("EM","FIRE"), ReadTriggers (aPattern, aDirectory, aVerbose){ 
////////////////////////////////////////////////////////////////////////////////////

  // number of maps
  Nmaps = TMath::Max((int)fabs(aNmaps),1);

  // init maps
  Map              = new TH2D*           [Nmaps];
  MapSNR           = new double**        [Nmaps];
  MapNoiseAmp      = new double*         [Nmaps];
  MapParam         = new TriggerSelect*  [Nmaps];
  MapCentralTime   = new double          [Nmaps];
  MapLoudestBin    = new int             [Nmaps];
  MapLoudestSNR_bf = new double*         [Nmaps];
  MapBinXmin       = new int             [Nmaps];
  MapBinXmax       = new int             [Nmaps];
  MapBinYmin       = new int             [Nmaps];
  MapBinYmax       = new int             [Nmaps];

  // create maps
  for(int m=0; m<Nmaps; m++){
    MapParam[m] = new TriggerSelect(this,ReadTriggerSegments::Verbose);
    MapParam[m]->SetTimeSelection(201, -2.0, 2.0);// default time range
    MapParam[m]->SetFrequencyResolution(200);// default frequency resolution

    // build dummy map
    // it will be deleted and built in ResetMap()
    Map[m]              = new TH2D();
    MapSNR[m]           = new double* [3];
    MapNoiseAmp[m]      = new double [3];
    MapLoudestSNR_bf[m] = new double [3];
    for(int bt=0; bt<3; bt++) MapSNR[m][bt] = new double [3];

    // build map
    ResetMap(m);
  }

  /*
   * IMPORTANT: map under/over flows are filled but they don't make much sense!
   *            Do not use!
   */
  
  // by default, SNR content
  SetMapSNRContent();
 
  // adjust map style
  Draw(Map[0],"COLZ");
  MapPrinted=0;
  SetLogy(1);
  SetLogz(1);
  SetGridx(1);
  SetGridy(1);
  AddText("Loudest tile:", 0.01,0.01,0.03);
}

////////////////////////////////////////////////////////////////////////////////////
EventMap::~EventMap(void){
////////////////////////////////////////////////////////////////////////////////////
  if(ReadTriggerSegments::Verbose>1) cout<<"EventMap::~EventMap"<<endl;
  for(int m=0; m<Nmaps; m++){
    for(int bt=0; bt<Map[m]->GetNbinsX()+2; bt++) delete MapSNR[m][bt];
    delete MapSNR[m];
    delete MapNoiseAmp[m];
    delete MapLoudestSNR_bf[m];
    delete Map[m];
    delete MapParam[m];
  }
  delete Map;
  delete MapSNR;
  delete MapNoiseAmp;
  delete MapParam;
  delete MapCentralTime;
  delete MapLoudestBin;
  delete MapLoudestSNR_bf;
  delete MapBinXmin;
  delete MapBinXmax;
  delete MapBinYmin;
  delete MapBinYmax;
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::BuildMap(const int aMapIndex, const double aTime){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps){
    cerr<<"EventMap::BuildMap: Map #"<<aMapIndex<<" does not exist"<<endl;
    return false;
  }
 
  if(ReadTriggerSegments::Verbose>2) cout<<"EventMap::BuildMap: map #"<<aMapIndex<<", time = "<<aTime<<endl;

  // reset map content
  ResetMapContent(aMapIndex);

  // map boundaries
  double maptmin=aTime+Map[aMapIndex]->GetXaxis()->GetBinLowEdge(1);
  double maptmax=aTime+Map[aMapIndex]->GetXaxis()->GetBinUpEdge(Map[aMapIndex]->GetNbinsX());
  double mapfmin=Map[aMapIndex]->GetYaxis()->GetBinLowEdge(1);
  double mapfmax=Map[aMapIndex]->GetYaxis()->GetBinUpEdge(Map[aMapIndex]->GetNbinsY());
  double mapqmin=MapParam[aMapIndex]->GetQMin();
  double mapqmax=MapParam[aMapIndex]->GetQMax();

  // local tile parameters
  int bin_start_t, bin_start_f,bin_end_t, bin_end_f; 
  double snrmax=-1.0;
   
  // pointer to map content
  double *content;
  if(MapContentType) content=&Tamp;
  else content=&Tsnr;
  
  // search first tree to consider
  int ft = 0;
  for(; ft<ReadTriggerSegments::GetNFiles(); ft++){
    if(Tstop[ft]>maptmin) break;// found the first tree
  }
  
  // read triggers starting at this tree
  for(int t=Toffsets[ft]; t<Ttree->GetEntries(); t++){
    Ttree->GetEntry(t);
    
    // time selection to fit in the map
    if(Ttend<maptmin) continue;// before the map
    if(Ttstart>=maptmax) break;// after the map
    
    // frequency selection to fit in the map
    if(Tfend<mapfmin) continue;   // below the map
    if(Tfstart>=mapfmax) continue;// above the map

    // Q selection
    if(Tq<mapqmin) continue;
    if(Tq>=mapqmax) continue;
      
    // bins to cover
    bin_start_t = Map[aMapIndex]->GetXaxis()->FindBin(Ttstart-aTime);
    bin_start_f = Map[aMapIndex]->GetYaxis()->FindBin(Tfstart);
    bin_end_t   = Map[aMapIndex]->GetXaxis()->FindBin(Ttend-aTime);
    bin_end_f   = Map[aMapIndex]->GetYaxis()->FindBin(Tfend);
        
    // loudest bin
    if(Tsnr>snrmax){// save loudest tile of the map
      MapLoudestBin[aMapIndex]=Map[aMapIndex]->GetBin((bin_start_t+bin_end_t)/2,(int)(sqrt((double)(bin_start_f*bin_end_f))));
      snrmax=Tsnr;
    }
    
    for(int bf=bin_start_f; bf<=bin_end_f; bf++){// freq-sweep the tile
      if(Tsnr>MapLoudestSNR_bf[aMapIndex][bf]){// get noise amplitude for this band
	MapLoudestSNR_bf[aMapIndex][bf]=Tsnr;
	MapNoiseAmp[aMapIndex][bf]=Tamp/Tsnr;
      }
      for(int bt=bin_start_t; bt<=bin_end_t; bt++){// time-sweep the tile
      	if(Tsnr>MapSNR[aMapIndex][bt][bf]){// keep only the loudest tile
	  Map[aMapIndex]->SetBinContent(bt,bf,*content);// update map
	  MapSNR[aMapIndex][bt][bf]=Tsnr;
	}
      }
    }

    // save for optimized match computation
    if(bin_start_t<MapBinXmin[aMapIndex]) MapBinXmin[aMapIndex]=bin_start_t;
    if(bin_start_f<MapBinYmin[aMapIndex]) MapBinYmin[aMapIndex]=bin_start_f;
    if(bin_end_t>MapBinXmax[aMapIndex])   MapBinXmax[aMapIndex]=bin_end_t;
    if(bin_end_f>MapBinYmax[aMapIndex])   MapBinYmax[aMapIndex]=bin_end_f;
  }

  // map title
  ostringstream tmpstream;
  tmpstream.flags ( ios::fixed );
  tmpstream<<GetStreamName()<<": "<<setprecision(3)<<"GPS = "<<aTime;
  Map[aMapIndex]->SetTitle(tmpstream.str().c_str());
  MapCentralTime[aMapIndex]=aTime;
  tmpstream.str(""); tmpstream.clear();

  // get loudest tile coordinates
  int x,y,z;
  Map[aMapIndex]->GetBinXYZ(MapLoudestBin[aMapIndex],x,y,z);
  if(ReadTriggerSegments::Verbose>2) cout<<"EventMap::BuildMap: Loudest tile: time="<<Map[aMapIndex]->GetXaxis()->GetBinCenter(x)+MapCentralTime[aMapIndex]<<", frequency = "<<Map[aMapIndex]->GetYaxis()->GetBinCenterLog(y)<<", SNR = "<<MapSNR[aMapIndex][x][y]<<endl;

  // update map loudest tile info
  if(aMapIndex==MapPrinted){
    tmpstream<<"Loudest tile: GPS="<<fixed<<setprecision(3)<<Map[aMapIndex]->GetXaxis()->GetBinCenter(x)+MapCentralTime[aMapIndex]<<", f="<<Map[aMapIndex]->GetYaxis()->GetBinCenterLog(y)<<" Hz, SNR="<<MapSNR[aMapIndex][x][y];
    UpdateText(tmpstream.str());
  }

  return true;

}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::BuildClusterMap(const int aMapIndex, const int aCl, const double aTimeCenter){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps){
    cerr<<"EventMap::BuildClusterMap: Map #"<<aMapIndex<<" does not exist"<<endl;
    return false;
  }
  if(Ctree->GetEntry(aCl)<=0){
    cerr<<"EventMap::BuildClusterMap: the cluster index "<<aCl<<" could not be found."<<endl;
    return false;
  }

 
  if(ReadTriggerSegments::Verbose>2) cout<<"EventMap::BuildClusterMap: map #"<<aMapIndex<<", cluster index = "<<aCl<<endl;

  // reset map content
  ResetMapContent(aMapIndex);

  // map center
  double tcenter=Ctime;
  if(aTimeCenter) tcenter=aTimeCenter;

  // map boundaries
  double maptmin=tcenter+Map[aMapIndex]->GetXaxis()->GetBinLowEdge(1);
  double maptmax=tcenter+Map[aMapIndex]->GetXaxis()->GetBinUpEdge(Map[aMapIndex]->GetNbinsX());
  double mapfmin=Map[aMapIndex]->GetYaxis()->GetBinLowEdge(1);
  double mapfmax=Map[aMapIndex]->GetYaxis()->GetBinUpEdge(Map[aMapIndex]->GetNbinsY());
  double mapqmin=MapParam[aMapIndex]->GetQMin();
  double mapqmax=MapParam[aMapIndex]->GetQMax();
  
  // local tile parameters
  int bin_start_t, bin_start_f,bin_end_t, bin_end_f; 
  double snrmax=-1.0;

  // map content
  double *content;
  if(MapContentType) content=&Tamp;
  else content=&Tsnr;
  
  // read triggers starting at this tree
  for(Long64_t t=Cfirstentry; t<Cfirstentry+Csize; t++){
    Ttree->GetEntry(t);
    
    // time selection to fit in the map
    if(Ttend<maptmin) continue;// before the map
    if(Ttstart>=maptmax) break;// after the map
    
    // frequency selection to fit in the map
    if(Tfend<mapfmin) continue;   // below the map
    if(Tfstart>=mapfmax) continue;// above the map

    // Q selection
    if(Tq<mapqmin) continue;
    if(Tq>=mapqmax) continue;
      
    // bins to cover
    bin_start_t = Map[aMapIndex]->GetXaxis()->FindBin(Ttstart-tcenter);
    bin_start_f = Map[aMapIndex]->GetYaxis()->FindBin(Tfstart);
    bin_end_t   = Map[aMapIndex]->GetXaxis()->FindBin(Ttend-tcenter);
    bin_end_f   = Map[aMapIndex]->GetYaxis()->FindBin(Tfend);

    // loudest bin
    if(Tsnr>snrmax){// save loudest tile of the map
      MapLoudestBin[aMapIndex]=Map[aMapIndex]->GetBin((bin_start_t+bin_end_t)/2,(int)(sqrt((double)(bin_start_f*bin_end_f))));
      snrmax=Tsnr;
    }
    
    for(int bf=bin_start_f; bf<=bin_end_f; bf++){// freq-sweep the tile
      if(Tsnr>MapLoudestSNR_bf[aMapIndex][bf]){// get noise amplitude for this band
	MapLoudestSNR_bf[aMapIndex][bf]=Tsnr;
	MapNoiseAmp[aMapIndex][bf]=Tamp/Tsnr;
      }
      for(int bt=bin_start_t; bt<=bin_end_t; bt++){// time-sweep the tile
      	if(Tsnr>MapSNR[aMapIndex][bt][bf]){// keep only the loudest tile
	  Map[aMapIndex]->SetBinContent(bt,bf,*content);// update map
	  MapSNR[aMapIndex][bt][bf]=Tsnr;
	}
      }
    }

    // save for optimized match computation
    if(bin_start_t<MapBinXmin[aMapIndex]) MapBinXmin[aMapIndex]=bin_start_t;
    if(bin_start_f<MapBinYmin[aMapIndex]) MapBinYmin[aMapIndex]=bin_start_f;
    if(bin_end_t>MapBinXmax[aMapIndex])   MapBinXmax[aMapIndex]=bin_end_t;
    if(bin_end_f>MapBinYmax[aMapIndex])   MapBinYmax[aMapIndex]=bin_end_f;
  }

  // map title
  ostringstream tmpstream;
  tmpstream.flags ( ios::fixed );
  tmpstream<<GetStreamName()<<": cluster #"<<aCl<<setprecision(3)<<", GPS = "<<tcenter;
  Map[aMapIndex]->SetTitle(tmpstream.str().c_str());
  MapCentralTime[aMapIndex]=tcenter;
  tmpstream.str(""); tmpstream.clear();

  // get loudest tile coordinates
  int x,y,z;
  Map[aMapIndex]->GetBinXYZ(MapLoudestBin[aMapIndex],x,y,z);
  if(ReadTriggerSegments::Verbose>2) cout<<"EventMap::BuildClusterMap: Loudest tile: time="<<Map[aMapIndex]->GetXaxis()->GetBinCenter(x)+MapCentralTime[aMapIndex]<<", frequency = "<<Map[aMapIndex]->GetYaxis()->GetBinCenterLog(y)<<", SNR = "<<MapSNR[aMapIndex][x][y]<<endl;

  // update map loudest tile info
  if(aMapIndex==MapPrinted){
    tmpstream<<"Loudest tile: GPS="<<fixed<<setprecision(3)<<Map[aMapIndex]->GetXaxis()->GetBinCenter(x)+MapCentralTime[aMapIndex]<<", f="<<Map[aMapIndex]->GetYaxis()->GetBinCenterLog(y)<<" Hz, SNR="<<MapSNR[aMapIndex][x][y];
    UpdateText(tmpstream.str());
  }

  return true;

}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::PrintMap(const int aMapIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps){
    cerr<<"EventMap::PrintMap: Map #"<<aMapIndex<<" does not exist"<<endl;
    return false;
  }

  // draw map
  Draw(Map[aMapIndex],"COLZ");
  MapPrinted=aMapIndex;

  // get loudest tile coordinates
  int x,y,z;
  Map[aMapIndex]->GetBinXYZ(MapLoudestBin[aMapIndex],x,y,z);

  // loudest tile info
  ostringstream tmpstream;
  tmpstream<<"Loudest tile: GPS="<<fixed<<setprecision(3)<<Map[aMapIndex]->GetXaxis()->GetBinCenter(x)+MapCentralTime[aMapIndex]<<", f="<<Map[aMapIndex]->GetYaxis()->GetBinCenterLog(y)<<" Hz, SNR="<<MapSNR[aMapIndex][x][y];
  AddText(tmpstream.str(), 0.01,0.01,0.03);
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::PrintMapNext(const double aTimeFraction){
////////////////////////////////////////////////////////////////////////////////////
  if(MapPrinted<0){
    cerr<<"EventMap::PrintMapNext: there is no current map printed in map"<<endl;
    return false;
  }
  return BuildMap(MapPrinted, MapCentralTime[MapPrinted]+aTimeFraction*MapParam[MapPrinted]->GetTimeRange());
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::PrintMapLive(const double aDuration, const string aOutFile){
////////////////////////////////////////////////////////////////////////////////////
  if(MapPrinted<0){
    cerr<<"EventMap::PrintMapNext: there is no current map printed in map"<<endl;
    return false;
  }

  // print current map
  double start=MapCentralTime[MapPrinted];
  if(aOutFile.compare("none")) Print(aOutFile);

  double dur=fabs(aDuration);
  int dur_sign=(int)aDuration/dur;
  

  while(fabs(MapCentralTime[MapPrinted]-start)<dur){
    if(!PrintMapNext(dur_sign*0.1)){
      cerr<<"EventMap::PrintMapLive: for some reason the live map had to stop"<<endl;
      return false;
    }
    if(aOutFile.compare("none")) Print(aOutFile+"+20");
  }
 
  if(aOutFile.compare("none")) Print(aOutFile+"++");

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void EventMap::ResetMapContent(const int aMapIndex){
////////////////////////////////////////////////////////////////////////////////////
  Map[aMapIndex]->Reset();
  Map[aMapIndex]->SetTitle("Map");
  MapCentralTime[aMapIndex]=0.0;
  MapLoudestBin[aMapIndex]=0;
  for(int bf=0; bf<Map[aMapIndex]->GetNbinsY()+2; bf++){
    MapNoiseAmp[aMapIndex][bf]=0.0;
    MapLoudestSNR_bf[aMapIndex][bf]=0.0;
    for(int bt=0; bt<Map[aMapIndex]->GetNbinsX()+2; bt++)
      MapSNR[aMapIndex][bt][bf]=0.0;
  }
  MapBinXmin[aMapIndex]=Map[aMapIndex]->GetNbinsX()+1;
  MapBinXmax[aMapIndex]=0;
  MapBinYmin[aMapIndex]=Map[aMapIndex]->GetNbinsY()+1;
  MapBinYmax[aMapIndex]=0;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void EventMap::ResetMap(const int aMapIndex){
////////////////////////////////////////////////////////////////////////////////////

  // delete map pixels
  for(int bt=0; bt<Map[aMapIndex]->GetNbinsX()+2; bt++)
    delete MapSNR[aMapIndex][bt];
  delete MapSNR[aMapIndex];
  delete MapNoiseAmp[aMapIndex];
  delete MapLoudestSNR_bf[aMapIndex];

  // re-create map pixels
  delete Map[aMapIndex];
  Map[aMapIndex] = MapParam[aMapIndex]->GetTH2D("Time", "Frequency");

  // re-create map pixels
  MapSNR[aMapIndex] = new double* [Map[aMapIndex]->GetNbinsX()+2];
  for(int bt=0; bt<Map[aMapIndex]->GetNbinsX()+2; bt++) MapSNR[aMapIndex][bt] = new double [Map[aMapIndex]->GetNbinsY()+2];// include under/overflow
  MapNoiseAmp[aMapIndex] = new double [Map[aMapIndex]->GetNbinsY()+2];
  MapLoudestSNR_bf[aMapIndex] = new double [Map[aMapIndex]->GetNbinsY()+2];

  // set unique map name
  ostringstream tmpstream;
  tmpstream<<"map_"<<aMapIndex<<"_"<<ReadTriggerSegments::srandid;
  Map[aMapIndex]->SetName(tmpstream.str().c_str());
  tmpstream.str(""); tmpstream.clear();

  // set style
  Map[aMapIndex]->GetXaxis()->SetTitle("Time [s]");
  Map[aMapIndex]->GetYaxis()->SetTitle("Frequency [Hz]");
  if(MapContentType){
    Map[aMapIndex]->GetZaxis()->SetTitle("Amplitude");
    Map[aMapIndex]->GetZaxis()->UnZoom();
  }
  else{
    Map[aMapIndex]->GetZaxis()->SetTitle("SNR");
    Map[aMapIndex]->GetZaxis()->SetRangeUser(1,50);
  }    
  //MapPrinted=-1;

  if(ReadTriggerSegments::Verbose>1){
    cout<<"EventMap::ResetMap: Map #"<<aMapIndex<<" was built with the following selection:"<<endl;
    MapParam[aMapIndex]->PrintSelection();
  }

  return ResetMapContent(aMapIndex);
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::SetMapTimeRange(const int aMapIndex, const double aTimeRange){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps){
    cerr<<"EventMap::SetMapTimeRange: Map #"<<aMapIndex<<" does not exist"<<endl;
    return false;
  }
  MapParam[aMapIndex]->SetTimeRange(-fabs(aTimeRange)/2.0,fabs(aTimeRange)/2.0);
  ResetMap(aMapIndex);
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::SetMapFrequencyRange(const int aMapIndex, const double aFreqMin, const double aFreqMax){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps){
    cerr<<"EventMap::SetMapFrequencyRange: Map #"<<aMapIndex<<" does not exist"<<endl;
    return false;
  }
  if(fabs(aFreqMin)>=fabs(aFreqMax)){
    cerr<<"EventMap::SetMapFrequencyRange: the frequency range "<<aFreqMin<<"-"<<aFreqMax<<" does not make sense"<<endl;
    return false;
  }
  MapParam[aMapIndex]->SetFrequencyRange(fabs(aFreqMin), fabs(aFreqMax));
  ResetMap(aMapIndex);
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::SetMapQRange(const int aMapIndex, const double aQMin, const double aQMax){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps){
    cerr<<"EventMap::SetMapQRange: Map #"<<aMapIndex<<" does not exist"<<endl;
    return false;
  }
  if(fabs(aQMin)>=fabs(aQMax)){
    cerr<<"EventMap::SetMapQRange: the Q range "<<aQMin<<"-"<<aQMax<<" does not make sense"<<endl;
    return false;
  }
  MapParam[aMapIndex]->SetQRange(fabs(aQMin), fabs(aQMax));
  ResetMap(aMapIndex);
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::SetMapResolution(const int aMapIndex, const int aTresolution, const int aFresolution){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps){
    cerr<<"EventMap::SetMapResolution: Map #"<<aMapIndex<<" does not exist"<<endl;
    return false;
  }
  if((int)fabs(aTresolution)<1||(int)fabs(aTresolution)<1){
    cerr<<"EventMap::SetMapResolution: the new map resolution "<<fabs(aTresolution)<<"x"<<fabs(aTresolution)<<" does not make sense"<<endl;
    return false;
  }
  MapParam[aMapIndex]->SetTimeResolution((int)fabs(aTresolution));
  MapParam[aMapIndex]->SetFrequencyResolution((int)fabs(aTresolution));
  ResetMap(aMapIndex);
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
double EventMap::GetMapLoudestSNR(const int aMapIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps) return -1.0;
  int x,y,z;
  Map[aMapIndex]->GetBinXYZ(MapLoudestBin[aMapIndex],x,y,z);
  return MapSNR[aMapIndex][x][y];
}

////////////////////////////////////////////////////////////////////////////////////
double EventMap::GetMapLoudestFrequency(const int aMapIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps) return 0.0;
  int x,y,z;
  Map[aMapIndex]->GetBinXYZ(MapLoudestBin[aMapIndex],x,y,z);
  return Map[aMapIndex]->GetYaxis()->GetBinCenterLog(y);
}

////////////////////////////////////////////////////////////////////////////////////
double EventMap::GetMapLoudestTime(const int aMapIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps) return 0.0;
  int x,y,z;
  Map[aMapIndex]->GetBinXYZ(MapLoudestBin[aMapIndex],x,y,z);
  return Map[aMapIndex]->GetXaxis()->GetBinCenter(x);
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::NormalizeMap(const int aMapIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps) return false;
  double max = Map[aMapIndex]->GetBinContent(Map[aMapIndex]->GetMaximumBin());
  if(max<=0.0) return true;
  Map[aMapIndex]->Scale(1.0/max);
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool EventMap::NormalizeMapLoudest(const int aMapIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps) return false;
  double max = Map[aMapIndex]->GetBinContent(MapLoudestBin[aMapIndex]);
  if(max<=0.0) return true;
  Map[aMapIndex]->Scale(1.0/max);
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
double EventMap::GetMapMatch(double &aTimeShift, double &aMatchSumw2, const int aMapIndex1, const int aMapIndex2, const double aTimeJitter){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex1<0||aMapIndex1>=Nmaps){
    cerr<<"EventMap::GetMapMatch: Map #"<<aMapIndex1<<" does not exist"<<endl;
    return -1.0;
  }
  if(aMapIndex2<0||aMapIndex2>=Nmaps){
    cerr<<"EventMap::GetMapMatch: Map #"<<aMapIndex2<<" does not exist"<<endl;
    return -1.0;
  }
  if(Map[aMapIndex1]->GetNbinsX()!=Map[aMapIndex2]->GetNbinsX()){
    cerr<<"EventMap::GetMapMatch: Map #"<<aMapIndex1<<" and #"<<aMapIndex2<<" have a different time resolution"<<endl;
    return -1.0;
  }
  if(Map[aMapIndex1]->GetNbinsY()!=Map[aMapIndex2]->GetNbinsY()){
    cerr<<"EventMap::GetMapMatch: Map #"<<aMapIndex1<<" and #"<<aMapIndex2<<" have a different frequency resolution"<<endl;
    return -1.0;
  }

  return GetMatch(aTimeShift,aMatchSumw2,this,aMapIndex1,this,aMapIndex2,aTimeJitter);
  
}

////////////////////////////////////////////////////////////////////////////////////
double EventMap::GetMapMatch(double &aTimeShift, double &aMatchSumw2, const int aMapIndex, EventMap *aExtMap, const int aExtMapIndex, const double aTimeJitter){
////////////////////////////////////////////////////////////////////////////////////
  if(aMapIndex<0||aMapIndex>=Nmaps){
    cerr<<"EventMap::GetMapMatch: Map #"<<aMapIndex<<" does not exist"<<endl;
    return -1.0;
  }
  if(aExtMapIndex<0||aExtMapIndex>=aExtMap->Nmaps){
    cerr<<"EventMap::GetMapMatch: Map #"<<aExtMapIndex<<" does not exist for the external EventMap object"<<endl;
    return -1.0;
  }
  if(Map[aMapIndex]->GetNbinsX()!=aExtMap->Map[aExtMapIndex]->GetNbinsX()){
    cerr<<"EventMap::GetMapMatch: Map #"<<aMapIndex<<" and external EventMap have a different time resolution"<<endl;
    return -1.0;
  }
  if(Map[aMapIndex]->GetNbinsY()!=aExtMap->Map[aExtMapIndex]->GetNbinsY()){
    cerr<<"EventMap::GetMapMatch: Map #"<<aMapIndex<<" and external EventMap have a different frequency resolution"<<endl;
    return -1.0;
  }

  return GetMatch(aTimeShift,aMatchSumw2,this,aMapIndex,aExtMap,aExtMapIndex,aTimeJitter);
  
}

////////////////////////////////////////////////////////////////////////////////////
double EventMap::GetMatch(double &aTimeShift, double &aSumw2,
			  EventMap *aEMap1, const int aMapIndex1,
			  EventMap *aEMap2, const int aMapIndex2,
			  const double aTimeJitter){
////////////////////////////////////////////////////////////////////////////////////

  double weight       = 0.0; // weight for each pixel
  double weight_sum   = 0.0; // weight sum
  double weightsq_sum = 0.0; // sum of weight squared
  double asymetry     = 0.0; // asymetry value for each pixel
  double match        = 0.0; // match 
  double bestmatch    = 0.0; // best match / jitter 
  double content1     = 0.0; // running cell content for map 1
  double content2     = 0.0; // running cell content for map 2
  double thr1, thr_snr1;     // threshold 1
  double thr2, thr_snr2;     // threshold 2
  int tst, ten;

  //init
  aTimeShift=0.0;
  aSumw2=0.0;

  // bin jitter
  int bin_jitter_range = (int)ceil(fabs(aTimeJitter)/aEMap1->Map[aMapIndex1]->GetXaxis()->GetBinWidth(1));
 
  // loudest tile value for normalization
  double max1=aEMap1->Map[aMapIndex1]->GetBinContent(aEMap1->MapLoudestBin[aMapIndex1]);
  double max2=aEMap2->Map[aMapIndex2]->GetBinContent(aEMap2->MapLoudestBin[aMapIndex2]);
  if(max1==0.0) max1=1.0;// not to divide by 0 (empty map)
  if(max2==0.0) max2=1.0;// not to divide by 0 (empty map)

  // frequency band to cover
  int fst=TMath::Min(aEMap1->MapBinYmin[aMapIndex1],aEMap2->MapBinYmin[aMapIndex2]);
  int fen=TMath::Max(aEMap1->MapBinYmax[aMapIndex1],aEMap2->MapBinYmax[aMapIndex2]);
  fst=TMath::Max(fst,1);// exclude underflow
  fen=TMath::Min(fen,aEMap1->Map[aMapIndex1]->GetNbinsY());// exclude overflow

  // time-jitter
  for(int j=-bin_jitter_range; j<=bin_jitter_range; j++){
 
    // init
    match        = 0.0;
    weight_sum   = 0.0;
    weightsq_sum = 0.0;
     
    // normalized SNR thresholds
    thr_snr1=aEMap1->GetSNRMin()/max1;
    thr_snr2=aEMap2->GetSNRMin()/max2;
    thr1=thr_snr1;
    thr2=thr_snr1;
    
    // time bins to cover (before time offset)
    tst=TMath::Min(aEMap1->MapBinXmin[aMapIndex1],aEMap2->MapBinXmin[aMapIndex2]);
    ten=TMath::Max(aEMap1->MapBinXmax[aMapIndex1],aEMap2->MapBinXmax[aMapIndex2]);

    // time bins to cover (after time offset)
    tst=TMath::Max(tst-j,1);
    ten=TMath::Min(ten-j,aEMap1->Map[aMapIndex1]->GetNbinsX());

    // loop over frequency bins
    for(int bf=fst; bf<=fen; bf++){

      // update thresholds if amplitude content
      if(aEMap1->MapContentType) thr1 = thr_snr1*aEMap1->MapNoiseAmp[aMapIndex1][bf];
      if(aEMap2->MapContentType) thr2 = thr_snr2*aEMap2->MapNoiseAmp[aMapIndex2][bf];

      // loop over time bins
      for(int bt=tst; bt<=ten; bt++){

	// get content of Map1
	content1=aEMap1->Map[aMapIndex1]->GetBinContent(bt,bf)/max1;

	// get content of Map2 and time-jitter it
	content2=aEMap2->Map[aMapIndex2]->GetBinContent(bt+j,bf)/max2;

	// below thresholds --> skip
	if(content1<=thr2&&content2<=thr1) continue;
		
	// asymetry
	asymetry = fabs(content1-content2)/(content1+content2);
		
	// weight
	weight=aEMap1->MapSNR[aMapIndex1][bt][bf]+aEMap2->MapSNR[aMapIndex2][bt+j][bf];
			
	// sum of weights
	weight_sum += weight;
      	weightsq_sum+=weight*weight;
	
	// cumulative match
	match+=(1.0-asymetry) * weight;
      }
    }
    
    // average match
    if(weight_sum) match /= weight_sum;

    // only keep best match
    if(match>bestmatch){
      bestmatch=match;
      aSumw2=weightsq_sum;
      aTimeShift=(double)j*aEMap1->Map[aMapIndex1]->GetXaxis()->GetBinWidth(1);
    }
  }

  return bestmatch;
}


