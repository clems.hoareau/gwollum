//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __EventMap__
#define __EventMap__

#include "GwollumPlot.h"
#include "ReadTriggers.h"
#include "TriggerSelect.h"
#include "TH2.h"

using namespace std;

/**
 * Map triggers.
 * This class was designed to plot time-frequency maps of triggers. When initialized, the EventMap object creates map containers centered on 0. These containers can be filled with trigger SNR or amplitude. Maps can be used interactively or saved on disk. This class offers functions to interact with the maps, to compare maps, to animate the maps etc...
 * \author Florent Robinet
 */
class EventMap: public GwollumPlot, public ReadTriggers {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the EventMap class.
   * aNmaps map containers are initialized. The map parameters are set to default using input triggers (see TriggerSelect). The time range is set to +/- 2 seconds with a 201-pixels resolution and the frequency resolution is set to 200 pixels. By default, maps are filled with tile SNR.
   * @param aNmaps number of map containers
   * @param aPattern input file pattern
   * @param aDirectory trigger ROOT directory
   * @param aVerbose verbosity level
   */
  EventMap(const int aNmaps, const string aPattern, const string aDirectory="", const int aVerbose=0);

  /**
   * Destructor of the EventMap class.
   */
  virtual ~EventMap(void);
  /**
     @}
  */
 
  
  /**
   * Builds the map centered on given GPS time.
   * Use PrintMap() to update the interactive pad.
   * @param aMapIndex map index
   * @param aTime central GPS time
   */
  bool BuildMap(const int aMapIndex, const double aTime);

  /**
   * Builds the map of a given cluster.
   * Only tiles of a given cluster are represented on the map. By default, the map is centered on the cluster peak time.
   * Use PrintMap() to update the interactive pad.
   * @param aMapIndex map index
   * @param aCl cluster index
   * @param aTimeCenter if this value is non 0.0, the map is centered on this time value.
   */
  bool BuildClusterMap(const int aMapIndex, const int aCl, const double aTimeCenter=0.0);

  /**
   * Prints the map with a given index.
   * @param aMapIndex map index
   */
  bool PrintMap(const int aMapIndex);

  /**
   * Prints the map incrementing the current central time.
   * Once a map is plotted, this function can be used to move back and forth in time. The time fraction indicates the fraction of the map time range used for the shift. A positive value will move forward in time while a negative value will move backward.
   * @param aTimeFraction time range fraction used to move to the next map
   */
  bool PrintMapNext(const double aTimeFraction=0.25);
  
  /**
   * Prints a live movie of maps incrementing the current central time.
   * Once a map is plotted, this function can be called to move forward in time. The live map stops after 'aDuration' seconds have been covered. This movie can be saved as an animated gif if an output file is given.
   * Use a negative duration to move backward in time.
   * @param aDuration duration in seconds after what the live map stops
   * @param aOutFile path to output file. 
   */
  bool PrintMapLive(const double aDuration, const string aOutFile="none");

  /**
   * Sets map content to tile amplitude.
   */
  inline void SetMapAmplitudeContent(void){ MapContentType=true; ResetAll(); };

  /**
   * Sets map content to tile SNR.
   */
  inline void SetMapSNRContent(void){ MapContentType=false; ResetAll(); };

  /**
   * Sets a new map time range.
   * @param aMapIndex map index
   * @param aTimeRange new time range value [s]
   */
  bool SetMapTimeRange(const int aMapIndex, const double aTimeRange);
 
  /**
   * Sets a new map frequency range.
   * @param aMapIndex map index
   * @param aFreqMin new minimal frequency value [Hz]
   * @param aFreqMax new maximal frequency value [Hz]
   */
  bool SetMapFrequencyRange(const int aMapIndex, const double aFreqMin, const double aFreqMax);
 
  /**
   * Sets a new map Q range.
   * @param aMapIndex map index
   * @param aQMin new minimal Q value
   * @param aQMax new maximal Q value
   */
  bool SetMapQRange(const int aMapIndex, const double aQMin, const double aQMax);
 
  /**
   * Sets a new map resolution.
   * The map resolution is defined by the number of time/frequency pixels in the map.
   * @param aMapIndex map index
   * @param aTresolution time resolution (number of pixels)
   * @param aFresolution frequency resolution (number of pixels)
   */
  bool SetMapResolution(const int aMapIndex, const int aTresolution, const int aFresolution);
 
  /**
   * Returns the SNR value of the loudest (SNR) tile of a given map.
   * Returns -1.0 if failure.
   * @param aMapIndex map index
   */
  double GetMapLoudestSNR(const int aMapIndex);

  /**
   * Returns the time value of the loudest (SNR) tile of a given map.
   * Returns 0.0 if failure.
   * @param aMapIndex map index
   */
  double GetMapLoudestTime(const int aMapIndex);

  /**
   * Returns the frequency value of the (SNR) loudest tile of a given map.
   * Returns 0.0 if failure.
   * @param aMapIndex map index
   */
  double GetMapLoudestFrequency(const int aMapIndex);

  /**
   * Normalizes the map pixels by the highest pixel value.
   * @param aMapIndex map index
   */
  bool NormalizeMap(const int aMapIndex);

  /**
   * Normalizes the map pixels by the value of the loudest (SNR) tile.
   * @param aMapIndex map index
   */
  bool NormalizeMapLoudest(const int aMapIndex);

  /**
   * Measures and returns the morphology match between 2 maps.
   * FIXME: explain the match algorithm.
   * Returns -1.0 if this function fails.
   * @param aTimeShift time-shift used to get the best match (applied to pixels of aMapIndex2)
   * @param aMatchSumw2 sum of weights squared
   * @param aMapIndex1 first map index
   * @param aMapIndex2 second map index
   * @param aTimeJitter time jitter +/- [s]
   */
  double GetMapMatch(double &aTimeShift, double &aMatchSumw2, const int aMapIndex1, const int aMapIndex2, const double aTimeJitter=0.0);

  /**
   * Measures and returns the morphology match with an external map.
   * FIXME: explain the match algorithm.
   * Returns -1.0 if this function fails.
   * @param aTimeShift time-shift used to get the best match (applied to pixels of aEmap)
   * @param aMatchSumw2 sum of weights squared
   * @param aMapIndex map index
   * @param aExtMap external EventMap object.
   * @param aExtMapIndex map index for the external EventMap object.
   * @param aTimeJitter time jitter +/- [s]
   */
  double GetMapMatch(double &aTimeShift, double &aMatchSumw2, const int aMapIndex, EventMap *aExtMap, const int aExtMapIndex, const double aTimeJitter=0.0);

  /**
   * Returns the map time resolution.
   * @param aMapIndex map index
   */
  inline int GetMapTimeResolution(const int aMapIndex){
    if(aMapIndex>=0&&aMapIndex<Nmaps) return Map[aMapIndex]->GetNbinsX();
    return -1;
  }

  /**
   * Returns the map frequency resolution.
   * @param aMapIndex map index
   */
  inline int GetMapFrequencyResolution(const int aMapIndex){
    if(aMapIndex>=0&&aMapIndex<Nmaps) return Map[aMapIndex]->GetNbinsY();
    return -1;
  }
    
  /**
   * Returns the map time range.
   * @param aMapIndex map index
   */
  inline int GetMapTimeRange(const int aMapIndex){
    if(aMapIndex>=0&&aMapIndex<Nmaps) return MapParam[aMapIndex]->GetTimeRange();
    return -1;
  }

  /**
   * Returns the map minimum frequency.
   * @param aMapIndex map index
   */
  inline int GetMapFrequencyMin(const int aMapIndex){
    if(aMapIndex>=0&&aMapIndex<Nmaps) return MapParam[aMapIndex]->GetFrequencyMin();
    return -1;
  }

  /**
   * Returns the map maximum frequency.
   * @param aMapIndex map index
   */
  inline int GetMapFrequencyMax(const int aMapIndex){
    if(aMapIndex>=0&&aMapIndex<Nmaps) return MapParam[aMapIndex]->GetFrequencyMax();
    return -1;
  }

 private:

  void ResetMap(const int aMapIndex);// delete map and rebuild it with new parameters
  void ResetMapContent(const int aMapIndex);// reset map content
  inline void ResetAll(void){
    for(int m=0; m<Nmaps; m++) ResetMap(m);
  };// reset all maps

  int Nmaps;                ///< number of maps
  TH2D **Map;               ///< maps
  double ***MapSNR;         ///< map pixel SNR
  double **MapNoiseAmp;     ///< map noise amplitude / f-band
  TriggerSelect **MapParam; ///< map parameters
  double *MapCentralTime;   ///< central time of the map
  int *MapLoudestBin;       ///< loudest pixel
  double **MapLoudestSNR_bf;///< loudest pixel / frequency band
  int MapPrinted;           ///< index of map currently printed (-1 if none)
  bool MapContentType;      ///< content type : 0=snr, 1=amp
  int *MapBinXmin,          ///< first time bin with something
    *MapBinXmax,            ///< last time bin with something
    *MapBinYmin,            ///< first frequency bin with something
    *MapBinYmax;            ///< last frequency bin with something

  double GetMatch(double &aTimeShift,
		  double &aSumw2,
		  EventMap *aEMap1, const int aMapIndex1,
		  EventMap *aEMap2, const int aMapIndex2,
		  const double aTimeJitter);

  ClassDef(EventMap,0)  
};

#endif


