/**
 * @file 
 * @brief Program to convert a txt file with triggers to a ROOT file.
 * @snippet this gwl-txt-to-triggers-usage
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "MakeTriggers.h"

using namespace std;

/**
 * @brief Usage funcion.
 */
int usage(){
    //! [gwl-txt-to-triggers-usage]
    cerr<<"This program saves a list of triggers from a txt file to a ROOT file."<<endl;
    cerr<<endl;
    cerr<<"usage:"<<endl;
    cerr<<"gwl-txt-to-triggers [input text file pattern] [stream name] [column headers] ([segment file])"<<endl; 
    cerr<<endl;
    cerr<<"  [input text file pattern]: List of text files (pattern) listing the triggers"<<endl; 
    cerr<<"  [stream name]: Stream name"<<endl; 
    cerr<<"  [column headers]: Assign column numbers."<<endl;
    cerr<<"                    Up to 10 trigger parameters are supported, in this order:"<<endl; 
    cerr<<"                    time - SNR - frequency - time start - time end - frequency start"<<endl;
    cerr<<"                    - frequency end - Q - amplitude - phase"<<endl; 
    cerr<<"                    Use a \"-1\" to indicate that a parameter is missing in the text file."<<endl; 
    cerr<<"                    For example:"<<endl; 
    cerr<<"                    \"1;5;2;-1;-1;-1;-1;9;-1;-1\""<<endl; 
    cerr<<"                    column 1 = time"<<endl; 
    cerr<<"                    column 2 = frequency"<<endl; 
    cerr<<"                    column 3-4 = ignored"<<endl; 
    cerr<<"                    column 5 = SNR"<<endl; 
    cerr<<"                    column 6-8 = ignored"<<endl; 
    cerr<<"                    column 9 = Q"<<endl; 
    cerr<<"  [segment file]: Segment file associated to the triggers (optional)"<<endl; 
    cerr<<""<<endl;
    cerr<<"The output root file is dumped in the current directory"<<endl;
    cerr<<endl;
    //! [gwl-txt-to-triggers-usage]
    return 1;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    GwlPrintVersion();
    return 0;
  }

  // check options
  if(argc<4) return usage();

  // get txt file pattern
  string intxtpattern=(string)argv[1];
  
  //  stream name
  string streamname=(string)argv[2];

  //  segment file name
  string segfilename="";
  if(argc>4) segfilename=(string)argv[4];

  //  column headers
  vector <string> shead=SplitString((string)argv[3], ';');
  if(shead.size()!=10){
    cerr<<argv[0]<<": cannot read column header"<<endl<<endl; 
    return 2;
  }

  // convert column numbers into integers
  int colnumber[10];
  string sformat="";
  for(unsigned int f=0; f<10; f++){
    istringstream iss(shead[f]); iss>>colnumber[f];
    colnumber[f]--;
    if(colnumber[f]>=0) sformat+="d;";
  }

  // check time and SNR
  if(colnumber[0]<0||colnumber[1]<0){
    cerr<<argv[0]<<": time and SNR columns must be given"<<endl<<endl; 
    return 3;
  }
 
  // Trigger object
  MakeTriggers *triggers = new MakeTriggers(streamname, 1);
 
  string ls;
  ls = "ls "+(string)intxtpattern;
  
  cout<<"Opening text files in "<<intxtpattern<<"..."<<endl;
    
  FILE *in;
  in = popen(ls.c_str(), "r");

  ReadAscii *ra;
  double var[10];

  // loop over txt files  
  char filename[1000];
  while(fscanf(in,"%s",filename) != EOF){
    cout<<"converting "<<filename<<" ... "<<flush;
    ra = new ReadAscii((string)filename, sformat);
   
    // check file
    if(!ra->GetNRow()){
      cerr<<"no triggers -> skip file"<<endl;
      delete ra;
      continue;
    }
    
    // loop over input files
    for(int i=0; i<ra->GetNRow(); i++){

      // read elements
      for(unsigned int f=0; f<10; f++){

        // missing --> default value
	if((colnumber[f]<0) || (!ra->GetElement(var[f],i,colnumber[f])))
          var[f]=0.0;
      }

      // missing time
      if(var[3]==0.0) var[3]=var[0]-0.001;
      if(var[4]==0.0) var[4]=var[0]+0.001;

      // missing frequency
      if(var[2]==0.0){
        if(var[5]>0&&var[6]>0) var[2]=var[5]/2.0+var[6]/2.0;
        else var[2]=100.00;
      }
      if(var[5]==0.0) var[5]=var[2]-0.01;
      if(var[6]==0.0) var[6]=var[2]+0.01;

      // add trigger
      if(!triggers->AddTrigger(var[0],var[2],var[1],var[7],var[3],var[4],var[5],var[6],var[8],var[9]))
        continue;
    }

    cout<<"OK"<<endl;
    delete ra;
  }
  
  pclose(in);

  // set segments if any
  Segments *S;
  if(segfilename.compare("")){
    S = new Segments(segfilename);
    if(S->GetN()>0) triggers->Append(S);
    delete S;
  }

  // sort triggers
  if(!triggers->SortTriggers()) return 4;

  // save trigger file
  if(!triggers->Write(".", "root", "").compare("")) return 5;
  
  delete triggers;

  return 0;
}
