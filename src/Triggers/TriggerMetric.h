//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __TriggerMetric__
#define __TriggerMetric__

#include <TObject.h>
#include "EventMap.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

using namespace std;

/**
 * Measure distance with triggers.
 * \author Florent Robinet
 */
class TriggerMetric: private EventMap {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the TriggerMetric class.
   * The input trigger set is loaded and clustered using the Triggers::Clusterize() method. The clustering parameters can be set (see Triggers::SetClusterizeDt() and Triggers::SetClusterizeSNRthr()).
   * @param aPattern trigger input file pattern
   * @param aDirectory trigger ROOT directory
   * @param aVerbose verbosity level
   * @param aDt new delta_t value in seconds 
   * @param aSNRthreshold new SNR threshold for clusters
   */
  TriggerMetric(const string aPattern, const string aDirectory="", const int aVerbose=0,
		const double aDt=0.1, const double aSNRthreshold=5.0);

  /**
   * Destructor of the TriggerMetric class.
   */
  virtual ~TriggerMetric(void);
  /**
     @}
  */

  /**
   * Compute the trigger metric.
   * In a first step, overlapping clusters are identified. Then, for each tile in the cluster, the SNR-squared weighted distance is computed.
   * @param[in] aFunc Function in the time-frequency plane: \f$t=t(f)\f$. It can be either a TGraph or a TF1 object.
   * @param[in] aTimeStart Starting time [s].
   * @param[in] aTimeEnd Ending time [s].
   * @param[in] aMetric If given, the distribution is returned.
   */
  void ComputeMetric(TObject *aFunc, const double aTimeStart, const double aTimeEnd, TH1D* aMetric=NULL);

  /**
   * Prints the trigger metric in a plot.
   * See ComputeMetric() for the algorithm definition. This function prints the spectrogram and the function on top of it.
   * @param[in] aFunc Function in the time-frequency plane: \f$t=t(f)\f$. It can be either a TGraph or a TF1 object.
   * @param[in] aTimeStart Starting time [s].
   * @param[in] aTimeEnd Ending time [s].
   * @param[in] aOutFile file name where to save the plot. Many formats are supported (gif, pdf, svg, png, eps...)
   */
  void PrintMetric(TObject *aFunc, const double aTimeStart, const double aTimeEnd, const string aOutFile);

  /**
   * Returns the mean distance.
   */
  inline double GetDistanceMean(void){ return distance_mean; };

  /**
   * Returns the distance variance.
   */
  inline double GetDistanceVariance(void){ return distance_var; };

  /**
   * Returns the number of overlapping clusters.
   */
  int GetNOverlappingClusters(void){ return (int)cl_list.size(); };

  /**
   * Returns the number of cluster tiles.
   */
  int GetNTiles(void){ return distance_n; };

  /**
   * Draws and prints an object.
   * @param aObj pointer to ROOT object to be drawn
   * @param aFileName file name to save the current canvas
   * @param aOptions drawing options (ROOT options)
   */
  void Print(TObject *aObj, const string aFileName, const string aOptions=""){
    GwollumPlot::Draw(aObj,aOptions);
    GwollumPlot::Print(aFileName);
  };

 private:

  /**
   * Get list of overlapping clusters.
   * @param aFunc \f$\tau(f)\f$
   * @param aTimeStart Starting time.
   * @param aTimeEnd Stop time.
   */
  void GetOverlappingClusters(TObject *aFunc, const double aTimeStart, const double aTimeEnd);
  double dt;                 ///< clusterize dt
  vector <int> cl_list;      ///< list of overlapping clusters
  double distance_mean;      ///< mean distance
  double distance_var;       ///< distance variance
  int distance_n;            ///< number of tiles

  
  ClassDef(TriggerMetric,0)  
};

#endif


