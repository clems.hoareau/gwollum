/**
 * @file 
 * @brief See ReadTriggers.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ReadTriggers.h"

ClassImp(ReadTriggers)

////////////////////////////////////////////////////////////////////////////////////
ReadTriggers::ReadTriggers(const string aPattern, const string aDirectory,
                           const unsigned int aVerbose):
ReadTriggerMetaData(aPattern, aDirectory, aVerbose), Triggers(aVerbose) { 
////////////////////////////////////////////////////////////////////////////////////
  Triggers::ReadTriggerFiles(aPattern, aDirectory);
}

////////////////////////////////////////////////////////////////////////////////////
ReadTriggers::~ReadTriggers(void){
////////////////////////////////////////////////////////////////////////////////////
  if(ReadTriggerMetaData::Verbose>1) cout<<"ReadTriggers::~ReadTriggers"<<endl;
}
