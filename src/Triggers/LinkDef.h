/**
 * @file 
 * @brief ROOT link definition.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Triggers;
#pragma link C++ class MakeTriggers;
#pragma link C++ class ReadTriggerMetaData;
#pragma link C++ class ReadTriggers;
#pragma link C++ class TriggerBuffer;
#pragma link C++ class TriggerSelect;
#pragma link C++ class TriggerPlot;
//#pragma link C++ class EventMap;
//#pragma link C++ class TriggerMetric;

#endif

