/**
 * @file 
 * @brief Manage injection TTrees.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjTre__
#define __InjTre__


#include "InjTyp.h"
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>

using namespace std;

/**
 * @brief Manage injection structures.
 * @author Florent Robinet
 */
class InjTre {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjTre class.
   * @details The injection type must be provided: see ::injtype.
   * For user-defined injections, waveforms must be provided with SetWave().
   *
   * @param[in] aInjType Injection type: see InjTyp.h.
   * @param[in] aVerbose Verbosity level.
   */
  InjTre(const injtype aInjType, const unsigned int aVerbose=0);
  
  /**
   * @brief Destructor of the InjTre class.
   */
  virtual ~InjTre();
  /**
     @}
  */

  /**
   * @brief Sets the user waveform for "Wave" injections.
   * @details The source \f$h_{+}\f$ (and optionally \f$h_{\times}\f$) timeseries must be provided.
   * The TGraphs are the wave calibrated polarized strains as a function of time in [s].
   * By convention, t = 0 s is taken to be the time of the injection at the center of the Earth. They are saved locally.
   * @note If the \f$h_{\times}\f$ is provided, it must be same the number of points as \f$h_{+}\f$. Moreover, the two timeseries must coincide in time.
   *
   * In addition to the waveform, the injection must be given:
   * - an amplitude \f$A\f$ which is used to scale the waveform amplitude: \f$Ah_{+}\f$ and \f$Ah_{\times}\f$.
   * - a sky position: the declination and the right ascension.
   * - a polarization.
   *
   * @returns true if success.
   * @param[in] aHplus Pointer to the \f$h_{+}\f$ time series.
   * @param[in] aHcross Pointer to the \f$h_{\times}\f$ time series.
   */
  bool SetWave(TGraph *aHplus, TGraph *aHcross=NULL);
    
  /**
   * @brief Adds an injection.
   * @returns The current number of injections in the TTree.
   * @param[in] aRa Injection right ascension [rad].
   * @param[in] aDec Injection declination [rad].
   * @param[in] aTime Injection GPS time [s].
   * @param[in] aAmplitude Injection amplitude.
   * @param[in] aEccentricity Injection eccentricity.
   * @param[in] aPolarization Injection polarization angle [rad].
   * @param[in] aSigma Injection sigma [s].
   * @param[in] aFrequency Injection frequency [Hz].
   */
  unsigned int Add(const double aRa, 
                   const double aDec, 
                   const double aTime, 
                   const double aAmplitude, 
                   const double aEccentricity = 0.0, 
                   const double aPolarization = 0.0, 
                   const double aSigma = 0.0, 
                   const double aFrequency = 0.0);

  /**
   * @brief Returns the current number of injections.
   */
  inline unsigned int GetN(void){ return (unsigned int)InjTree->GetEntries(); };

  /**
   * @brief Resets the injection TTree.
   * @note The waveform tree is also reset.
   */
  inline void Reset(void){ InjTree->Reset(); WaveTree->Reset(); };

  /**
   * @brief Writes injections to disk.
   * @note When writing multiple injection files, think about resetting the TTree structure with Reset(). 
   * @param[in] aRootFileName Output file name.
   */  
  bool Write(const string aRootFileName="./myinjections.root");
  

 protected:
  
  unsigned int Verbose; ///< Verbosity level.
  int randid;           ///< Random integer to identify the class object.
  string srandid;       ///< Random integer (string) to identify the class object.

 private:  

  TTree *InjTree;       ///< Injection tree.
  double inj_ra,        ///< Injection right ascension [rad].
    inj_dec,            ///< Injection declination [rad].
    inj_psi,            ///< Injection polarization angle [rad].
    inj_time,           ///< Injection time [s].
    inj_amp,            ///< Injection amplitude.
    inj_f0,             ///< Injection frequency [Hz].
    inj_sigma,          ///< Injection sigma [s].
    inj_ecc;            ///< Injection eccentricity.

  TTree *WaveTree;      ///< Waveform tree.
  UInt_t wave_type;     ///< Waveform type: see InjTyp.h.
  TGraph *wave_hplus;   ///< Waveform \f$h_{+}(t)\f$.
  TGraph *wave_hcross;  ///< Waveform \f$h_{\times}(t)\f$.

  ClassDef(InjTre,0)
};

#endif


