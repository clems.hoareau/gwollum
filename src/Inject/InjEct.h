/**
 * @file 
 * @brief Inject simulated signals in a data stream.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjEct__
#define __InjEct__

#include "Streams.h"
#include "InjRea.h"
#include "GwollumPlot.h"

using namespace std;

/**
 * @brief Inject simulated signals in a data stream.
 * @details This class injects signal waveforms in a data vector using injection files produced with InjGen.
 *
 * The injection waveform is generated at the center of the Earth using the parameters from the injection files. Both polarizations \f$h_{+}\f$ and \f$h_{\times}\f$ are calculated. The waveform timing is adjusted to a given detector (Streams): the waveforms are calculated with the detector local time, accounting for the light travel time and the sky position of the source. Then we compute the detector strain:
 *\f[
 h_{det}(t) = T(t)\times \left(F_{+}(t)\times h_{+}(t) + F_{\times}(t)\times h_{\times}(t)\right).
 \f]
 * The detector antenna factors \f$F_{+}\f$ and \f$F_{\times}\f$ are calculated for the source sky position and polarization and at any given time. Moreover, a Tukey window \f$T(t)\f$ is applied to have nice transitions to 0.
 *
 * @sa ConstructSineGaussWaveform() and ConstructUserWaveform(0
 *
 * @author Florent Robinet
 */
class InjEct: public InjRea, public GwollumPlot {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjEct class.
   * @details Injections listed in the ROOT files designated by a file pattern are loaded with InjRea.
   *
   * @param[in] aStream Input data stream in which to inject. Only the pointer is saved by the class.
   * @param[in] aPattern Injection file pattern.
   * @param[in] aPlotStyle GwollumPlot style.
   * @param[in] aVerbose Verbosity level.
   */
  InjEct(Streams* aStream, const string aPattern,
         const string aPlotStyle="STANDARD", const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the InjEct class.
   */
  virtual ~InjEct();
  /**
     @}
  */

  /**
   * @brief Injects in a data vector.
   * @details All the signals overlapping the input data vector are injected.
   * @note The native sampling frequency of the Streams object is considered to be the data sampling frequency.
   * @param[in] aDataSize Input vector size.
   * @param[in,out] aData Pointer to input data vector (time-domain).
   * @param[in] aTimeStart GPS time of first data sample.
   * @todo Optimize the loop over injections.
   */
  void Inject(const unsigned int aDataSize, double *aData, const double aTimeStart);

  /**
   * @brief Plots the local waveform of a given injection.
   * @param[in] aInjectionIndex Injection index.
   */
  void Plot(const Long64_t aInjectionIndex);

 private:

  Streams *InStream;     ///< Input stream (DO NOT DELETE).

  // INJECTION
  TGraph *hplus;         ///< Injection waveform \f$h_{+}\f$ (local time).
  TGraph *hcross;        ///< Injection waveform \f$h_{\times}\f$ (local time).
  TGraph *hdet;          ///< Injection waveform projected on the detector \f$h_{det}\f$.
    
  /**
   * @brief Constructs the local time-domain waveforms \f$h_{+}\f$, \f$h_{\times}\f$, and \f$h_{det}\f$ for the current "user-defined" injection.
   * @details The \f$h_{+}(t)\f$ and \f$h_{\times}(t)\f$ waveforms are first calculated at the center of the Earth. Then, they are time-shifted to match the detector local time. Moreover, the wavform amplitude is rescaled by the injection amplitude.
   *
   * Then we compute the detector strain:
   *\f[
   h_{det}(t) = T(t)\times \left(F_{+}(t)\times h_{+}(t) + F_{\times}(t)\times h_{\times}(t)\right).
   \f]
   * The detector antenna factors \f$F_{+}\f$ and \f$F_{\times}\f$ are calculated for the source sky position and polarization and at any given time. Moreover, a Tukey window \f$T(t)\f$ is applied to have nice transitions to 0.
   */
  void ConstructUserWaveform(void);

  /**
   * @brief Constructs the local time-domain waveforms \f$h_{+}\f$, \f$h_{\times}\f$, and \f$h_{det}\f$ for the current "Sine-Gaussian" injection.
   * @details The \f$h_{+}(t)\f$ and \f$h_{\times}(t)\f$ waveforms are first calculated at the center of the Earth. Then, they are time-shifted to match the detector local time. Moreover, the wavform amplitude is rescaled by the injection amplitude.
   *
   * Then we compute the detector strain:
   *\f[
   h_{det}(t) = T(t)\times \left(F_{+}(t)\times h_{+}(t) + F_{\times}(t)\times h_{\times}(t)\right).
   \f]
   * The detector antenna factors \f$F_{+}\f$ and \f$F_{\times}\f$ are calculated for the source sky position and polarization and at any given time. Moreover, a Tukey window \f$T(t)\f$ is applied to have nice transitions to 0.
   * @todo Document the waveform calculation.
   */
  void ConstructSineGaussWaveform(void);

  ClassDef(InjEct,0)
};

#endif


