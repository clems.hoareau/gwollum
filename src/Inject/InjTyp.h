/**
 * @file 
 * @brief Manage injection types.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjTyp__
#define __InjTyp__


#include "CUtils.h"

using namespace std;

/**
 * @brief List of injection types.
 */
enum injtype{
             injtype_user = 0,  ///< User-defined waveform.
             injtype_sinegauss, ///< Sine-Gaussian waveform.
             injtype_n          ///< Number of injection types
};

#endif


