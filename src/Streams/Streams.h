/**
 * @file 
 * @brief Data stream identifier.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Streams__
#define __Streams__

#include "TimeDelay.h"
#include "Sample.h"
#include "DetConst.h"

using namespace std;

/**
 * @brief Manage streams.
 * @details This class is designed to identify data streams.
 * In particular, the Stream object can be converted into one detector of the LIGO-virgo-KAGRA global network if the stream name is recognized as such.
 * In that case many specific functions are provided to access the detector properties.
 * @author Florent Robinet
 */
class Streams: public Sample{
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Streams class.
   * @details A stream is identified by its name which shall follow the stream name convention.
   * @sa SetName()
   * @param[in] aName Stream name.
   * @param[in] aVerbose Verbosity level.
   */
  Streams(const string aName, const unsigned int aVerbose=0);
  
  /**
   * @brief Destructor of the Streams class.
   */
  virtual ~Streams(void);
  /**
     @}
  */

  /**
   * @brief Sets a new name to the stream.
   * @details A stream is identified by its name which shall follow the stream name convention:
   * [prefix]:[suffix], where [prefix] is 2-characters string exactly.
   * @param[in] aName Stream name.
   */
  void SetName(const string aName);

  /**
   * @brief Returns the stream full name.
   */
  inline string GetName(void){ return Name; };

  /**
   * @brief Returns the stream name prefix.
   */
  inline string GetNamePrefix(void){ return NamePrefix; };

  /**
   * @brief Returns the stream name suffix.
   */
  inline string GetNameSuffix(void){ return NameSuffix; };

  /**
   * @brief Returns the stream name suffix with only underscores.
   * @details Dash characters are turned into underscore characters.
   */
  inline string GetNameSuffixUnderScore(void){ return NameSuffixUnderScore; };

  /**
   * @brief Returns the stream name using the LIGO-Virgo file name convention.
   */
  inline string GetNameConv(void){ return NamePrefix+"-"+NameSuffixUnderScore; };

  /**
   * @brief Returns the stream directory.
   * @details Convention: ./[name]
   *
   * Optionally, a root directory can be added: [aOutDir]/[name]
   * @param[in] aOutDir Root directory.
   */
  inline string GetDirectory(const string aOutDir="."){
    return aOutDir+"/"+Name;
  };
    
  /**
   * @brief Returns the stream trigger file name.
   * @param[in] aGpsStart GPS start [s].
   * @param[in] aDuration Duration [s].
   * @param[in] aFileFormat File format (extension).
   * @param[in] aProcessName Process name.
   * @param[in] aOutDir Output directory.
   */
  inline string GetTriggerFileName(const unsigned int aGpsStart, const unsigned int aDuration, const string aFileFormat="root", const string aProcessName="PROC", const string aOutDir="."){
    ostringstream outfilestream;
    outfilestream<<aOutDir<<"/"<<GetNameConv()<<"_"<<aProcessName<<"-"<<aGpsStart<<"-"<<aDuration<<"."<<aFileFormat;
    return outfilestream.str();
  };
    
  /**
   * @brief Returns the detector index for this stream.
   */
  inline unsigned int GetDetectorIndex(void){ return DetIndex; };

  /**
   * @brief Returns the detector prefix for this stream.
   */
  inline string GetDetectorPrefix(void){ return DET_PREFIX[DetIndex]; };

  /**
   * @brief Computes antenna factors \f$F_+\f$ and \f$F_\times\f$.
   * @details Antenna factors are calculated for a source at a specified sky position, polarization angle, and sidereal time.
   * The implementation follows the formulae given in Anderson et al., PRD 63 042003 (2001)
   * @param[out] aFplus Returned \f$F_+\f$ value.
   * @param[out] aFcross Returned \f$F_\times\f$ value.
   * @param[in] aRa Right ascension [rad].
   * @param[in] aDec Declination [rad].
   * @param[in] aPsi Polarisation angle [rad].
   * @param[in] aGmst Sideral time [s].
   */
  void GetDetectorAMResponse(double &aFplus, double &aFcross,
                             const double aRa, const double aDec,
                             const double aPsi, const double aGmst);
  
  /**
   * @brief Converts the geocentric time to the detector local time [s].
   * @details The wave propagation direction must be given by the right ascension and declination.
   * @param[in] aRa Right ascension [rad].
   * @param[in] aDec Declination [rad].
   * @param[in] aGeocentricTime Geocentric time [s].
   */
  inline double GetLocalTime(const double aRa, const double aDec, const double aGeocentricTime){
    return aGeocentricTime+TimeDelayFromEarthCenter(DetLoc, aRa, aDec, aGeocentricTime);
  };
  
protected:
  
  string Name;                 ///< Stream name.
  
private:
  
  unsigned int verbose;        ///< Verbosity level.
  string NamePrefix;           ///< Stream prefix.
  string NameSuffix;           ///< Stream suffix.
  string NameSuffixUnderScore; ///< Stream suffix with only underscores.
  unsigned int DetIndex;       ///< Detector index.
  double Response[3][3];       ///< Detector response.
  double DetLoc[3];            ///< Detector location (vertex) on Earth.

  /**
   * @brief Associates detector's properties to the stream (based on prefix).
   */
  void MakeDetector(void);

  /**
   * @brief Computes the detector spatial response matrix.
   */
  void ComputeDetectorResponse(void);

  ClassDef(Streams,0)
};

#endif


