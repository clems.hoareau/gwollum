/**
 * @file 
 * @brief Program to test the Streams class.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Streams.h"

using namespace std;

/**
 * @brief Test program.
 */
int main (int argc, char* argv[]){

  //*********************************************************************
  // Streams(const string aName, const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nStreams(const string aName, const unsigned int aVerbose=0)"<<endl;
  Streams *S = new Streams("V1:TEST1-TEST2_TEST3");
  cout<<"\tGetName()"<<endl;
  if(S->GetName().compare("V1:TEST1-TEST2_TEST3")) return 1;
  cout<<"\tGetNamePrefix()"<<endl;
  if(S->GetNamePrefix().compare("V1")) return 1;
  cout<<"\tGetNameSuffix()"<<endl;
  if(S->GetNameSuffix().compare("TEST1-TEST2_TEST3")) return 1;
  cout<<"\tGetNameSuffixUnderScore()"<<endl;
  if(S->GetNameSuffixUnderScore().compare("TEST1_TEST2_TEST3")) return 1;
  cout<<"\tGetNameConv()"<<endl;
  if(S->GetNameConv().compare("V1-TEST1_TEST2_TEST3")) return 1;
  cout<<"\tGetDirectory() 1"<<endl;
  if(S->GetDirectory().compare("./V1:TEST1-TEST2_TEST3")) return 1;
  cout<<"\tGetDirectory() 2"<<endl;
  if(S->GetDirectory("/full/path").compare("/full/path/V1:TEST1-TEST2_TEST3")) return 1;
  cout<<"\tGetTriggerFileName() 2"<<endl;
  if(S->GetTriggerFileName(1299999999, 123, "txt", "PRTEST", "/full/path").compare("/full/path/V1-TEST1_TEST2_TEST3_PRTEST-1299999999-123.txt")) return 1;
  cout<<"\tGetDetectorIndex()"<<endl;
  if(S->GetDetectorIndex()!=DET_INDEX[::GetDetectorIndex("V1")]) return 1;
  cout<<"\tGetDetectorPrefix()"<<endl;
  if(S->GetDetectorPrefix().compare("V1")) return 1;

  //*********************************************************************
  // SetName(const string aName)
  //*********************************************************************
  cout<<"\nSetName(const string aName)"<<endl;
  S->SetName("T");
  cout<<"\tGetName()"<<endl;
  if(S->GetName().compare("00:T")) return 1;
  cout<<"\tGetNamePrefix()"<<endl;
  if(S->GetNamePrefix().compare("00")) return 1;
  cout<<"\tGetNameSuffix()"<<endl;
  if(S->GetNameSuffix().compare("T")) return 1;
  cout<<"\tGetNameSuffixUnderScore()"<<endl;
  if(S->GetNameSuffixUnderScore().compare("T")) return 1;
  cout<<"\tGetNameConv()"<<endl;
  if(S->GetNameConv().compare("00-T")) return 1;
  cout<<"\tGetDirectory() 1"<<endl;
  if(S->GetDirectory().compare("./00:T")) return 1;
  cout<<"\tGetDirectory() 2"<<endl;
  if(S->GetDirectory("/full/path").compare("/full/path/00:T")) return 1;
  cout<<"\tGetDetectorIndex()"<<endl;
  if(S->GetDetectorIndex()!=DET_INDEX[::GetDetectorIndex("00")]) return 1;
  cout<<"\tGetDetectorPrefix()"<<endl;
  if(S->GetDetectorPrefix().compare("00")) return 1;

  //*********************************************************************
  // GetDetectorAMResponse(double &aFplus, double &aFcross, const double aRa, const double aDec, const double aPsi, const double aGmst);
  //*********************************************************************
  cout<<"\nGetDetectorAMResponse()"<<endl;
  S->SetName("V1:TOTO");
  double fp, fc;
  S->GetDetectorAMResponse(fp, fc, 1.0, 1.0, 1.0, ::GreenwichMeanSiderealTime(S->GetLocalTime(1.0, 1.0, 1187008882.4)));
  if(fp>0.072214||fp<0.072213) return 2;
  if(fc>-0.461293||fc<-0.461294) return 2;


  delete S;
  return 0;
}

