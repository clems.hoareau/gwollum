/**
 * @file 
 * @brief Detector constants.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __DetConst__
#define __DetConst__

#include "CUtils.h"

/**
 * @brief Number of detectors.
 */
static const unsigned int N_KNOWN_DET = 6;

/**
 * @brief Detector indices.
 */
static const unsigned int DET_INDEX[N_KNOWN_DET] = {0,  // Unknown
                                                    1,  // Virgo
                                                    2,  // Hanford 1
                                                    3,  // Hanford 2
                                                    4,  // Livingston
                                                    5}; // KAGRA
                                           
/**
 * @brief Detector names.
 */
static const string DET_NAME[N_KNOWN_DET] = {"UNKNOWN",
                                             "Virgo", 
					     "Hanford1", 
					     "Hanford2", 
					     "Livingston",
                                             "KAGRA"};

/**
 * @brief Detector prefix.
 */
static const string DET_PREFIX[N_KNOWN_DET] = {"00",
                                               "V1", 
					       "H1", 
					       "H2", 
					       "L1",
                                               "K1"};

/**
 * @brief x-component of vertex location in Earth-centered frame (m).
 */
static const double DET_VERTEX_LOCATION_X_SI[N_KNOWN_DET] = {0.0,
                                                             4.54637409900e+06,
							     -2.16141492636e+06, 
							     -2.16141492636e+06, 
							     -7.42760447238e+04,
                                                             -3777336.02};

/**
 * @brief y-component of vertex location in Earth-centered frame (m).
 */
static const double DET_VERTEX_LOCATION_Y_SI[N_KNOWN_DET] = {0.0,
                                                             8.42989697626e+05,
							     -3.83469517889e+06, 
							     -3.83469517889e+06, 
							     -5.49628371971e+06,
                                                             3484898.411};

/**
 * @brief z-component of vertex location in Earth-centered frame (m).
 */
static const double DET_VERTEX_LOCATION_Z_SI[N_KNOWN_DET] = {0.0,
                                                             4.37857696241e+06,
							     4.60035022664e+06, 
							     4.60035022664e+06, 
							     3.22425701744e+06,
                                                             3765313.697};

/**
 * @brief x-component of unit vector pointing along x arm in Earth-centered frame.
 */
static const double DET_ARM_X_DIRECTION_X[N_KNOWN_DET] = {0.0,
                                                          -0.70045821479, 
							  -0.22389266154, 
							  -0.22389266154, 
							  -0.95457412153,
                                                          -0.3759040};

/**
 * @brief y-component of unit vector pointing along x arm in Earth-centered frame.
 */
static const double DET_ARM_X_DIRECTION_Y[N_KNOWN_DET] = {0.0,
                                                          0.20848948619, 
							  0.79983062746, 
							  0.79983062746, 
							  -0.14158077340,
                                                          -0.8361583};

/**
 * @brief z-component of unit vector pointing along x arm in Earth-centered frame.
 */
static const double DET_ARM_X_DIRECTION_Z[N_KNOWN_DET] = {0.0,
                                                          0.68256166277, 
							  0.55690487831, 
							  0.55690487831, 
							  -0.26218911324,
                                                          0.3994189};

/**
 * @brief x-component of unit vector pointing along y arm in Earth-centered frame.
 */
static const double DET_ARM_Y_DIRECTION_X[N_KNOWN_DET] = {0.0,
                                                          -0.05379255368, 
							  -0.91397818574, 
							  -0.91397818574, 
							  0.29774156894,
                                                          0.7164378};

/**
 * @brief y-component of unit vector pointing along y arm in Earth-centered frame.
 */
static const double DET_ARM_Y_DIRECTION_Y[N_KNOWN_DET] = {0.0,
                                                          -0.96908180549, 
							  0.02609403989, 
							  0.02609403989, 
							  -0.48791033647,
                                                          0.01114076};

/**
 * @brief z-component of unit vector pointing along y arm in Earth-centered frame.
 */
static const double DET_ARM_Y_DIRECTION_Z[N_KNOWN_DET] = {0.0,
                                                          0.24080451708, 
							  -0.40492342125, 
							  -0.40492342125, 
							  -0.82054461286,
                                                          0.6975620};
/**
 * @brief Returns the detector index associated to a detector prefix.
 * @param[in] aDetectorPrefix Detector prefix.
 * @sa ::DET_PREFIX
 */
inline unsigned int GetDetectorIndex(const string aDetectorPrefix){
  for(unsigned int d=0; d<N_KNOWN_DET; d++)
    if(!aDetectorPrefix.compare(DET_PREFIX[d])) return d;
  return 0;
}


#endif
