//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __NetConst__
#define __NetConst__

using namespace std;

/**
 * Returns the network type for any given network index.
 * @param aNetIndex network index
 */
inline int GetType(const int aNetIndex){ 
  int count = 0;
  int n=aNetIndex;
  while(n){
    n &= (n-1) ;
    count++;
  }
  return count;
};
 
/**
 * Tests whether a given stream index is inside a network.
 * true is returned if yes.
 * @param aStreamIndex stream index to test
 * @param aNetIndex network index
 */
inline bool IsStreamInNet(const int aStreamIndex, const int aNetIndex){
  int n = (int)pow(2.0,(double)aStreamIndex);
  if((aNetIndex&n)==n) return true;
  return false;
};


#endif
